USE `essentialmode`;

CREATE TABLE `weashops` (
  
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `item` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  
  PRIMARY KEY (`id`)
);

INSERT INTO `weashops` (`id`, `name`, `item`, `price`) VALUES
(1, 'BlackWeashop', 'WEAPON_SMOKEGRENADE', 1500),
(2, 'BlackWeashop', 'WEAPON_GRENADE', 4000),
(3, 'BlackWeashop', 'WEAPON_MOLOTOV', 2500),
(4, 'BlackWeashop', 'WEAPON_PIPEBOMB', 8000),
(5, 'BlackWeashop', 'WEAPON_STICKYBOMB', 12000),
(6, 'BlackWeashop', 'WEAPON_PISTOL', 3000),
(7, 'BlackWeashop', 'WEAPON_COMBATPISTOL', 5000),
(8, 'BlackWeashop', 'WEAPON_STUNGUN', 10000),
(9, 'BlackWeashop', 'WEAPON_SAWNOFFSHOTGUN', 15000),
(10, 'BlackWeashop', 'WEAPON_MICROSMG', 18000),
(11, 'BlackWeashop', 'WEAPON_HEAVYSHOTGUN', 20000),
(12, 'BlackWeashop', 'WEAPON_COMPACTRIFLE', 12000),
(13, 'BlackWeashop', 'WEAPON_ASSAULTRIFLE', 25000),
(14, 'BlackWeashop', 'WEAPON_SPECIALCARBINE', 55000),
(15, 'BlackWeashop', 'WEAPON_SMG', 60000),
(16, 'BlackWeashop', 'WEAPON_MG', 60000),
(17, 'BlackWeashop', 'WEAPON_COMBATPDW', 65000),
(18, 'BlackWeashop', 'WEAPON_MARKSMANRIFLE', 65000),
(19, 'BlackWeashop', 'WEAPON_GUSENBERG', 70000),
(20, 'BlackWeashop', 'WEAPON_COMPACTLAUNCHER', 100000),
(21, 'BlackWeashop', 'WEAPON_HOMINGLAUNCHER', 150000),
(22, 'GunShop', 'WEAPON_FLASHLIGHT', 500),
(23, 'GunShop', 'WEAPON_SWITCHBLADE', 500),
(24, 'GunShop', 'WEAPON_BAT', 500),
(25, 'GunShop', 'WEAPON_KNUCKLE', 500),
(26, 'GunShop', 'WEAPON_GOLFCLUB', 500),
(27, 'GunShop', 'WEAPON_FLAREGUN', 1000),
(28, 'GunShop', 'WEAPON_VINTAGEPISTOL', 8000),
(29, 'GunShop', 'WEAPON_PISTOL', 10000),
(30, 'GunShop', 'WEAPON_COMBATPISTOL', 15000),
(31, 'GunShop', 'WEAPON_PISTOL50', 25000),
(32, 'GunShop', 'WEAPON_COMPACTRIFLE', 35000),
(33, 'GunShop', 'WEAPON_BULLPUPRIFLE', 45000),
(34, 'GunShop', 'WEAPON_ASSAULTSHOTGUN', 55000);
;
