ESX = nil
local hasSniffer2 = false
local blipList2 = {}

local loaded = false

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
	PlayerData = xPlayer
	local isHavingTracker2 = false
	local isHavingSniffer2 = false

	for i=1, #PlayerData.inventory, 1 do
		if PlayerData.inventory[i].name == 'sniffer2' then
			if PlayerData.inventory[i].count > 0 then
				isHavingSniffer2 = true
			end
		elseif PlayerData.inventory[i].name == 'tracker2' then
			if PlayerData.inventory[i].count > 0 then
				isHavingTracker2 = true
			end
		end
	end

	if isHavingTracker2 then
		TriggerServerEvent('bl_trackGPS:addTracker2')
		if isHavingSniffer2 then
			hasSniffer2 = true
		end
	elseif isHavingSniffer2 then
		hasSniffer2 = true
	  	TriggerServerEvent('bl_trackGPS:get2')
	end
end)

RegisterNetEvent('bl_trackGPS:addSniffer2')
AddEventHandler('bl_trackGPS:addSniffer2', function()
	hasSniffer2 = true
  	TriggerServerEvent('bl_trackGPS:get2')
end)

RegisterNetEvent('bl_trackGPS:removeSniffer2')
AddEventHandler('bl_trackGPS:removeSniffer2', function()
	hasSniffer2 = false
  	TriggerServerEvent('bl_trackGPS:get2')
end)

RegisterNetEvent('bl_trackGPS:update2')
AddEventHandler('bl_trackGPS:update2', function()
  	TriggerServerEvent('bl_trackGPS:get2')
end)


RegisterNetEvent('bl_trackGPS:getCallback2')
AddEventHandler('bl_trackGPS:getCallback2', function(list)
	Citizen.CreateThread(function()
		-- Fuck that, blip seems to don't want to stick to player if inited to early
	    Citizen.Wait(5000)
		if #blipList2 > 0 then
			for i=#blipList2,1,-1 do
			    if DoesBlipExist(blipList2[i]) then
			    	RemoveBlip(blipList2[i])
			    end
			end
		end
			blipList = {}

		if hasSniffer2 and #list > 0 then
		  local players = ESX.Game.GetPlayers()
	      for i = 1, #players, 1 do
	          local ped    = GetPlayerPed(players[i])
	          local find = -1

	      	  for j = 1, #list, 1 do
		          if GetPlayerServerId(players[i]) == list[j].src then
		          	find = j
		          end
	      	  end

	          if find > -1 and DoesEntityExist(ped) and list[find] ~= nil then
	          	local myBlip2 = AddBlipForEntity(ped)

	          	if list[find].job == 'taxi' then
					SetBlipSprite(myBlip2, 1)
					SetBlipColour(myBlip2, 3)
					SetBlipScale(myBlip2, 1.0)
					BeginTextCommandSetBlipName("STRING")
					AddTextComponentString(tostring("Chauffeur"))
					EndTextCommandSetBlipName(myBlip2)
	          	end
				SetBlipDisplay(myBlip2, 3)
				SetBlipFlashes(myBlip2, true)
			  	table.insert(blipList2, myBlip2)
	    		Citizen.Wait(1000)
	          end
	        end
	      end
	end)
end)
