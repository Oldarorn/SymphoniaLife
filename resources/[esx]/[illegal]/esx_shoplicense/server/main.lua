ESX                      = nil
local ItemsLabels        = {}
local ItemLabelsLicenses = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('onMySQLReady', function()

	MySQL.Async.fetchAll(
		'SELECT * FROM items',
		{},
		function(result)

			for i=1, #result, 1 do
				ItemsLabels[result[i].name] = result[i].label
			end

		end
	)

	TriggerEvent('esx_license:getLicensesList', function(licenses)
		for i=1, #licenses, 1 do
			ItemLabelsLicenses[licenses[i].type] = licenses[i].label
		end
	end)

end)

RegisterServerEvent('esx_shoplicense:buyItem')
AddEventHandler('esx_shoplicense:buyItem', function(itemType, itemName, price)

    local _source         = source
    local xPlayer         = ESX.GetPlayerFromId(source)

    if xPlayer.get('money') >= price then

        xPlayer.removeMoney(price)

        if itemType == 'item_standard' then

	        xPlayer.addInventoryItem(itemName, 1)

        	TriggerClientEvent('esx:showNotification', _source, _U('bought') .. ItemsLabels[itemName])

        elseif itemType == 'item_license' then

        	TriggerEvent('esx_license:addLicense', _source, itemName)
        	TriggerClientEvent('esx:showNotification', _source, _U('bought') .. ItemLabelsLicenses[itemName])

        end

    else
        TriggerClientEvent('esx:showNotification', _source, _U('not_enough'))
    end

end)


ESX.RegisterServerCallback('esx_shoplicense:requestDBItems', function(source, cb)

	MySQL.Async.fetchAll(
		'SELECT * FROM shops',
		{},
		function(result)

			local shopItems  = {}

			for i=1, #result, 1 do

				if shopItems[result[i].name] == nil then
					shopItems[result[i].name] = {}
				end

				local label = nil

				if result[i].type == 'item_standard' then
					label = ItemsLabels[result[i].item]
				elseif result[i].type == 'item_license' then
					label = ItemLabelsLicenses[result[i].item]
				end

				table.insert(shopItems[result[i].name], {
					name  = result[i].item,
					price = result[i].price,
					label = label,
					type  = result[i].type
				})

			end

			cb(shopItems)

		end
	)

end)