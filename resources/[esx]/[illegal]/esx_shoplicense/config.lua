Config = {}

Config.DrawDistance = 100
Config.Size         = {x = 1.5, y = 1.5, z = 1.5}
Config.Type         = 1
Config.Locale       = 'fr'


Config.Zones = {

  ShopLicense = {
    Items = {},
    Pos = {
      {x = 120.986,   y = -2468.724,  z = 5.094}
    }
  },

  TwentyFourSeven = {
    Items = {},
    Pos = {
      --{x = 373.875,   y = 325.896,  z = 102.566}
    }
  },

  RobsLiquor = {
    Items = {},
    Pos = {
      --{x = 1135.808,  y = -982.281,  z = 45.415}
    }
  },

  LTDgasoline = {
    Items = {},
    Pos = {
      --{x = -48.519,   y = -1757.514, z = 28.421}
    }
  },

  ShopAero = {
    Items = {},
    Pos = {
      --{x = -1038.669,   y = -2731.113, z = 19.069}
    }
  },
  
  UpgradeArmes = {
    Items = {},
    Pos = {
      --{x = 1691.6618652,   y = 3757.926269,  z = 34.3053}
    }
  },

  UpgradeArmesPolice = {
    Items = {},
    Pos = {
      --{x = 461.57238,   y = -979.61309,  z = 30.3895},
    }
  }
}