ESX                    = nil

local CopsConnected    = 1

local PlayersFarmWeed        = {}
local PlayersTraitWeed       = {}
local PlayersVenteWeed       = {}

local PlayersFarmCoke        = {}
local PlayersTraitCoke       = {}
local PlayersVenteCoke       = {}

local PlayersFarmMeth        = {}
local PlayersTraitMeth       = {}
local PlayersVenteMeth       = {}

local PlayersFarmOpium       = {}
local PlayersTraitOpium      = {}
local PlayersVenteOpium      = {}


TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

AddEventHandler('esx:playerLoaded', function(source)
  TriggerEvent('esx_license:getLicenses', source, function(licenses)
    TriggerClientEvent('esx_drugs_v2:loadLicenses', source, licenses)
  end)
end)


function CountCops()

  local xPlayers = ESX.GetPlayers()

  CopsConnected = 0

  for i=1, #xPlayers, 1 do
    local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
    if xPlayer.job.name == 'police' then
      CopsConnected = CopsConnected + 1
    end
  end

  SetTimeout(5000, CountCops)

end

CountCops()


-------------- Récupération de weed -------------
local function FarmWeed(source)

  if CopsConnected < Config.RequiredCopsWeed then
    TriggerClientEvent('esx:showNotification', source, 'action ~r~impossible~s~, ~b~policiers~s~: ' .. CopsConnected .. '/' .. Config.RequiredCopsWeed)
    return
  end

  SetTimeout(4000, function()

    if PlayersFarmWeed[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local WeedQuantity = xPlayer.getInventoryItem('weed').count

      if WeedQuantity >= 100 then
        TriggerClientEvent('esx:showNotification', source, 'Vous avez déja les poches pleines.')
      else
                xPlayer.addInventoryItem('weed', 1)

        FarmWeed(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startFarmWeed')
AddEventHandler('esx_drugs_v2:startFarmWeed', function()
  local _source = source
  PlayersFarmWeed[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Vous récolter de la weed.')
  FarmWeed(source)
end)

RegisterServerEvent('esx_drugs_v2:stopFarmWeed')
AddEventHandler('esx_drugs_v2:stopFarmWeed', function()
  local _source = source
  PlayersFarmWeed[_source] = false
end)

------------ Séchage Weed  ---------------
local function TraitWeed(source)

  SetTimeout(4000, function()

    if PlayersTraitWeed[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local WeedPoochQuantity = xPlayer.getInventoryItem('weed_pooch').count

      if WeedPoochQuantity >= 50 then
        TriggerClientEvent('esx:showNotification', source, 'Vous etes plein')
      else
                xPlayer.removeInventoryItem('weed', 2)
                xPlayer.addInventoryItem('weed_pooch', 1)

        TraitWeed(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startTraitWeed')
AddEventHandler('esx_drugs_v2:startTraitWeed', function()
  local _source = source
  PlayersTraitWeed[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Traitement de la weed en cours.')
  TraitWeed(_source)
end)

RegisterServerEvent('esx_drugs_v2:stopTraitWeed')
AddEventHandler('esx_drugs_v2:stopTraitWeed', function()
  local _source = source
  PlayersTraitWeed[_source] = false
end)

--------- Vente Weed -------------
local function VenteWeed(source)

  if CopsConnected < Config.RequiredCopsWeed then
    TriggerClientEvent('esx:showNotification', source, 'action ~r~impossible~s~, ~b~policiers~s~: ' .. CopsConnected .. '/' .. Config.RequiredCopsWeed)
    return
  end

  SetTimeout(7500, function()

    if PlayersVenteWeed[source] == true then

        local _source = source
  local xPlayer = ESX.GetPlayerFromId(_source)

      local poochQuantity = xPlayer.getInventoryItem('weed_pooch').count

      if poochQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, 'vous n\'avez plus de pochons à ~r~vendre~s~')
      else
        xPlayer.removeInventoryItem('weed_pooch', 1)
        if CopsConnected == 1 then
                    xPlayer.addAccountMoney('black_money', 250)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de weed~s~')
                elseif CopsConnected == 2 then
                    xPlayer.addAccountMoney('black_money', 350)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de weed~s~')
                elseif CopsConnected == 3 then
                    xPlayer.addAccountMoney('black_money', 450)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de weed~s~')
                elseif CopsConnected == 4 then
                    xPlayer.addAccountMoney('black_money', 550)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de weed~s~')
                elseif CopsConnected == 5 then
                    xPlayer.addAccountMoney('black_money', 650)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de weed~s~')
                elseif CopsConnected >= 6 then
                    xPlayer.addAccountMoney('black_money', 750)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de weed~s~')
                end
        
        VenteWeed(source)
      end

    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startVenteWeed')
AddEventHandler('esx_drugs_v2:startVenteWeed', function()

  local _source = source

  PlayersVenteWeed[_source] = true

  TriggerClientEvent('esx:showNotification', _source, '~g~Vente en cours~s~...')

  VenteWeed(_source)

end)

RegisterServerEvent('esx_drugs_v2:StopVenteWeed')
AddEventHandler('esx_drugs_v2:StopVenteWeed', function()

  local _source = source

  PlayersVenteWeed[_source] = false

end)


-------------- Récupération de coke -------------
local function FarmCoke(source)

  SetTimeout(4000, function()

    if PlayersFarmCoke[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local CokeQuantity = xPlayer.getInventoryItem('coke').count

      if CokeQuantity >= 150 then
        TriggerClientEvent('esx:showNotification', source, 'Vous avez déja les poches pleines.')
      else
                xPlayer.addInventoryItem('coke', 1)

        FarmWeed(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startFarmCoke')
AddEventHandler('esx_drugs_v2:startFarmCoke', function()
  local _source = source
  PlayersFarmCoke[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Vous récolter de la coke.')
  FarmCoke(source)
end)

RegisterServerEvent('esx_drugs_v2:stopFarmCoke')
AddEventHandler('esx_drugs_v2:stopFarmCoke', function()
  local _source = source
  PlayersFarmCoke[_source] = false
end)

------------ Traitement Coke  ---------------
local function TraitCoke(source)

  SetTimeout(4000, function()

    if PlayersTraitCoke[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local CokePoochQuantity = xPlayer.getInventoryItem('coke_pooch').count

      if CokePoochQuantity <= 0 then
        TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas assez de coke')
      else
                xPlayer.removeInventoryItem('coke', 2)
                xPlayer.addInventoryItem('coke_pooch', 1)

        TraitCoke(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startTraitCoke')
AddEventHandler('esx_drugs_v2:startTraitCoke', function()
  local _source = source
  PlayersTraitCoke[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Traitement de la coke en cours.')
  TraitCoke(_source)
end)

RegisterServerEvent('esx_drugs_v2:stopTraitCoke')
AddEventHandler('esx_drugs_v2:stopTraitCoke', function()
  local _source = source
  PlayersTraitCoke[_source] = false
end)


local function VenteCoke(source)

  if CopsConnected < Config.RequiredCopsCoke then
    TriggerClientEvent('esx:showNotification', source, 'action ~r~impossible~s~, ~b~policiers~s~: ' .. CopsConnected .. '/' .. Config.RequiredCopsCoke)
    return
  end

  SetTimeout(7500, function()

    if PlayersVenteCoke[source] == true then

      local _source = source
      local xPlayer = ESX.GetPlayerFromId(_source)

      local poochQuantity = xPlayer.getInventoryItem('coke_pooch').count

      if poochQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, 'vous n\'avez plus de pochons à ~r~vendre~s~')
      else
        xPlayer.removeInventoryItem('coke_pooch', 1)
        if CopsConnected == 1 then
                    xPlayer.addAccountMoney('black_money', 380)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de coke~s~')
                elseif CopsConnected == 2 then
                    xPlayer.addAccountMoney('black_money', 465)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de coke~s~')
                elseif CopsConnected == 3 then
                    xPlayer.addAccountMoney('black_money', 550)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de coke~s~')
                elseif CopsConnected == 4 then
                    xPlayer.addAccountMoney('black_money', 640)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de coke~s~')
                elseif CopsConnected == 5 then
                    xPlayer.addAccountMoney('black_money', 730)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de coke~s~')
                elseif CopsConnected >= 6 then
                    xPlayer.addAccountMoney('black_money', 825)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de coke~s~')
                end
        
        VenteCoke(source)
      end

    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startVenteCoke')
AddEventHandler('esx_drugs_v2:startVenteCoke', function()

  local _source = source

  PlayersVenteCoke[_source] = true

  TriggerClientEvent('esx:showNotification', _source, '~g~Vente en cours~s~...')

  VenteCoke(_source)

end)

RegisterServerEvent('esx_drugs_v2:StopVenteCoke')
AddEventHandler('esx_drugs_v2:StopVenteCoke', function()

  local _source = source

  PlayersVenteCoke[_source] = false

end)

-------------- Récupération de meth -------------
local function FarmMeth(source)

  SetTimeout(4000, function()

    if PlayersFarmMeth[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local MethQuantity = xPlayer.getInventoryItem('meth').count

      if MethQuantity >= 200 then
        TriggerClientEvent('esx:showNotification', source, 'Vous avez déja les poches pleines.')
      else
                xPlayer.addInventoryItem('meth', 1)

        FarmMeth(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startFarmMeth')
AddEventHandler('esx_drugs_v2:startFarmMeth', function()
  local _source = source
  PlayersFarmMeth[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Vous récolter de la Meth.')
  FarmMeth(source)
end)

RegisterServerEvent('esx_drugs_v2:stopFarmMeth')
AddEventHandler('esx_drugs_v2:stopFarmMeth', function()
  local _source = source
  PlayersFarmMeth[_source] = false
end)

------------ Traitement Meth  ---------------
local function TraitMeth(source)

  SetTimeout(4000, function()

    if PlayersTraitMeth[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local MethPoochQuantity = xPlayer.getInventoryItem('meth_pooch').count

      if MethPoochQuantity <= 0 then
        TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas assez de Meth')
      else
                xPlayer.removeInventoryItem('meth', 2)
                xPlayer.addInventoryItem('meth_pooch', 1)

        TraitMeth(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startTraitMeth')
AddEventHandler('esx_drugs_v2:startTraitMeth', function()
  local _source = source
  PlayersTraitMeth[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Traitement de la meth en cours.')
  TraitMeth(_source)
end)

RegisterServerEvent('esx_drugs_v2:stopTraitMeth')
AddEventHandler('esx_drugs_v2:stopTraitMeth', function()
  local _source = source
  PlayersTraitMeth[_source] = false
end)

local function VenteMeth(source)

  if CopsConnected < Config.RequiredCopsMeth then
    TriggerClientEvent('esx:showNotification', source, 'action ~r~impossible~s~, ~b~policiers~s~: ' .. CopsConnected .. '/' .. Config.RequiredCopsMeth)
    return
  end

  SetTimeout(7500, function()

    if PlayersVenteMeth[source] == true then

      local _source = source
      local xPlayer = ESX.GetPlayerFromId(_source)

      local poochQuantity = xPlayer.getInventoryItem('meth_pooch').count

      if poochQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, 'vous n\'avez plus de pochons à ~r~vendre~s~')
      else
        xPlayer.removeInventoryItem('meth_pooch', 1)
        if CopsConnected == 1 then
                    xPlayer.addAccountMoney('black_money', 420)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de meth~s~')
                elseif CopsConnected == 2 then
                    xPlayer.addAccountMoney('black_money', 545)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de meth~s~')
                elseif CopsConnected == 3 then
                    xPlayer.addAccountMoney('black_money', 650)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de meth~s~')
                elseif CopsConnected == 4 then
                    xPlayer.addAccountMoney('black_money', 740)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de meth~s~')
                elseif CopsConnected == 5 then
                    xPlayer.addAccountMoney('black_money', 830)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de meth~s~')
                elseif CopsConnected >= 6 then
                    xPlayer.addAccountMoney('black_money', 925)
                    TriggerClientEvent('esx:showNotification', source, 'vous avez vendu ~g~x1 Pochon de meth~s~')
                end
        
        VenteMeth(source)
      end

    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startVenteMeth')
AddEventHandler('esx_drugs_v2:startVenteMeth', function()

  local _source = source

  PlayersVenteMeth[_source] = true

  TriggerClientEvent('esx:showNotification', _source, '~g~Vente en cours~s~...')

  VenteMeth(_source)

end)

RegisterServerEvent('esx_drugs_v2:StopVenteMeth')
AddEventHandler('esx_drugs_v2:StopVenteMeth', function()

  local _source = source

  PlayersVenteMeth[_source] = false

end)

-------------- Récupération de l'opium -------------
local function FarmOpium(source)

  SetTimeout(4000, function()

    if PlayersFarmOpium[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local OpiumQuantity = xPlayer.getInventoryItem('opium').count

      if OpiumQuantity >= 250 then
        TriggerClientEvent('esx:showNotification', source, 'Vous avez déja les poches pleines.')
      else
                xPlayer.addInventoryItem('opium', 1)

        FarmOpium(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startFarmOpium')
AddEventHandler('esx_drugs_v2:startFarmOpium', function()
  local _source = source
  PlayersFarmOpium[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Vous récolter de l\'Opium.')
  FarmOpium(source)
end)

RegisterServerEvent('esx_drugs_v2:stopFarmOpium')
AddEventHandler('esx_drugs_v2:stopFarmOpium', function()
  local _source = source
  PlayersFarmOpium[_source] = false
end)

------------ Traitement Opium  ---------------
local function TraitOpium(source)

  SetTimeout(4000, function()

    if PlayersTraitOpium[source] == true then

      local xPlayer  = ESX.GetPlayerFromId(source)
      local OpiumPoochQuantity = xPlayer.getInventoryItem('opium_pooch').count

      if OpiumPoochQuantity <= 0 then
        TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez pas assez d\'Opium')
      else
                xPlayer.removeInventoryItem('opium', 2)
                xPlayer.addInventoryItem('opium_pooch', 1)

        TraitOpium(source)
      end
    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startTraitOpium')
AddEventHandler('esx_drugs_v2:startTraitOpium', function()
  local _source = source
  PlayersTraitOpium[_source] = true
  TriggerClientEvent('esx:showNotification', _source, 'Traitement de l\'Opium en cours.')
  TraitOpium(_source)
end)

RegisterServerEvent('esx_drugs_v2:stopTraitOpium')
AddEventHandler('esx_drugs_v2:stopTraitOpium', function()
  local _source = source
  PlayersTraitOpium[_source] = false
end)

local function VenteOpium(source)

  if CopsConnected < Config.RequiredCopsOpium then
    TriggerClientEvent('esx:showNotification', source, 'action ~r~impossible~s~, ~b~policiers~s~: ' .. CopsConnected .. '/' .. Config.RequiredCopsOpium)
    return
  end

  SetTimeout(7500, function()

    if PlayersVenteOpium[source] == true then

      local _source = source
      local xPlayer = ESX.GetPlayerFromId(_source)

      local poochQuantity = xPlayer.getInventoryItem('opium_pooch').count

      if poochQuantity == 0 then
        TriggerClientEvent('esx:showNotification', source, 'vous n\'avez plus de pochons à ~r~vendre~s~')
      else
        xPlayer.removeInventoryItem('opium_pooch', 1)
        if CopsConnected == 0 then
                    xPlayer.addAccountMoney('black_money', 420)
                    TriggerClientEvent('esx:showNotification', source, 'Vous avez vendu 1 Pochon de ~b~opium')
                elseif CopsConnected == 1 then
                    xPlayer.addAccountMoney('black_money', 545)
                    TriggerClientEvent('esx:showNotification', source, 'Vous avez vendu 1 Pochon de ~b~opium')
                elseif CopsConnected == 2 then
                    xPlayer.addAccountMoney('black_money', 650)
                    TriggerClientEvent('esx:showNotification', source, 'Vous avez vendu 1 Pochon de ~b~opium')
                elseif CopsConnected == 3 then
                    xPlayer.addAccountMoney('black_money', 740)
                    TriggerClientEvent('esx:showNotification', source, 'Vous avez vendu 1 Pochon de ~b~opium')
                elseif CopsConnected == 4 then
                    xPlayer.addAccountMoney('black_money', 830)
                    TriggerClientEvent('esx:showNotification', source, 'Vous avez vendu 1 Pochon de ~b~opium')
                elseif CopsConnected >= 5 then
                    xPlayer.addAccountMoney('black_money', 925)
                    TriggerClientEvent('esx:showNotification', source, 'Vous avez vendu 1 Pochon de ~b~opium')
                end
        
        VenteOpium(source)
      end

    end
  end)
end

RegisterServerEvent('esx_drugs_v2:startVenteOpium')
AddEventHandler('esx_drugs_v2:startVenteOpium', function()

  local _source = source

  PlayersVenteOpium[_source] = true

  TriggerClientEvent('esx:showNotification', _source, '~g~Vente en cours~s~...')

  VenteOpium(_source)

end)

RegisterServerEvent('esx_drugs_v2:StopVenteOpium')
AddEventHandler('esx_drugs_v2:StopVenteOpium', function()

  local _source = source

  PlayersVenteOpium[_source] = false

end)