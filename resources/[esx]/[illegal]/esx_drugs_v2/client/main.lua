local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ESX                           = nil

local PlayerData              = {}
local Licenses                = {}
local GUI                     = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local LastPart                = nil
local LastData                = {}
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local TargetCoords            = nil

GUI.Time                      = 0





Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end
end)

RegisterNetEvent('esx_drugs_v2:loadLicenses')
AddEventHandler('esx_drugs_v2:loadLicenses', function(licenses)
    Licenses = licenses
end)


function OpenWeedFarmMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

  local elements = {}

  if not ownedLicenses['licenseweed'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

  if ownedLicenses['licenseweed'] then
		table.insert(elements, {label = 'Bud de Weed', value = 'weed'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'weed_farm',
      {
        title    = 'Récolte de weed',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'weed' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startFarmWeed')
        end       
      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'weed_farm_menu'
        CurrentActionMsg  = 'Récolte de weed'
        CurrentActionData = {}
      end
    )

end

function OpenWeedTraitMenu()

  local ownedLicenses = {}

  for i=1, #Licenses, 1 do
    ownedLicenses[Licenses[i].type] = true
  end

  local elements = {}

  if not ownedLicenses['licenseweed'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

  if ownedLicenses['licenseweed'] then
		table.insert(elements, {label = 'Traiter de la Weed', value = 'weedtrait'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'weed_trait',
      {
        title    = 'Traitement de weed',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'weedtrait' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startTraitWeed')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'weed_trait_menu'
        CurrentActionMsg  = 'Traitement de weed'
        CurrentActionData = {}
      end
    )

end

function OpenWeedVenteMenu()

  local ownedLicenses = {}

  for i=1, #Licenses, 1 do
    ownedLicenses[Licenses[i].type] = true
  end

  local elements = {}

  if not ownedLicenses['licenseweed'] then
    table.insert(elements, {label = 'Pas de license', value = ''})
  end

    if ownedLicenses['licenseweed'] then
    table.insert(elements, {label = 'Vente de Weed', value = 'weedvente'})
  end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'weed_vente',
      {
        title    = 'Vente de Weed',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'weedvente' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startVenteWeed')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'weed_vente_menu'
        CurrentActionMsg  = 'Vente de Weed'
        CurrentActionData = {}
      end
    )

end

---- COKE

function OpenCokeFarmMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

    local elements = {}

    if not ownedLicenses['licensecoke'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

    if ownedLicenses['licensecoke'] then
		table.insert(elements, {label = 'Récolter de la Coke', value = 'coke'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'coke_farm',
      {
        title    = 'Récolte de coke',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'coke' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startFarmCoke')
        end       
      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'coke_farm_menu'
        CurrentActionMsg  = 'Récolte de coke'
        CurrentActionData = {}
      end
    )

end

function OpenCokeTraitMenu()

  local ownedLicenses = {}

  for i=1, #Licenses, 1 do
    ownedLicenses[Licenses[i].type] = true
  end

  local elements = {}

  if not ownedLicenses['licensecoke'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

  if ownedLicenses['licensecoke'] then
		table.insert(elements, {label = 'Traiter de la Coke', value = 'coketrait'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'coke_trait',
      {
        title    = 'Traitement de la coke',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'coketrait' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startTraitCoke')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'coke_trait_menu'
        CurrentActionMsg  = 'Traitement de la coke'
        CurrentActionData = {}
      end
    )

end

function OpenCokeVenteMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

  local elements = {}

  if not ownedLicenses['licensecoke'] then
    table.insert(elements, {label = 'Pas de license', value = ''})
  end

    if ownedLicenses['licensecoke'] then
    table.insert(elements, {label = 'Vente de Coke', value = 'cokevente'})
  end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'coke_vente',
      {
        title    = 'Vente de Coke',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'cokevente' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startVenteCoke')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'coke_vente_menu'
        CurrentActionMsg  = 'Vente de Coke'
        CurrentActionData = {}
      end
    )

end

---- METH

function OpenMethFarmMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

  local elements = {}

  if not ownedLicenses['licensemeth'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

    if ownedLicenses['licensemeth'] then
		table.insert(elements, {label = 'Récolter de la Meth', value = 'meth'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'meth_farm',
      {
        title    = 'Récolte de meth',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'meth' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startFarmMeth')
        end       
      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'meth_farm_menu'
        CurrentActionMsg  = 'Récolte de meth'
        CurrentActionData = {}
      end
    )

end

function OpenMethTraitMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

  local elements = {}

  if not ownedLicenses['licensemeth'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

    if ownedLicenses['licensemeth'] then
		table.insert(elements, {label = 'Traiter de la Meth', value = 'methtrait'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'meth_trait',
      {
        title    = 'Traitement de la meth',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'methtrait' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startTraitMeth')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'meth_trait_menu'
        CurrentActionMsg  = 'Traitement de la meth'
        CurrentActionData = {}
      end
    )

end

function OpenMethVenteMenu()

  local ownedLicenses = {}

  for i=1, #Licenses, 1 do
    ownedLicenses[Licenses[i].type] = true
  end

  local elements = {}

  if not ownedLicenses['licensemeth'] then
    table.insert(elements, {label = 'Pas de license', value = ''})
  end

  if ownedLicenses['licensemeth'] then
    table.insert(elements, {label = 'Vente de Meth', value = 'methvente'})
  end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'meth_vente',
      {
        title    = 'Vente de Meth',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'methvente' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startVenteMeth')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'meth_vente_menu'
        CurrentActionMsg  = 'Vente de Meth'
        CurrentActionData = {}
      end
    )

end


---- OPIUM

function OpenOpiumFarmMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

  local elements = {}

  if not ownedLicenses['licenseopium'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

  if ownedLicenses['licenseopium'] then
		table.insert(elements, {label = 'Récolter de la Meth', value = 'opium'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'opium_farm',
      {
        title    = 'Récolte de l\'opium',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'opium' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startFarmOpium')
        end       
      end,
      function(data, menu)
        menu.close()
        CurrentAction     = 'opium_farm_menu'
        CurrentActionMsg  = 'Récolte de l\'Opium'
        CurrentActionData = {}
      end
    )

end

function OpenOpiumTraitMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

  local elements = {}

  if not ownedLicenses['licenseopium'] then
		table.insert(elements, {label = 'Pas de license', value = ''})
	end

  if ownedLicenses['licenseopium'] then
		table.insert(elements, {label = 'Traiter de l\'Opium', value = 'opiumtrait'})
	end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'opium_trait',
      {
        title    = 'Traitement de l\'Opium',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'opiumtrait' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startTraitOpium')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'opium_trait_menu'
        CurrentActionMsg  = 'Traitement de l\'Opium'
        CurrentActionData = {}
      end
    )

end

function OpenOpiumVenteMenu()

	local ownedLicenses = {}

	for i=1, #Licenses, 1 do
		ownedLicenses[Licenses[i].type] = true
	end

  local elements = {}

  if not ownedLicenses['licenseopium'] then
    table.insert(elements, {label = 'Pas de license', value = ''})
  end

  if ownedLicenses['licenseopium'] then
    table.insert(elements, {label = 'Vente de Meth', value = 'opiumvente'})
  end

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'opium_vente',
      {
        title    = 'Vente de l\'Opium',
        elements = elements
      },
      function(data, menu)
        if data.current.value == 'opiumvente' then
          menu.close()
          TriggerServerEvent('esx_drugs_v2:startVenteOpium')
        end

      end,

      function(data, menu)
        menu.close()
        CurrentAction     = 'opium_vente_menu'
        CurrentActionMsg  = 'Vente de l\'Opium'
        CurrentActionData = {}
      end
    )

end

AddEventHandler('esx_drugs_v2:hasEnteredMarker', function(zone)

  --- WEED

  if zone == 'WeedFarm' then
    CurrentAction     = 'weed_farm_menu'
    CurrentActionMsg  = 'Récolte de weed'
    CurrentActionData = {}
  end

  if zone == 'WeedTrait' then
    CurrentAction     = 'weed_trait_menu'
    CurrentActionMsg  = 'Traitement de weed'
    CurrentActionData = {}
  end

  if zone == 'WeedVente' then
    CurrentAction     = 'weed_vente_menu'
    CurrentActionMsg  = 'Vente de weed'
    CurrentActionData = {}
  end

  ---- COKE

  if zone == 'CokeFarm' then
    CurrentAction     = 'coke_farm_menu'
    CurrentActionMsg  = 'Récolte de Coke'
    CurrentActionData = {}
  end

  if zone == 'CokeTrait' then
    CurrentAction     = 'coke_trait_menu'
    CurrentActionMsg  = 'Traitement de Coke'
    CurrentActionData = {}
  end

  if zone == 'CokeVente' then
    CurrentAction     = 'coke_vente_menu'
    CurrentActionMsg  = 'Vente de Coke'
    CurrentActionData = {}
  end

  ---- METH

  if zone == 'MethFarm' then
    CurrentAction     = 'meth_farm_menu'
    CurrentActionMsg  = 'Récolte de Meth'
    CurrentActionData = {}
  end

  if zone == 'MethTrait' then
    CurrentAction     = 'meth_trait_menu'
    CurrentActionMsg  = 'Traitement de Meth'
    CurrentActionData = {}
  end

  if zone == 'MethVente' then
    CurrentAction     = 'meth_vente_menu'
    CurrentActionMsg  = 'Vente de Meth'
    CurrentActionData = {}
  end

  ---- OPIUM

  if zone == 'OpiumFarm' then
    CurrentAction     = 'opium_farm_menu'
    CurrentActionMsg  = 'Récolte de `l\'Opium'
    CurrentActionData = {}
  end

  if zone == 'OpiumTrait' then
    CurrentAction     = 'opium_trait_menu'
    CurrentActionMsg  = 'Traitement de l\'Opium'
    CurrentActionData = {}
  end

  if zone == 'OpiumVente' then
    CurrentAction     = 'opium_vente_menu'
    CurrentActionMsg  = 'Vente de l\'Opium'
    CurrentActionData = {}
  end

end)

AddEventHandler('esx_drugs_v2:hasExitedMarker', function(zone)

  ---- FARM

  if zone == 'WeedFarm' then
    TriggerServerEvent('esx_drugs_v2:stopFarmWeed')
  end

  if zone == 'CokeFarm' then
    TriggerServerEvent('esx_drugs_v2:stopFarmCoke')
  end  

  if zone == 'MethFarm' then
    TriggerServerEvent('esx_drugs_v2:stopFarmMeth')
  end

  if zone == 'OpiumFarm' then
    TriggerServerEvent('esx_drugs_v2:stopFarmOpium')
  end

  ---- TRAITEMENT

  if zone == 'WeedTrait' then
    TriggerServerEvent('esx_drugs_v2:stopTraitWeed')
  end

  if zone == 'CokeTrait' then
    TriggerServerEvent('esx_drugs_v2:stopTraitCoke')
  end

  if zone == 'MethTrait' then
    TriggerServerEvent('esx_drugs_v2:stopTraitMeth')
  end

  if zone == 'OpiumTrait' then
    TriggerServerEvent('esx_drugs_v2:stopTraitOpium')
  end

  ---- VENTE

  if zone == 'WeedVente' then
    TriggerServerEvent('esx_drugs_v2:stopVenteWeed')
  end

  if zone == 'CokeVente' then
    TriggerServerEvent('esx_drugs_v2:stopVenteCoke')
  end

  if zone == 'MethVente' then
    TriggerServerEvent('esx_drugs_v2:stopVenteMeth')
  end

  if zone == 'OpiumVente' then
    TriggerServerEvent('esx_drugs_v2:stopVenteOpium')
  end

  CurrentAction = nil
  ESX.UI.Menu.CloseAll()
end)


-- Display markers
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, 100, false, true, 2, false, false, false, false)
        end
      end
  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    Citizen.Wait(1)

      local coords      = GetEntityCoords(GetPlayerPed(-1))
      local isInMarker  = false
      local currentZone = nil

      for k,v in pairs(Config.Zones) do
        if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
          isInMarker  = true
          currentZone = k
        end
      end

      if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
        HasAlreadyEnteredMarker = true
        LastZone                = currentZone
        TriggerEvent('esx_drugs_v2:hasEnteredMarker', currentZone)
      end

      if not isInMarker and HasAlreadyEnteredMarker then
        HasAlreadyEnteredMarker = false
        TriggerEvent('esx_drugs_v2:hasExitedMarker', LastZone)
      end

  end
end)


-- Key Controls
Citizen.CreateThread(function()

  while ESX == nil or not ESX.IsPlayerLoaded() do
    Citizen.Wait(1)
  end

    while true do
        Citizen.Wait(1)

        if CurrentAction ~= nil then

          SetTextComponentFormat('STRING')
          AddTextComponentString(CurrentActionMsg)
          DisplayHelpTextFromStringLabel(0, 0, 1, -1)

          if IsControlJustReleased(0, 38) then

            if CurrentAction == 'weed_farm_menu' then
                OpenWeedFarmMenu()
            end

            if CurrentAction == 'weed_trait_menu' then
                OpenWeedTraitMenu()
            end

            if CurrentAction == 'weed_vente_menu' then
                OpenWeedVenteMenu()
            end

            if CurrentAction == 'coke_farm_menu' then
                OpenCokeFarmMenu()
            end

            if CurrentAction == 'coke_trait_menu' then
                OpenCokeTraitMenu()
            end

            if CurrentAction == 'coke_vente_menu' then
                OpenCokeVenteMenu()
            end

            if CurrentAction == 'meth_farm_menu' then
                OpenMethFarmMenu()
            end

            if CurrentAction == 'meth_trait_menu' then
                OpenMethTraitMenu()
            end

            if CurrentAction == 'meth_vente_menu' then
                OpenMethVenteMenu()
            end

            if CurrentAction == 'opium_farm_menu' then
                OpenOpiumFarmMenu()
            end

            if CurrentAction == 'opium_trait_menu' then
                OpenOpiumTraitMenu()
            end

            if CurrentAction == 'opium_vente_menu' then
                OpenOpiumVenteMenu()
            end

            CurrentAction = nil
          end
        end
    end
end)

