Config = {}
Config.Locale = 'fr'
Config.NumberOfCopsRequired = 5

Banks = {
	["fleeca"] = {
		position = { ['x'] = 147.04908752441, ['y'] = -1044.9448242188, ['z'] = 29.36802482605 },
		reward = math.random(300000,500000),
		nameofbank = "Fleeca Bank",
		secondsRemaining = 600, -- seconds
		lastrobbed = 0
	},
	["fleeca2"] = {
		position = { ['x'] = -2957.6674804688, ['y'] = 481.45776367188, ['z'] = 15.697026252747 },
		reward = math.random(300000,400000),
		nameofbank = "Fleeca Bank (Highway)",
		secondsRemaining = 600, -- seconds
		lastrobbed = 0
	},
		["fleecals"] = {
		position = { ['x'] = -354.8674804688, ['y'] = -54.42776367188, ['z'] = 49.057026252747 },
		reward = math.random(200000,300000),
		nameofbank = "Fleeca Bank (LS Custom)",
		secondsRemaining = 600, -- seconds
		lastrobbed = 0
	},
		["fleecawest"] = {
		position = { ['x'] = -1211.7574804688, ['y'] = -336.23776367188, ['z'] = 37.797026252747 },
		reward = math.random(200000,400000),
		nameofbank = "Fleeca Bank (West)",
		secondsRemaining = 600, -- seconds
		lastrobbed = 0
	},
	["blainecounty"] = {
		position = { ['x'] = -107.06505584717, ['y'] = 6474.8012695313, ['z'] = 31.62670135498 },
		reward = math.random(100000,200000),
		nameofbank = "Blaine County Savings",
		secondsRemaining = 600, -- seconds
		lastrobbed = 0
	},
	["PrincipalBank"] = {
		position = { ['x'] = 265.40866088867, ['y'] = 213.69737243652, ['z'] = 101.05 },
		reward = math.random(1000000,2000000),
		nameofbank = "Principal bank",
		secondsRemaining = 600, -- seconds
		lastrobbed = 0
	}
}