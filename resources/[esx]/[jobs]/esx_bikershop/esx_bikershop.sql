
INSERT INTO `addon_account` (name, label, shared) VALUES
  ('society_biker','BikerShop',1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  ('society_biker','BikerShop',1)
;

INSERT INTO `jobs` (name, label) VALUES
  ('biker','BikerShop')
;

INSERT INTO `job_grades` (job_name, grade, name, label, salary, skin_male, skin_female) VALUES
  ('biker',1,'novice','Prépa Moto',24,'{}','{}'),
  ('biker',2,'experimente','Prépa Custom',36,'{}','{}'),
  ('biker',3,'chief','Chef Mécano',48,'{}','{}'),
  ('biker',4,'boss','Boss',0,'{}','{}')
;

CREATE TABLE `biker_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicle` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

CREATE TABLE `biker_categories` (

  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `label` varchar(60) NOT NULL,

  PRIMARY KEY (`id`)
);

INSERT INTO `biker_categories` (name, label) VALUES
  ('motorcycles','Motos');