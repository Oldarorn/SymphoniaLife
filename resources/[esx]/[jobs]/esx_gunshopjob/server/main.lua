ESX                = nil

PlayersHarvesting  = {}
PlayersCrafting    = {}
PlayersDismantling = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
  TriggerEvent('esx_service:activateService', 'gunshop', Config.MaxInService)
end

TriggerEvent('esx_society:registerSociety', 'gunshop', 'Gunshop', 'society_gunshop', 'society_gunshop', 'society_gunshop', {type = 'private'})


RegisterServerEvent('esx_gunshopjob:getStockItem')
AddEventHandler('esx_gunshopjob:getStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_gunshop', function(inventory)

    local item = inventory.getItem(itemName)
    local itemQuantity = xPlayer.getInventoryItem(itemName).count
    local itemLimit    = xPlayer.getInventoryItem(itemName).limit

    if count > 0 and item.count >= count then
      if itemLimit ~= -1 and (itemQuantity + count) > itemLimit then
        TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_do_not_room'))
      else
        inventory.removeItem(itemName, count)
        xPlayer.addInventoryItem(itemName, count)
        TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)
      end
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

  end)

end)

ESX.RegisterServerCallback('esx_gunshopjob:getStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_gunshop', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_gunshopjob:putStockItems')
AddEventHandler('esx_gunshopjob:putStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_gunshop', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if count > 0 and item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

  end)

end)

RegisterServerEvent('esx_gunshopjob:getBossVaultStockItem')
AddEventHandler('esx_gunshopjob:getBossVaultStockItem', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_gunshop_bossvault', function(inventory)

    local item = inventory.getItem(itemName)
    local itemQuantity = xPlayer.getInventoryItem(itemName).count
    local itemLimit    = xPlayer.getInventoryItem(itemName).limit

    if count > 0 and item.count >= count then
      if itemLimit ~= -1 and (itemQuantity + count) > itemLimit then
        TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_do_not_room'))
      else
        inventory.removeItem(itemName, count)
        xPlayer.addInventoryItem(itemName, count)
        TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_removed') .. count .. ' ' .. item.label)
      end
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

  end)

end)

ESX.RegisterServerCallback('esx_gunshopjob:getBossVaultStockItems', function(source, cb)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_gunshop_bossvault', function(inventory)
    cb(inventory.items)
  end)

end)

RegisterServerEvent('esx_gunshopjob:putBossVaultStockItems')
AddEventHandler('esx_gunshopjob:putBossVaultStockItems', function(itemName, count)

  local xPlayer = ESX.GetPlayerFromId(source)

  TriggerEvent('esx_addoninventory:getSharedInventory', 'society_gunshop_bossvault', function(inventory)

    local item = inventory.getItem(itemName)
    local playerItemCount = xPlayer.getInventoryItem(itemName).count

    if count > 0 and item.count >= 0 and count <= playerItemCount then
      xPlayer.removeInventoryItem(itemName, count)
      inventory.addItem(itemName, count)
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('you_added') .. count .. ' ' .. item.label)
    else
      TriggerClientEvent('esx:showNotification', xPlayer.source, _U('invalid_quantity'))
    end

  end)

end)

ESX.RegisterServerCallback('esx_gunshopjob:getPlayerInventory', function(source, cb)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local items      = xPlayer.inventory

  cb({
    items      = items
  })

end)


------------ Récolte des ressources -------------------
local function Harvest(source, itemName)
  local _source   = source
  local _itemName = itemName

  SetTimeout(5000, function()

    if PlayersHarvesting[_source] == true then

      local xPlayer  = ESX.GetPlayerFromId(_source)
      local quantity = xPlayer.getInventoryItem(_itemName).count
      local limit    = xPlayer.getInventoryItem(_itemName).limit

      if quantity >= limit then
        TriggerClientEvent('esx:showNotification', _source, _U('you_do_not_room'))
      else
        xPlayer.addInventoryItem(_itemName, 1)
        Harvest(_source, _itemName)
      end

    end
  end)
end

RegisterServerEvent('esx_gunshopjob:startHarvest')
AddEventHandler('esx_gunshopjob:startHarvest', function(itemName)
  local _source = source
  local _itemName = itemName
    
  if PlayersHarvesting[_source] == false then
      TriggerClientEvent('esx:showNotification', _source, _U('no_harvesting_yet'))
      PlayersHarvesting[_source] = false
      
  else
    PlayersHarvesting[_source] = true
      TriggerClientEvent('esx:showNotification', _source, _U('harvesting_'.. _itemName))
      Harvest(_source, _itemName)
  end
end)

RegisterServerEvent('esx_gunshopjob:stopHarvest')
AddEventHandler('esx_gunshopjob:stopHarvest', function()
  local _source = source

  if PlayersHarvesting[_source] == true then
    PlayersHarvesting[_source] = false
    TriggerClientEvent('esx:showNotification', _source, _U('leaving_area'))

  else
    TriggerClientEvent('esx:showNotification', _source, _U('harvesting_allowed'))        
    PlayersHarvesting[_source] = true
  end
end)


RegisterServerEvent('esx_gunshopjob:craftGunParts')
AddEventHandler('esx_gunshopjob:craftGunParts', function()
  local _source         = source
  local xPlayer         = ESX.GetPlayerFromId(_source)
  local sulfurQuantity  = xPlayer.getInventoryItem('sulfur').count
  local coalQuantity    = xPlayer.getInventoryItem('coal').count
  local metalQuantity   = xPlayer.getInventoryItem('metal').count
  local gunPartQuantity = xPlayer.getInventoryItem('gun_part').count
  local gunPartLimit    = xPlayer.getInventoryItem('gun_part').limit

  if sulfurQuantity < 10 then
    TriggerClientEvent('esx:showNotification', _source, _U('not_enough_sulfur'))
  elseif coalQuantity < 10 then
    TriggerClientEvent('esx:showNotification', _source, _U('not_enough_coal'))
  elseif metalQuantity < 10 then
    TriggerClientEvent('esx:showNotification', _source, _U('not_enough_metal'))
  else
    if (gunPartQuantity + 3) > gunPartLimit then
      TriggerClientEvent('esx:showNotification', _source,_U('too_many_gunparts'))
    else
      TriggerClientEvent('esx:showNotification', _source,_U('assembling_gun_part'))
      SetTimeout(8000, function()
        xPlayer.removeInventoryItem('sulfur', 10)
        xPlayer.removeInventoryItem('coal', 10)
        xPlayer.removeInventoryItem('metal', 10)
        xPlayer.addInventoryItem('gun_part', 3)
      end)
    end
  end

end)

RegisterServerEvent('esx_gunshopjob:craft')
AddEventHandler('esx_gunshopjob:craft', function(craftName, craftLabel, price)
  local _source       = source
  local _craftName   = craftName
  local _craftLabel  = craftLabel
  local _price        = price

  local xPlayer         = ESX.GetPlayerFromId(_source)
  local gunPartQuantity = xPlayer.getInventoryItem('gun_part').count
  local craftQuantity  = xPlayer.getInventoryItem(_craftName).count
  local craftLimit     = xPlayer.getInventoryItem(_craftName).limit
  
  if gunPartQuantity < _price then
    TriggerClientEvent('esx:showNotification', _source, _U('not_enough_gunparts'))
  else
    if (craftQuantity + 1) > craftLimit then
      TriggerClientEvent('esx:showNotification', _source,_U('too_many_crafts') .. _craftLabel)
    else
      TriggerClientEvent('esx:showNotification', _source,_U('assembling_craft') .. _craftLabel)
      SetTimeout(8000, function()
        xPlayer.removeInventoryItem('gun_part', _price)
        xPlayer.addInventoryItem(_craftName, 1)
      end)
    end
  end

end)

RegisterServerEvent('esx_gunshopjob:dismantle')
AddEventHandler('esx_gunshopjob:dismantle', function(craftName, craftLabel, price, dismantleValue)
  local _source       = source
  local _craftName   = craftName
  local _craftLabel  = craftLabel
  local _price        = price

  local dismantlePrice  = _price / dismantleValue
  local intPrice        = math.ceil(dismantlePrice)
  local xPlayer         = ESX.GetPlayerFromId(_source)
  local craftQuantity  = xPlayer.getInventoryItem(_craftName).count
  local gunPartQuantity = xPlayer.getInventoryItem('gun_part').count
  local gunPartLimit    = xPlayer.getInventoryItem('gun_part').limit
  
  if craftQuantity < 1 then
    TriggerClientEvent('esx:showNotification', _source, _U('no_craft_to_dismantle'))
  else
    if (gunPartQuantity + intPrice) > gunPartLimit then
      TriggerClientEvent('esx:showNotification', _source, _U('too_many_gunparts'))
    else
      TriggerClientEvent('esx:showNotification', _source,_U('dismantling_craft') .. _craftLabel)
      SetTimeout(8000, function()
        xPlayer.removeInventoryItem(_craftName, 1)
        xPlayer.addInventoryItem('gun_part', intPrice)
      end)
    end
  end
end)

RegisterServerEvent('esx_gunshopjob:buyClip')
AddEventHandler('esx_gunshopjob:buyClip', function()

	local _source   = source
  local xPlayer   = ESX.GetPlayerFromId(_source)
  local itemName  = 'clip_50'
  local price     = 750
  local limit     = xPlayer.getInventoryItem(itemName).limit
  local qtty      = xPlayer.getInventoryItem(itemName).count

  local societyAccountGunshop = nil
    
  TriggerEvent('esx_addonaccount:getSharedAccount', 'society_gunshop', function(account)
    societyAccountGunshop = account
  end)
  
  if xPlayer.get('money') >= price then
    if qtty < limit then
      xPlayer.removeMoney(price)
      societyAccountGunshop.addMoney(price)
      xPlayer.addInventoryItem(itemName, 1)
      TriggerClientEvent('esx:showNotification', _source, _U('clip_bought'))
    else
      TriggerClientEvent('esx:showNotification', _source, _U('max_clip'))
    end
  else
    TriggerClientEvent('esx:showNotification', _source, _U('not_enough_money'))
  end

end)

RegisterServerEvent('esx_gunshopjob:removeClip')
AddEventHandler('esx_gunshopjob:removeClip', function(item)
	local xPlayer = ESX.GetPlayerFromId(source)
	xPlayer.removeInventoryItem(item, 1)
end)

ESX.RegisterUsableItem('clip_50', function(source)
  local xPlayer   = ESX.GetPlayerFromId(source)
  TriggerClientEvent('esx_gunshopjob:onUseClip', source, 'clip_50', 50)
end)
