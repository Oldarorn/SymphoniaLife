local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local JobBlips                = {}

ESX                           = nil

Citizen.CreateThread(function()
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
  CreateJobBlips()
end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)
  PlayerData.job = job
  DeleteJobBlips()
  CreateJobBlips()
end)


function IsJobTrue()
  if PlayerData ~= nil then
    local IsJobTrue = false
    if PlayerData.job ~= nil and PlayerData.job.name == 'gunshop' then
      IsJobTrue = true
    end
    return IsJobTrue
  end
end

function IsGradeBoss()
  if PlayerData ~= nil then
    local IsGradeBoss = false
    if PlayerData.job.grade_name == 'boss' or PlayerData.job.grade_name == 'viceboss' then
      IsGradeBoss = true
    end
    return IsGradeBoss
  end
end

function IsInVehicle()
  local ply = GetPlayerPed(-1)
  if IsPedSittingInAnyVehicle(ply) then
    return true
  else
    return false
  end
end

function SetVehicleMaxMods(vehicle)
  local props = {
    modEngine       = 0,
    modBrakes       = 0,
    modTransmission = 0,
    modSuspension   = 0,
    modTurbo        = false,
  }
  ESX.Game.SetVehicleProperties(vehicle, props)
end

function cleanPlayer(playerPed)
  ClearPedBloodDamage(playerPed)
  ResetPedVisibleDamage(playerPed)
  ClearPedLastWeaponDamage(playerPed)
  ResetPedMovementClipset(playerPed, 0)
end

function CreateJobBlips()
  if IsJobTrue() then               
    for k,v in pairs(Config.JobBlips) do
      local blipCoord = AddBlipForCoord(v.Pos.x, v.Pos.y, v.Pos.z)

      SetBlipSprite (blipCoord, v.Sprite)
      SetBlipDisplay(blipCoord, v.Display)
      SetBlipScale  (blipCoord, v.Scale)
      SetBlipColour (blipCoord, v.Color)
      SetBlipAsShortRange(blipCoord, true)

      BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(v.Name)
      EndTextCommandSetBlipName(blipCoord)

      table.insert(JobBlips, blipCoord)
    end 
  end
end

function DeleteJobBlips()
  if JobBlips[1] ~= nil then
    for i=1, #JobBlips, 1 do
      RemoveBlip(JobBlips[i])
      JobBlips[i] = nil
    end
  end
end

function OpenCloakroomMenu()

  local playerPed = GetPlayerPed(-1)

  local elements = {
    { label = _U('citizen_wear'),   value = 'citizen_wear'},
    { label = _U('work_wear'),      value = 'work_wear'}
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = _U('cloakroom'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      cleanPlayer(playerPed)

      if data.current.value == 'citizen_wear' then
        menu.close()
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin)
          TriggerEvent('skinchanger:loadSkin', skin)
        end)
      end

      if data.current.value == 'work_wear' then
        menu.close()
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end
        end)
      end

      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}

    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_cloakroom'
      CurrentActionMsg  = _U('open_cloackroom')
      CurrentActionData = {}
    end
  )
end

function OpenVaultMenu()

  if Config.EnableVaultManagement then

    local elements = {
      {label = _U('get_object'), value = 'get_stock'},
      {label = _U('put_object'), value = 'put_stock'}
    }
    

    ESX.UI.Menu.CloseAll()

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vault',
      {
        title    = _U('vault'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        if data.current.value == 'put_stock' then
          OpenPutStocksMenu()
        end

        if data.current.value == 'get_stock' then
          OpenGetStocksMenu()
        end

      end,
      
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_vault'
        CurrentActionMsg  = _U('open_vault')
        CurrentActionData = {}
      end
    )

  end

end

function OpenBossVaultMenu()

  local elements = {
    {label = _U('get_object'), value = 'get_stock'},
    {label = _U('put_object'), value = 'put_stock'}
  }
  

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'bossvault',
    {
      title    = _U('bossvault'),
      align    = 'top-left',
      elements = elements,
    },
    function(data, menu)

      if data.current.value == 'put_stock' then
        OpenPutBossVaultStocksMenu()
      end

      if data.current.value == 'get_stock' then
        OpenGetBossVaultStocksMenu()
      end

    end,
    
    function(data, menu)

      menu.close()

      CurrentAction     = 'menu_bossvault'
      CurrentActionMsg  = _U('open_bossvault')
      CurrentActionData = {}
    end
  )

end

function OpenVehicleSpawnerMenu()

  local vehicles = Config.Zones.Vehicles

  ESX.UI.Menu.CloseAll()

  if Config.EnableSocietyOwnedVehicles then

    local elements = {}

    ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(garageVehicles)

      for i=1, #garageVehicles, 1 do
        table.insert(elements, {label = GetDisplayNameFromVehicleModel(garageVehicles[i].model) .. ' [' .. garageVehicles[i].plate .. ']', value = garageVehicles[i]})
      end

      ESX.UI.Menu.Open(
        'default', GetCurrentResourceName(), 'vehicle_spawner',
        {
          title    = _U('vehicle_menu'),
          align    = 'top-left',
          elements = elements,
        },
        function(data, menu)

          menu.close()

          local vehicleProps = data.current.value

          local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)

          if not DoesEntityExist(vehicle) and not IsAnyVehicleNearPoint(vehicles.SpawnPoint.x, vehicles.SpawnPoint.y, vehicles.SpawnPoint.z, 7.0) then

            ESX.Game.SpawnVehicle(vehicleProps.model, vehicles.SpawnPoint, vehicles.Heading, function(vehicle)
                ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
                local playerPed = GetPlayerPed(-1)
                SetVehicleDirtLevel(vehicle, 0)
                SetVehRadioStation(vehicle, "OFF")
                --TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
            end)            

            TriggerServerEvent('esx_society:removeVehicleFromGarage', 'gunshop', vehicleProps)

          else
            ESX.ShowNotification(_U('vehicle_out'))
          end

        end,
        function(data, menu)

          menu.close()

          CurrentAction     = 'menu_vehicle_spawner'
          CurrentActionMsg  = _U('vehicle_spawner')
          CurrentActionData = {}

        end
      )

    end, 'gunshop')

  else

    local elements = {}

    for i=1, #Config.AuthorizedVehicles, 1 do
      local vehicle = Config.AuthorizedVehicles[i]
      table.insert(elements, {label = vehicle.label, value = vehicle.name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'vehicle_spawner',
      {
        title    = _U('vehicle_menu'),
        align    = 'top-left',
        elements = elements,
      },
      function(data, menu)

        menu.close()

        local model = data.current.value

        local vehicle = GetClosestVehicle(vehicles.SpawnPoint.x,  vehicles.SpawnPoint.y,  vehicles.SpawnPoint.z,  3.0,  0,  71)

        if not DoesEntityExist(vehicle) and not IsAnyVehicleNearPoint(vehicles.SpawnPoint.x, vehicles.SpawnPoint.y, vehicles.SpawnPoint.z, 7.0) then

          local playerPed = GetPlayerPed(-1)

          if Config.MaxInService == -1 then

            local plateNumber = math.random(1000, 9000)
            local plateJob    = Config.PlatePrefix
            local plate       = plateJob .." ".. plateNumber
            
            ESX.Game.SpawnVehicle(model, {
              x = vehicles.SpawnPoint.x,
              y = vehicles.SpawnPoint.y,
              z = vehicles.SpawnPoint.z
            }, vehicles.Heading, function(vehicle)
              --TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1) -- teleport into vehicle
              SetVehicleMaxMods(vehicle)
              SetVehicleDirtLevel(vehicle, 0)
              SetVehRadioStation(vehicle, "OFF")
              SetVehicleNumberPlateText(vehicle, plate)
            end)

          else

            ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)

              if canTakeService then

                local plateNumber = math.random(1000, 9000)
                local plateJob    = Config.Plate
                local plate       = plateJob .." ".. plateNumber

                ESX.Game.SpawnVehicle(model, {
                  x = vehicles[partNum].SpawnPoint.x,
                  y = vehicles[partNum].SpawnPoint.y,
                  z = vehicles[partNum].SpawnPoint.z
                }, vehicles[partNum].Heading, function(vehicle)
                  --TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)  -- teleport into vehicle
                  SetVehicleMaxMods(vehicle)
                  SetVehicleDirtLevel(vehicle, 0)
                  SetVehRadioStation(vehicle, "OFF")
                  SetVehicleNumberPlateText(vehicle, plate)
                end)

              else
                ESX.ShowNotification(_U('service_max') .. inServiceCount .. '/' .. maxInService)
              end

            end, 'gunshop')

          end

        else
          ESX.ShowNotification(_U('vehicle_out'))
        end

      end,
      function(data, menu)

        menu.close()

        CurrentAction     = 'menu_vehicle_spawner'
        CurrentActionMsg  = _U('vehicle_spawner')
        CurrentActionData = {}

      end
    )

  end

end

function OpenSocietyActionsMenu()

  local elements = {
    {label = _U('billing'),    value = 'billing'}
  }

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'gunshop_actions',
    {
      title    = _U('gunshop'),
      align    = 'top-left',
      elements = elements
    },
    function(data, menu)

      if data.current.value == 'billing' then
        OpenBillingMenu()
      end
    end,
    function(data, menu)
      menu.close()
    end
  )

end

function OpenBillingMenu()

  ESX.UI.Menu.Open(
    'dialog', GetCurrentResourceName(), 'billing',
    {
      title = _U('billing_amount')
    },
    function(data, menu)
    
      local amount = tonumber(data.value)
      local player, distance = ESX.Game.GetClosestPlayer()

      if player ~= -1 and distance <= 3.0 then

        menu.close()
        if amount == nil or amount < 0 then
          ESX.ShowNotification(_U('invalid_amount'))
        else
          TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(player), 'society_gunshop', _U('billing'), amount)
        end

      else
        ESX.ShowNotification(_U('no_players_nearby'))
      end

    end,
    function(data, menu)
      menu.close()
    end
  )
end

function OpenGetStocksMenu()

  ESX.TriggerServerCallback('esx_gunshopjob:getStockItems', function(items)

    print(json.encode(items))

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('gunshop_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              --OpenGetStocksMenu()

              TriggerServerEvent('esx_gunshopjob:getStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutStocksMenu()

  ESX.TriggerServerCallback('esx_gunshopjob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'stocks_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              --OpenPutStocksMenu()

              TriggerServerEvent('esx_gunshopjob:putStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenGetBossVaultStocksMenu()

  ESX.TriggerServerCallback('esx_gunshopjob:getBossVaultStockItems', function(items)

    print(json.encode(items))

    local elements = {}

    for i=1, #items, 1 do
      table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'bossvault_menu',
      {
        title    = _U('bossvault_stock'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'bossvault_menu_get_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              --OpenGetStocksMenu()

              TriggerServerEvent('esx_gunshopjob:getBossVaultStockItem', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenPutBossVaultStocksMenu()

  ESX.TriggerServerCallback('esx_gunshopjob:getPlayerInventory', function(inventory)

    local elements = {}

    for i=1, #inventory.items, 1 do

      local item = inventory.items[i]

      if item.count > 0 then
        table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
      end

    end

    ESX.UI.Menu.Open(
      'default', GetCurrentResourceName(), 'bossvault_menu',
      {
        title    = _U('inventory'),
        elements = elements
      },
      function(data, menu)

        local itemName = data.current.value

        ESX.UI.Menu.Open(
          'dialog', GetCurrentResourceName(), 'bossvault_menu_put_item_count',
          {
            title = _U('quantity')
          },
          function(data2, menu2)

            local count = tonumber(data2.value)

            if count == nil then
              ESX.ShowNotification(_U('invalid_quantity'))
            else
              menu2.close()
              menu.close()
              --OpenPutBossVaultStocksMenu()

              TriggerServerEvent('esx_gunshopjob:putBossVaultStockItems', itemName, count)
            end

          end,
          function(data2, menu2)
            menu2.close()
          end
        )

      end,
      function(data, menu)
        menu.close()
      end
    )

  end)

end

function OpenHarvestMenu(zone)

  local elements = {}
  for k,v in pairs(Config.Zones[zone].Items) do
    item = v.label
    table.insert(elements, {label = v.label, value = v.name})
  end

  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_harvest',
    {
      title    = _U('menu_harvest_title').. item,
      elements = elements
    },
    function(data, menu)
      menu.close()
      TriggerServerEvent('esx_gunshopjob:startHarvest', data.current.value, data.current.label)
    end,
    function(data, menu)
      menu.close()
    end
  )

end

function OpenCraftMenu()
  local elements = {
    {label = _U('gun_part'),    value = 'gun_part'}
  }
  if IsGradeBoss() or PlayerData.job.grade_name == 'vendor' or PlayerData.job.grade_name == 'vendor_experimente' then
    table.insert(elements, {label = _U('weapons'),      value = 'weapons'})
    table.insert(elements, {label = _U('components'),   value = 'components'})
    table.insert(elements, {label = _U('tints'),        value = 'tints'})
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_craft',
    {
      title    = _U('menu_crafting'),
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'gun_part' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:craftGunParts')
      end
      if data.current.value == 'weapons' then
        menu.close()
        OpenWeaponsMenu()
      end
      if data.current.value == 'components' then
        menu.close()
        OpenComponentsMenu()
      end
      if data.current.value == 'tints' then
        menu.close()
        OpenTintsMenu()
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_craft'
      CurrentActionMsg  = _U('menu_craft')
      CurrentActionData = {}
    end
  )
end

function OpenWeaponsMenu()
  local elements = {
    {label = _U('miscellaneous'),   value = 'miscellaneous'},
    {label = _U('contact_weapons'), value = 'contact_weapons'},
    {label = _U('handguns'),        value = 'handguns'},
    {label = _U('heavy_weapons'),   value = 'heavy_weapons'},
  }
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_weapons',
    {
      title    = _U('menu_weapons'),
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'miscellaneous' then
        menu.close()
        OpenMiscWeaponsMenu()
      end
      if data.current.value == 'contact_weapons' then
        menu.close()
        OpenContactWeaponsMenu()
      end
      if data.current.value == 'handguns' then
        menu.close()
        OpenHandgunsMenu()
      end
      if data.current.value == 'heavy_weapons' then
        menu.close()
        OpenHeavyWeaponsMenu()
      end
    end,
    function(data, menu)
      menu.close()
      OpenCraftMenu()
    end
  )
end

function OpenMiscWeaponsMenu()
  local elements = {}
  for k,v in pairs(Config.Weapons.Miscellaneous) do
    table.insert(elements, {
      label     = v.label .. ' [<span style="color:' .. v.color .. ';">' .. v.price .. '</span>]',
      realLabel = v.label,
      value     = v.name,
      price     = v.price
    })
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_weapons_misc',
    {
      title    = _U('menu_weapons_misc'),
      elements = elements
    },
    function(data, menu)
      OpenChoiceWeaponsMenu(data.current.value, data.current.realLabel, data.current.price)
    end,
    function(data, menu)
      menu.close()
      OpenWeaponsMenu()
    end
  )
end

function OpenContactWeaponsMenu()
  local elements = {}
  for k,v in pairs(Config.Weapons.ContactWeapons) do
    table.insert(elements, {
      label     = v.label .. ' [<span style="color:' .. v.color .. ';">' .. v.price .. '</span>]',
      realLabel = v.label,
      value     = v.name,
      value     = v.name,
      price     = v.price
    })
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_weapons_contacts',
    {
      title    = _U('menu_weapons_contacts'),
      elements = elements
    },
    function(data, menu)
      OpenChoiceWeaponsMenu(data.current.value, data.current.realLabel, data.current.price)
    end,
    function(data, menu)
      menu.close()
      OpenWeaponsMenu()
    end
  )
end

function OpenHandgunsMenu()
  local elements = {}
  for k,v in pairs(Config.Weapons.Handguns) do
    table.insert(elements, {
      label     = v.label .. ' [<span style="color:' .. v.color .. ';">' .. v.price .. '</span>]',
      realLabel = v.label,
      value     = v.name,
      value     = v.name,
      price     = v.price
    })
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_weapons_handguns',
    {
      title    = _U('menu_weapons_handguns'),
      elements = elements
    },
    function(data, menu)
      OpenChoiceWeaponsMenu(data.current.value, data.current.realLabel, data.current.price)
    end,
    function(data, menu)
      menu.close()
      OpenWeaponsMenu()
    end
  )
end

function OpenHeavyWeaponsMenu()
  local elements = {}
  for k,v in pairs(Config.Weapons.HeavyWeapons) do
    table.insert(elements, {
      label     = v.label .. ' [<span style="color:' .. v.color .. ';">' .. v.price ..'</span>]',
      realLabel = v.label,
      value     = v.name,
      value     = v.name,
      price     = v.price
    })
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_weapons_heavy',
    {
      title    = _U('menu_weapons_heavy'),
      elements = elements
    },
    function(data, menu)
      OpenChoiceWeaponsMenu(data.current.value, data.current.realLabel, data.current.price)
    end,
    function(data, menu)
      menu.close()
      OpenWeaponsMenu()
    end
  )
end

function OpenComponentsMenu()
  local elements = {}
  if IsGradeBoss() or PlayerData.job.grade_name == 'vendor_experimente' then
    table.insert(elements, {label = _U('component_skins'),   value = 'component_skins'})
  end
  for k,v in pairs(Config.ComponentsList) do
    table.insert(elements, {
      label     = v.label .. ' [<span style="color:' .. v.color .. ';">' .. v.price ..'</span>]',
      realLabel = v.label,
      value     = v.name,
      value     = v.name,
      price     = v.price
    })
  end
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_components',
    {
      title    = _U('menu_components'),
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'component_skins' then
        OpenComponentSkinsMenu()
      else
        OpenChoiceComponentsMenu(data.current.value, data.current.realLabel, data.current.price)
      end
    end,
    function(data, menu)
      menu.close()
      OpenCraftMenu()
    end
  )
end

function OpenComponentSkinsMenu()
  local elements = {}
  for k,v in pairs(Config.SkinsList) do
    table.insert(elements, {
      label     = v.label .. ' [<span>' .. v.price ..'</span>]',
      realLabel = v.label,
      value     = v.name,
      value     = v.name,
      price     = v.price
    })
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_components_skins',
    {
      title    = _U('menu_components_skins'),
      elements = elements
    },
    function(data, menu)
      OpenChoiceComponentsMenu(data.current.value, data.current.realLabel, data.current.price)
    end,
    function(data, menu)
      menu.close()
      OpenComponentsMenu()
    end
  )
end

function OpenTintsMenu()
  local elements = {}
  for k,v in pairs(Config.TintsList) do
    table.insert(elements, {
      label     = v.label .. ' [<span>' .. v.price ..'</span>]',
      realLabel = v.label,
      value     = v.name,
      price     = v.price
    })
  end
  if IsGradeBoss() or PlayerData.job.grade_name == 'vendor_experimente' then
    table.insert(elements, {label = _U('mk2_tints'),   value = 'mk2_tints'})
    table.insert(elements, {label = _U('component_tints'),   value = 'component_tints'})
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_tints',
    {
      title    = _U('menu_tints'),
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'mk2_tints' then
        OpenMk2TintsMenu()
      elseif data.current.value == 'component_tints' then
        OpenComponentTintsMenu()
      else
        OpenChoiceTintsMenu(data.current.value, data.current.realLabel, data.current.price)
      end
    end,
    function(data, menu)
      menu.close()
      OpenCraftMenu()
    end
  )
end

function OpenMk2TintsMenu()
  local elements = {}
  for k,v in pairs(Config.Mk2TintsList) do
    table.insert(elements, {
      label     = v.label .. ' [<span>' .. v.price ..'</span>]',
      realLabel = v.label,
      value     = v.name,
      price     = v.price
    })
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_mk2_tints',
    {
      title    = _U('menu_mk2_tints'),
      elements = elements
    },
    function(data, menu)
      OpenChoiceTintsMenu(data.current.value, data.current.realLabel, data.current.price)
    end,
    function(data, menu)
      menu.close()
      OpenTintsMenu()
    end
  )
end

function OpenComponentTintsMenu()
  local elements = {}
  for k,v in pairs(Config.MK2CamoTintsList) do
    table.insert(elements, {
      label     = v.label .. ' [<span>' .. v.price ..'</span>]',
      realLabel = v.label,
      value     = v.name,
      value     = v.name,
      price     = v.price
    })
  end
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_component_tints',
    {
      title    = _U('menu_component_tints'),
      elements = elements
    },
    function(data, menu)
      OpenChoiceTintsMenu(data.current.value, data.current.realLabel, data.current.price)
    end,
    function(data, menu)
      menu.close()
      OpenTintsMenu()
    end
  )
end

function OpenChoiceWeaponsMenu(weaponValue, weaponLabel, price)
  local elements = {
    { label = _U('craft_weapon'),     value = 'craft_weapon'},
    { label = _U('dismantle_weapon'), value = 'dismantle_weapon'},
  }
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_choice_menu',
    {
      title    = _U('menu_choice_menu') .. weaponLabel,
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'craft_weapon' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:craft', weaponValue, weaponLabel, price)
      end
      if data.current.value == 'dismantle_weapon' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:dismantle', weaponValue, weaponLabel, price, Config.weaponDismantlingValue)
      end
    end,
    function(data, menu)
      menu.close()
      OpenCraftMenu()
    end
  ) 
end

function OpenChoiceComponentsMenu(componentValue, componentLabel, price)
  local elements = {
    { label = _U('craft_component'),     value = 'craft_component'},
    { label = _U('dismantle_component'), value = 'dismantle_component'},
  }
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_choice_component_menu',
    {
      title    = _U('menu_choice_menu') .. componentLabel,
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'craft_component' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:craft', componentValue, componentLabel, price)
      end
      if data.current.value == 'dismantle_component' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:dismantle', componentValue, componentLabel, price, Config.componentDismantlingValue)
      end
    end,
    function(data, menu)
      menu.close()
      OpenCraftMenu()
    end
  ) 
end

function OpenChoiceTintsMenu(tintValue, tintLabel, price)
  local elements = {
    { label = _U('craft_tint'),     value = 'craft_tint'},
    { label = _U('dismantle_tint'), value = 'dismantle_tint'},
  }
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_choice_tint_menu',
    {
      title    = _U('menu_choice_menu') .. tintLabel,
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'craft_tint' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:craft', tintValue, tintLabel, price)
      end
      if data.current.value == 'dismantle_tint' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:dismantle', tintValue, tintLabel, price, Config.tintDismantlingValue)
      end
    end,
    function(data, menu)
      menu.close()
      OpenCraftMenu()
    end
  ) 
end

function OpenClipShopMenu()
  local elements = {
    {label = _U('clip_shop') .. ' [<span style="color:red;">$750</span>]',    value = 'clip_shop'}
  }
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_clipshop',
    {
      title    = _U('menu_clipshop_title'),
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'clip_shop' then
        menu.close()
        TriggerServerEvent('esx_gunshopjob:buyClip')
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_clipshop'
      CurrentActionMsg  = _U('menu_clipshop')
      CurrentActionData = {}
    end
  )
end

function OpenAntiNoiseHelmetMenu()
  local elements = {
    {label = _U('antinoise_helmet'),    value = 'antinoise_helmet'},
    {label = _U('clear_helmet'),        value = 'clear_helmet'}
  }
  
  ESX.UI.Menu.CloseAll()
  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'menu_antinoisehelmet',
    {
      title    = _U('menu_helmet_title'),
      elements = elements
    },
    function(data, menu)
      if data.current.value == 'antinoise_helmet' then
        local randColor = math.random(0,7)
        menu.close()
        SetPedPropIndex(GetPlayerPed(-1), 0, 0, randColor, 1) -- Anti-noise helmet
      end
      if data.current.value == 'clear_helmet' then
        menu.close()
        ClearPedProp(GetPlayerPed(-1),  0)  -- Helmet
      end
    end,
    function(data, menu)
      menu.close()
      CurrentAction     = 'menu_antinoisehelmet'
      CurrentActionMsg  = _U('menu_antinoisehelmet')
      CurrentActionData = {}
    end
  )
end


AddEventHandler('esx_gunshopjob:hasEnteredMarker', function(zone)
 
  if zone == 'BossActions' and IsGradeBoss() then
    CurrentAction     = 'menu_boss_actions'
    CurrentActionMsg  = _U('open_bossmenu')
    CurrentActionData = {}
  end

  if zone == 'Cloakrooms' then
    CurrentAction     = 'menu_cloakroom'
    CurrentActionMsg  = _U('open_cloackroom')
    CurrentActionData = {}
  end

  if Config.EnableVaultManagement then
    if zone == 'Vaults' then
      CurrentAction     = 'menu_vault'
      CurrentActionMsg  = _U('open_vault')
      CurrentActionData = {}
    end

    if zone == 'BossVault' then
      CurrentAction     = 'menu_bossvault'
      CurrentActionMsg  = _U('open_bossvault')
      CurrentActionData = {}
    end
  end
  
  if zone == 'Vehicles' then
    CurrentAction     = 'menu_vehicle_spawner'
    CurrentActionMsg  = _U('vehicle_spawner')
    CurrentActionData = {}
  end

  if zone == 'VehicleDeleters' then

    local playerPed = GetPlayerPed(-1)

    if IsPedInAnyVehicle(playerPed,  false) then

      local vehicle = GetVehiclePedIsIn(playerPed,  false)

      CurrentAction     = 'delete_vehicle'
      CurrentActionMsg  = _U('store_vehicle')
      CurrentActionData = {vehicle = vehicle}
    end

  end

  if Config.EnableHelicopters then
      if zone == 'Helicopters' then

        local helicopters = Config.Zones.Helicopters

        if not IsAnyVehicleNearPoint(helicopters.SpawnPoint.x, helicopters.SpawnPoint.y, helicopters.SpawnPoint.z,  3.0) then

          ESX.Game.SpawnVehicle('swift2', {
            x = helicopters.SpawnPoint.x,
            y = helicopters.SpawnPoint.y,
            z = helicopters.SpawnPoint.z
          }, helicopters.Heading, function(vehicle)
            SetVehicleModKit(vehicle, 0)
            SetVehicleLivery(vehicle, 0)
          end)

        end

      end

      if zone == 'HelicopterDeleters' then

        local playerPed = GetPlayerPed(-1)

        if IsPedInAnyVehicle(playerPed,  false) then

          local vehicle = GetVehiclePedIsIn(playerPed,  false)

          CurrentAction     = 'delete_vehicle'
          CurrentActionMsg  = _U('store_vehicle')
          CurrentActionData = {vehicle = vehicle}
        end

      end
  end

  if zone == 'Sulfur' or zone == 'Coal' or zone == 'Metal' then
    CurrentAction     = 'menu_harvest'
    CurrentActionMsg  = _U('menu_harvest')
    CurrentActionData = {zone = zone}
  end

  if zone == 'Craft' then
    CurrentAction     = 'menu_craft'
    CurrentActionMsg  = _U('menu_craft')
    CurrentActionData = {}
  end

  if zone == 'ClipShop' then
    CurrentAction     = 'menu_clipshop'
    CurrentActionMsg  = _U('menu_clipshop')
    CurrentActionData = {}
  end

  if zone == 'AntiNoiseHelmet' then
    CurrentAction     = 'menu_antinoisehelmet'
    CurrentActionMsg  = _U('menu_antinoisehelmet')
    CurrentActionData = {}
  end

end)

AddEventHandler('esx_gunshopjob:hasExitedMarker', function(zone)

  if zone == 'Craft' then
    TriggerServerEvent('esx_gunshopjob:stopCraft')
    TriggerServerEvent('esx_gunshopjob:stopCraftWeapons')
    TriggerServerEvent('esx_gunshopjob:stopDismantleWeapons')
  end

  if zone == 'Sulfur' or zone == 'Coal' or zone == 'Metal' then
    TriggerServerEvent('esx_gunshopjob:stopHarvest')
  end

  CurrentAction = nil
  ESX.UI.Menu.CloseAll()

end)

RegisterNetEvent('esx_gunshopjob:onUseClip')
AddEventHandler('esx_gunshopjob:onUseClip', function(clipName, clipValue)

  local playerPed               = GetPlayerPed(-1)
  local currentWeaponHash       = GetSelectedPedWeapon(playerPed)

  if HasPedGotWeapon(playerPed, currentWeaponHash, false) then -- Test if player have the Weapon
    TriggerServerEvent('esx_gunshopjob:removeClip', clipName)
    AddAmmoToPed(playerPed, currentWeaponHash, clipValue)
  end

end)

-- Create public blips
Citizen.CreateThread(function()
  local blipMarker = Config.Blips.Blip
  local blipCoord = AddBlipForCoord(blipMarker.Pos.x, blipMarker.Pos.y, blipMarker.Pos.z)

  SetBlipSprite (blipCoord, blipMarker.Sprite)
  SetBlipDisplay(blipCoord, blipMarker.Display)
  SetBlipScale  (blipCoord, blipMarker.Scale)
  SetBlipColour (blipCoord, blipMarker.Color)
  SetBlipAsShortRange(blipCoord, true)

  BeginTextCommandSetBlipName("STRING")
  AddTextComponentString(_U('map_blip'))
  EndTextCommandSetBlipName(blipCoord)
end)

-- Display markers
Citizen.CreateThread(function()
  while true do

    Wait(0)
    if IsJobTrue() then

      local coords = GetEntityCoords(GetPlayerPed(-1))

      for k,v in pairs(Config.Zones) do
        if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
          DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, false, 2, false, false, false, false)
        end
      end

    end

  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do

      Wait(0)
      if IsJobTrue() then

          local coords      = GetEntityCoords(GetPlayerPed(-1))
          local isInMarker  = false
          local currentZone = nil

          for k,v in pairs(Config.Zones) do
              if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
                  isInMarker  = true
                  currentZone = k
              end
          end

          if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
              HasAlreadyEnteredMarker = true
              LastZone                = currentZone
              TriggerEvent('esx_gunshopjob:hasEnteredMarker', currentZone)
          end

          if not isInMarker and HasAlreadyEnteredMarker then
              HasAlreadyEnteredMarker = false
              TriggerEvent('esx_gunshopjob:hasExitedMarker', LastZone)
          end

      end

  end
end)

-- Display public markers
Citizen.CreateThread(function()
  while true do

    Wait(0)

    local coordsPublic = GetEntityCoords(GetPlayerPed(-1))

    for k,v in pairs(Config.PublicZones) do
      if(v.Type ~= -1 and GetDistanceBetweenCoords(coordsPublic, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
        DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, false, 2, false, false, false, false)
      end
    end

  end
end)

-- Enter / Exit public marker events
Citizen.CreateThread(function()
  while true do

    Wait(0)
      
    local coordsPublic      = GetEntityCoords(GetPlayerPed(-1))
    local isInMarkerPublic  = false
    local currentZonePublic = nil
    for k,v in pairs(Config.PublicZones) do
      if(GetDistanceBetweenCoords(coordsPublic, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
        isInMarkerPublic  = true
        currentZonePublic = k
      end
    end
    if (isInMarkerPublic and not HasAlreadyEnteredMarkerPublic) or (isInMarkerPublic and LastZone ~= currentZonePublic) then
      HasAlreadyEnteredMarkerPublic = true
      LastZone                = currentZonePublic
      TriggerEvent('esx_gunshopjob:hasEnteredMarker', currentZonePublic)
    end
    if not isInMarkerPublic and HasAlreadyEnteredMarkerPublic then
      HasAlreadyEnteredMarkerPublic = false
      TriggerEvent('esx_gunshopjob:hasExitedMarker', LastZone)
    end

  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(1)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlJustReleased(0,  Keys['E']) then

        if CurrentAction == 'menu_cloakroom' and IsJobTrue() then
          OpenCloakroomMenu()
        end

        if CurrentAction == 'menu_vault' and IsJobTrue() then
          OpenVaultMenu()
        end

        if CurrentAction == 'menu_bossvault' and IsJobTrue() then
          OpenBossVaultMenu()
        end

        if CurrentAction == 'menu_vehicle_spawner' and IsJobTrue() then
          OpenVehicleSpawnerMenu()
        end

        if CurrentAction == 'delete_vehicle' and IsJobTrue() then

          if Config.EnableSocietyOwnedVehicles then

            local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
            TriggerServerEvent('esx_society:putVehicleInGarage', 'gunshop', vehicleProps)

          else

            if
              GetEntityModel(vehicle) == GetHashKey('gburrito2')
            then
              TriggerServerEvent('esx_service:disableService', 'gunshop')
            end

          end

          ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
        end

        if CurrentAction == 'menu_boss_actions' and IsJobTrue() and IsGradeBoss() then

          local options = {
            wash      = Config.EnableMoneyWash,
          }

          ESX.UI.Menu.CloseAll()

          TriggerEvent('esx_society:openBossMenu', 'gunshop', function(data, menu)

            menu.close()
            CurrentAction     = 'menu_boss_actions'
            CurrentActionMsg  = _U('open_bossmenu')
            CurrentActionData = {}

          end,options)

        end
  
        if CurrentAction == 'menu_harvest' and IsJobTrue() then
          if IsInVehicle() then
            ESX.ShowNotification(_U('no_farming_in_vehicle'))
          else
            OpenHarvestMenu(CurrentActionData.zone)
          end
        end
        
        if CurrentAction == 'menu_craft' and IsJobTrue() then
          if IsInVehicle() then
            ESX.ShowNotification(_U('no_farming_in_vehicle'))
          else
            OpenCraftMenu()
          end
        end

        if CurrentAction == 'menu_clipshop' then
          OpenClipShopMenu()
        end
      
        if CurrentAction == 'menu_antinoisehelmet' then
          OpenAntiNoiseHelmetMenu()
        end

        CurrentAction = nil

      end

    end

    --[[
    if IsControlJustReleased(0,  Keys['F6']) and IsJobTrue() and not ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'gunshop_actions') then
        OpenSocietyActionsMenu()
    end
    ]]--

  end
end)



---------------------------------------------------------------------------------------------------------
--NB : gestion des menu
---------------------------------------------------------------------------------------------------------

RegisterNetEvent('NB:openMenuGunshop')
AddEventHandler('NB:openMenuGunshop', function()
    OpenSocietyActionsMenu()
end)

RegisterNetEvent('NB:openMenuGunshop')
AddEventHandler('NB:openMenuGunshop', function()
    if ESX.UI.Menu.IsOpen('default', GetCurrentResourceName(), 'citizen_interaction') then
        ESX.UI.Menu.Close('default', GetCurrentResourceName(), 'citizen_interaction')
    end
end)
