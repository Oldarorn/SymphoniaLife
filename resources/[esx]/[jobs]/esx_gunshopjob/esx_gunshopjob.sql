SET @faction_name = 'gunshop';
SET @society_name = 'society_gunshop';
SET @faction_Name_Caps = 'Armurerie';



INSERT INTO `addon_account` (name, label, shared) VALUES
  (@society_name, @faction_Name_Caps, 1)
;

INSERT INTO `addon_inventory` (name, label, shared) VALUES
  (@society_name, @faction_Name_Caps, 1),
  ('society_gunshop_bossvault', 'Armurerie (Coffre Secondaire)', 1)
;

INSERT INTO `datastore` (name, label, shared) VALUES 
    (@society_name, @faction_Name_Caps, 1)
;

INSERT INTO `factions` (name, label, whitelisted) VALUES
  (@faction_name, @faction_Name_Caps, 1)
;

INSERT INTO `faction_grades` (faction_name, grade, name, label, skin_male, skin_female) VALUES
  (@faction_name, 0, 'recruit', 'Recrue', '{}', '{}'),
  (@faction_name, 1, 'vendor', 'Vendeur', '{}', '{}'),
  (@faction_name, 2, 'vendor_experimente', 'Vendeur Expérimenté', '{}', '{}'),
  (@faction_name, 3, 'viceboss', 'Gérant', '{}', '{}'),
  (@faction_name, 4, 'boss', 'Patron', '{}', '{}')
;

INSERT INTO `items` (`name`, `label`, `limit`) VALUES  
  ('sulfur', 'Soufre', 10),
  ('coal', 'Charbon', 10),
  ('metal', 'Métal', 10),
  ('gun_part', "Pièce d'arme", 25),
  ('clip_50', 'Chargeur [50]', 10)
;
