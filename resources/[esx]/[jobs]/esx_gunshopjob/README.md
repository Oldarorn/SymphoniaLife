# fxserver-esx_gunshopjob

FXServer ESX GUNSHOP JOB


[REQUIREMENTS]

* Player management (billing and boss actions)
  * esx_society => https://github.com/ESX-Org/esx_society
  * esx_billing => https://github.com/ESX-Org/esx_billing


[INSTALLATION]

1) CD in your resources/[esx] folder

3) Import esx_gunshopjob.sql in your database

4) Add this in your server.cfg :

```
start esx_gunshopjob
```
5) If you want player management you have to set Config.EnablePlayerManagement to true in config.lua
   You can config VaultManagement & Helicopters with true/false (don't forget to comment the area in the same file)

