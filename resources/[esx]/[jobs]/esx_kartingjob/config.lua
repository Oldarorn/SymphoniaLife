Config                        = {}
Config.DrawDistance           = 100.0
Config.MaxInService           = -1
Config.EnablePlayerManagement = true
Config.Locale                 = 'fr'

Config.Zones = {

	KartingActions = {
		Pos   = {x = 1723.337, y = 3323.585, z = 41.2},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 27
	},

	VehicleDeleter = {
		Pos   = {x = 1737.733, y = 3320.790, z = 40.2},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 102, g = 0, b = 0 },
		Type  = 1
	},

	VehicleSpawnPoint = {
		Pos   = {x = 1729.459, y = 3316.994, z = 41.2},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 0, b = 0},
		Type  = -1
	},

}
