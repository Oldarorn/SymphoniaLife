ESX         = nil
local Items = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

if Config.MaxInService ~= -1 then
	TriggerEvent('esx_service:activateService', 'teamster', Config.MaxInService)
end

TriggerEvent('esx_phone:registerNumber', 'teamster', 'Contact Routier', false, false)
TriggerEvent('esx_society:registerSociety', 'teamster', 'Routier', 'society_teamster', 'society_teamster', 'society_teamster', {type = 'private'})

AddEventHandler('onMySQLReady', function()

	MySQL.Async.fetchAll(
		'SELECT * FROM items',
		{},
		function(result)

			for i=1, #result, 1 do
				Items[result[i].name] = result[i].label
			end

		end
	)

end)

RegisterServerEvent('esx_teamsterjob:harvest')
AddEventHandler('esx_teamsterjob:harvest', function(item)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_teamster', function(account)
		account.removeMoney(Config.Jobs[item].buyPrice)
	end)

	xPlayer.addInventoryItem(item, 1)

	TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous avez ~g~ramassé~s~ x1 ' .. Items[item])

end)

RegisterServerEvent('esx_teamsterjob:delivery')
AddEventHandler('esx_teamsterjob:delivery', function()

	local xPlayer              = ESX.GetPlayerFromId(source)
	local totalCount           = 0
	local totalPlayerEarnings  = 0
	local totalSocietyEarnings = 0

	for k,v in pairs(Config.Jobs) do

		local item  = xPlayer.getInventoryItem(k)
		local count = item.count
		local file 	   = io.open('logs/Routier.txt', "a")
		local time 		= os.date("%d/%m/%y %X")
		local newFile	 = "( Routier ) Livraison : " .. (v.societyResellPrice * count) .. "$ par " .. xPlayer.name .. "\n"

		if count > 0 then

			TriggerEvent('esx_addoninventory:getSharedInventory', 'society_grocer', function(inventory)
				inventory.addItem(v.transform.item, v.transform.amount * count)
			end)

			xPlayer.removeInventoryItem(k, count)

			TriggerEvent('esx_addonaccount:getSharedAccount', 'society_grocer', function(account)
				account.removeMoney( (v.playerResellPrice + v.societyResellPrice) * count)
			end)

			TriggerEvent('esx_addonaccount:getSharedAccount', 'society_teamster', function(account)
				account.addMoney(v.societyResellPrice * count)
			end)


			
		file:write(newFile)
		file:flush()
		file:close()

			xPlayer.addMoney(v.playerResellPrice * count)

			totalCount           = totalCount + count
			totalPlayerEarnings  = totalPlayerEarnings  + v.playerResellPrice  * count
			totalSocietyEarnings = totalSocietyEarnings + v.societyResellPrice * count

		end

	end

	if totalCount == 0 then
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous n\'avez pas de marchandise à vendre')
	else
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous avez gagné ~g~$'       .. totalPlayerEarnings)
		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Votre société a gagné ~g~$' .. totalSocietyEarnings)
	end

end)

----------------------------------
---- Ajout Gestion Stock Boss ----
----------------------------------

RegisterServerEvent('esx_teamsterjob:getStockItem')
AddEventHandler('esx_teamsterjob:getStockItem', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_teamster', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= count then
			inventory.removeItem(itemName, count)
			xPlayer.addInventoryItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, 'Quantité invalide')
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous avez retiré x' .. count .. ' ' .. item.label)

	end)

end)

ESX.RegisterServerCallback('esx_teamsterjob:getStockItems', function(source, cb)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_teamster', function(inventory)
		cb(inventory.items)
	end)

end)

-------------
-- AJOUT 2 --
-------------

RegisterServerEvent('esx_teamsterjob:putStockItems')
AddEventHandler('esx_teamsterjob:putStockItems', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_teamster', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, 'Quantité invalide')
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous avez ajouté x' .. count .. ' ' .. item.label)

	end)

end)

--ESX.RegisterServerCallback('esx_teamsterjob:putStockItems', function(source, cb)

--	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_teamster', function(inventory)
--		cb(inventory.items)
--	end)

--end)

ESX.RegisterServerCallback('esx_teamsterjob:getPlayerInventory', function(source, cb)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local items      = xPlayer.inventory

	cb({
		items      = items
	})

end)

RegisterServerEvent('esx_routierjob:annonce')
AddEventHandler('esx_routierjob:annonce', function(result)
	local _source  = source
	local xPlayer  = ESX.GetPlayerFromId(_source)
	local xPlayers = ESX.GetPlayers()
	local text     = result
	print(text)
	for i=1, #xPlayers, 1 do
 		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
 		TriggerClientEvent('esx_routierjob:annonce', xPlayers[i],text)
	end

	Wait(8000)

	local xPlayers = ESX.GetPlayers()
	for i=1, #xPlayers, 1 do
 		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
 		TriggerClientEvent('esx_routierjob:annoncestop', xPlayers[i])
	end

end)