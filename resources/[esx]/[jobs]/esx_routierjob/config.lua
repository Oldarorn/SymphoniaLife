Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1
Config.EnableSocietyOwnedVehicles = false

Config.Zones = {
	
	VehicleSpawner = {
		Pos   = {x = 911.057, y = -1261.177, z = 24.581},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},

	VehicleSpawnPoint = {
		Pos   = {x = 907.488, y = -1247.852, z = 24.524},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = {x = 884.146, y = -1255.464, z = 25.189},
		Size  = {x = 5.0, y = 5.0, z = 3.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},

	Delivery = {
		Pos   = {x = -3048.11, y = 591.036, z = 6.6199},
		Size  = {x = 6.0, y = 6.0, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = -1
	},

	RoutierActions = {
		Pos   = {x = 923.406, y = -1266.032, z = 24.523},
		Size  = {x = 1.2, y = 1.5, z = 1.0},
		Color = {r = 102, g = 102, b = 204},
		Type  = -1
	},

	RemorqueActions = {
		Pos   = {x = 924.507, y = -1249.560, z = 24.483},
		Size  = {x = 1.2, y = 1.5, z = 1.0},
		Color = {r = 102, g = 102, b = 204},
		Type  = -1
	},

	RemorqueSpawnPoint = {
		Pos   = {x = 921.642, y = -1233.835, z = 25.541},
		Size  = {x = 1.2, y = 1.5, z = 1.0},
		Color = {r = 102, g = 102, b = 204},
		Type  = -1
	},

	BossActions = {
		Pos   = {x = 946.875, y = -1249.92, z = 26.0759},
		Size  = {x = 1.2, y = 1.5, z = 1.0},
		Color = {r = 102, g = 102, b = 204},
		Type  = -1
	}

}

Config.Jobs = {

	bread_cargo = {

		label              = 'Livraison de pain',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'bread'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	water_cargo = {

		label              = 'Livraison d\'eau',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'water'},
		Pos                = {x = 67.440, y = 6304.222, z = 29.872},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	beer_cargo = {

		label              = 'Livraison de bière',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'beer'},
		Pos                = {x = -78.048, y = 6537.318, z = 30.490},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	cigaret_cargo = {

      label              = 'Livraison de cigarettes',
      buyPrice           = 25,
	  playerResellPrice  = 250,
	  societyResellPrice = 650,
      vehicle            = 'phantom3',
      transform          = {amount = 100, item = 'cigaret'},
      Pos                = {x = 1155.102, y = -277.573, z = 68.019},
      Size               = {x = 6.0, y = 6.0, z = 1.0},
      Color              = {r = 204, g = 204, b = 0},
      Type               = 1

  	},

  	cocacola_cargo = {

    label              = 'Livraison de coca cola',
    buyPrice           = 25,
	playerResellPrice  = 250,
	societyResellPrice = 650,
    vehicle            = 'phantom3',
    transform          = {amount = 100, item = 'cocacola'},
    Pos                = {x = 889.991, y = 3652.997, z = 31.825},
    Size               = {x = 6.0, y = 6.0, z = 1.0},
    Color              = {r = 204, g = 204, b = 0},
    Type               = 1

  	},

    redbull_cargo = {

    label              = 'Livraison de RedBull',
    buyPrice           = 25,
    playerResellPrice  = 250,
	societyResellPrice = 650,
    vehicle            = 'phantom3',
    transform          = {amount = 100, item = 'redbull'},
    Pos                = {x = 666.135, y = -2688.06, z = 5.08514},
    Size               = {x = 6.0, y = 6.0, z = 1.0},
    Color              = {r = 204, g = 204, b = 0},
    Type               = 1

  	},

  	coffee_cargo = {

		label              = 'Livraison de café',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'coffee'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	eaugazifie_cargo = {

		label              = 'Livraison deau gazifie',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'eaugazifie'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	parapluie_cargo = {

		label              = 'Livraison de parapluie',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'parapluie'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	pepsi_cargo = {

		label              = 'Livraison de pepsi',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'pepsi'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	fanta_cargo = {

		label              = 'Livraison de fanta',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'fanta'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	cargo_7up = {

		label              = 'Livraison de 7up',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = '7up'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	sprite_cargo = {

		label              = 'Livraison de sprite',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'sprite'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	chocolate_cargo = {

		label              = 'Livraison de chocolate',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'chocolate'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	orangina_cargo = {

		label              = 'Livraison de orangina',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'orangina'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	tequila_cargo = {

		label              = 'Livraison de tequila',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'tequila'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	whisky_cargo = {

		label              = 'Livraison de whisky',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'whisky'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	fishing_rod_cargo = {

		label              = 'Livraison de canne a pêche',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'fishing_rod'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	sandwich_cargo = {

		label              = 'Livraison de sandwich',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'sandwich'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	gps_cargo = {

		label              = 'Livraison de gps',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'gps'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	tel_cargo = {

		label              = 'Livraison de tel',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'tel'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	},

	doliprane_cargo = {

		label              = 'Livraison de doliprane',
		buyPrice           = 25,
	    playerResellPrice  = 250,
	    societyResellPrice = 650,
		vehicle            = 'phantom3',
		transform          = {amount = 100, item = 'doliprane'},
		Pos                = {x = -33.487, y = 6417.324, z = 30.440},
		Size               = {x = 6.0, y = 6.0, z = 1.0},
		Color              = {r = 204, g = 204, b = 0},
		Type               = 1

	}
}
