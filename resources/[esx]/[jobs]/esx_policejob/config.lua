Config                            = {}
Config.DrawDistance               = 100.0
Config.MarkerType                 = 1
Config.MarkerSize                 = { x = 1.5, y = 1.5, z = 1.0 }
Config.MarkerColor                = { r = 50, g = 50, b = 204 }
Config.EnablePlayerManagement     = true
Config.EnableArmoryManagement     = true
Config.EnableESXIdentity          = true -- only turn this on if you are using esx_identity
Config.EnableNonFreemodePeds      = false -- turn this on if you want custom peds
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = true -- only turn this on if you are using esx_license
Config.MaxInService               = -1
Config.Locale                     = 'fr'

Config.PoliceStations = {

  LSPD = {

    Blip = {
      Pos     = { x = 425.130, y = -979.558, z = 30.711 },
      Sprite  = 60,
      Display = 4,
      Scale   = 1.2,
      Colour  = 29,
    },

    AuthorizedWeapons = {
      { name = 'WEAPON_NIGHTSTICK',       price = 150 },
      { name = 'WEAPON_COMBATPISTOL',     price = 450 },
      { name = 'WEAPON_SMG',              price = 1500 },
      { name = 'WEAPON_CARBINERIFLE_MK2', price = 3700 },
      { name = 'WEAPON_PUMPSHOTGUN',      price = 2300 },
      { name = 'WEAPON_STUNGUN',          price = 750 },
      { name = 'WEAPON_FLASHLIGHT',       price = 80 },
      { name = 'WEAPON_FIREEXTINGUISHER', price = 120 },
      { name = 'WEAPON_FLAREGUN',         price = 60 },
      { name = 'WEAPON_STICKYBOMB',       price = 4500 },
      { name = 'GADGET_PARACHUTE',        price = 400 },
    },

    AuthorizedVehicles = {
      { name = 'police',  label = 'Véhicule de patrouille 1' },
      { name = 'police2', label = 'Véhicule de patrouille 2' },
      { name = 'police3', label = 'Véhicule de patrouille 3' },
      { name = 'police4', label = 'Véhicule civil' },
      { name = 'policeb', label = 'Moto' },
      { name = 'policet', label = 'Van de transport' },
      { name = 'polp1',   label = 'Véhicule Rapide Intervention' }
    },

    Cloakrooms = {
      { x = 452.600, y = -993.306, z = 29.750 },
    },

    Armories = {
      { x = 451.699, y = -980.356, z = 29.689 },
    },

    Vehicles = {
      {
        Spawner    = { x = 454.69, y = -1017.4, z = 27.430 },
        SpawnPoint = { x = 438.42, y = -1018.3, z = 27.757 },
        Heading    = 90.0,
      }
    },

    Helicopters = {
      {
        Spawner    = { x = 466.477, y = -982.819, z = 42.691 },
        SpawnPoint = { x = 450.04, y = -981.14, z = 42.691 },
        Heading    = 0.0,
      }
    },

    VehicleDeleters = {
      { x = 462.74, y = -1014.4, z = 27.065 },
      { x = 462.40, y = -1019.7, z = 27.104 },
    },

    BossActions = {
      { x = 448.417, y = -973.208, z = 29.689 }
    },

  },

}


-- CHECK SKINCHANGER CLIENT MAIN.LUA for matching elements

Config.Uniforms = {
    
  cadet_wear = {
    male = {
                    ['tshirt_1'] = 50, ['tshirt_2'] = 0,
                    ['torso_1'] = 55, ['torso_2'] = 0,
                    ['decals_1'] = 0, ['decals_2'] = 0,
                    ['arms'] = 30,
                    ['pants_1'] = 35, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
			        ['glasses_1'] = 5, ['glasses_2'] = 0,
                    ['mask_1'] = 0, ['mask_2'] = 0,
			        ['watches_1'] = 3, ['watches_2'] = 0
    },
    female = {
                    ['tshirt_1'] = 35, ['tshirt_2'] = 0,
                    ['torso_1'] = 48, ['torso_2'] = 0,
                    ['decals_1'] = 0, ['decals_2'] = 0,
                    ['arms'] = 30,  
                    ['pants_1'] = 34, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
                    ['ears_1'] = 2, ['ears_2'] = 0,
                    ['watches_1'] = 11, ['watches_2'] = 3
    }
  },
  officer_wear = {
    male = {
                    ['tshirt_1'] = 50, ['tshirt_2'] = 0,
                    ['torso_1'] = 18, ['torso_2'] = 0,
                    ['decals_1'] = 0, ['decals_2'] = 0,
                    ['arms'] = 30, 
                    ['pants_1'] = 35, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
			        ['glasses_1'] = 5, ['glasses_2'] = 0,
                    ['mask_1'] = 0, ['mask_2'] = 0,
		            ['watches_1'] = 3, ['watches_2'] = 0
    },
    female = {
                    ['tshirt_1'] = 35, ['tshirt_2'] = 0,
                    ['torso_1'] = 36, ['torso_2'] = 0,
                    ['decals_1'] = 7, ['decals_2'] = 1,
                    ['arms'] = 44,
                    ['pants_1'] = 34, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
                    ['ears_1'] = 2, ['ears_2'] = 0,
                    ['watches_1'] = 11, ['watches_2'] = 3
    }
  },
  sergeant_wear = {
    male = {
                    ['tshirt_1'] = 50, ['tshirt_2'] = 0,
                    ['torso_1'] = 18, ['torso_2'] = 1,
                    ['decals_1'] = 8, ['decals_2'] = 0,
                    ['arms'] = 30, 
                    ['pants_1'] = 35, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
			        ['glasses_1'] = 5, ['glasses_2'] = 0,
                    ['mask_1'] = 0, ['mask_2'] = 0,
					['watches_1'] = 3, ['watches_2'] = 0
    },
    female = {
                    ['tshirt_1'] = 35, ['tshirt_2'] = 0,
                    ['torso_1'] = 36, ['torso_2'] = 0,
                    ['decals_1'] = 7, ['decals_2'] = 1,
                    ['arms'] = 44,
                    ['pants_1'] = 34, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
                    ['ears_1'] = 2, ['ears_2'] = 0,
                    ['watches_1'] = 11, ['watches_2'] = 3
    }
  },
  sergeant_chef_wear = {
    male = {
                    ['tshirt_1'] = 50, ['tshirt_2'] = 0,
                    ['torso_1'] = 18, ['torso_2'] = 1,
                    ['decals_1'] =8, ['decals_2'] = 1,
                    ['arms'] = 30,
                    ['pants_1'] = 35, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
		            ['glasses_1'] = 5, ['glasses_2'] = 0,
                    ['mask_1'] = 0, ['mask_2'] = 0,
	  	            ['watches_1'] = 3, ['watches_2'] = 0
    },
    female = {
                    ['tshirt_1'] = 35, ['tshirt_2'] = 0,
                    ['torso_1'] = 36, ['torso_2'] = 0,
                    ['decals_1'] = 7, ['decals_2'] = 2,
                    ['arms'] = 44,
                    ['pants_1'] = 34, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
                    ['ears_1'] = 2, ['ears_2'] = 0,
                    ['watches_1'] = 11, ['watches_2'] = 3
    }
  },
  lieutenant_wear = {
    male = {
                    ['tshirt_1'] = 50, ['tshirt_2'] = 0,
                    ['torso_1'] = 18, ['torso_2'] = 2,
                    ['decals_1'] = 8, ['decals_2'] = 3,
                    ['arms'] = 30, 
                    ['pants_1'] = 35, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
			    	['glasses_1'] = 5, ['glasses_2'] = 0,
                    ['mask_1'] = 0, ['mask_2'] = 0,
		         	['watches_1'] = 3, ['watches_2'] = 0
    },
    female = {
                    ['tshirt_1'] = 35, ['tshirt_2'] = 0,
                    ['torso_1'] = 36, ['torso_2'] = 0,
                    ['decals_1'] = 7, ['decals_2'] = 2,
                    ['arms'] = 44,
                    ['pants_1'] = 34, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
                    ['ears_1'] = 2, ['ears_2'] = 0,
                    ['watches_1'] = 11, ['watches_2'] = 3
    }
  },
  capitaine_wear = {
    male = {
                    ['tshirt_1'] = 58, ['tshirt_2'] = 0,
                    ['torso_1'] = 18, ['torso_2'] = 3,
                    ['decals_1'] = 11, ['decals_2'] = 4,
                    ['arms'] = 30, 
                    ['pants_1'] = 35, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
                    ['mask_1'] = 0, ['mask_2'] = 0,
				  	['glasses_1'] = 5, ['glasses_2'] = 0,
		        	['watches_1'] = 3, ['watches_2'] = 0
    },
    female = {
                    ['tshirt_1'] = 35, ['tshirt_2'] = 0,
                    ['torso_1'] = 36, ['torso_2'] = 0,
                    ['decals_1'] = 7, ['decals_2'] = 3,
                    ['arms'] = 44,
                    ['pants_1'] = 34, ['pants_2'] = 0,
                    ['shoes_1'] = 25, ['shoes_2'] = 0,
                    ['helmet_1'] = -1, ['helmet_2'] = 0,
                    ['chain_1'] = 8, ['chain_2'] = 0,
                    ['ears_1'] = 2, ['ears_2'] = 0,
                    ['watches_1'] = 11, ['watches_2'] = 3
    }
  },
  bullet_wear = {
    male = {
        ['bproof_1'] = 12,  ['bproof_2'] = 3
    },
    female = {
        ['bproof_1'] = 12,  ['bproof_2'] = 3
    }
  },
  gilet_wear = {
    male = {
        ['bproof_1'] = 18, ['bproof_2'] = 0
    },
    female = {
        ['bproof_1'] = 18, ['bproof_2'] = 0
    }
  }

}