ESX                    = nil
local PlayersCrafting  = {}
local PlayersCrafting2 = {}
local PlayersCrafting3 = {}
local PlayersCrafting4 = {}

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

TriggerEvent('esx_phone:registerNumber', 'grocer', 'Client épicerie', false, false)
TriggerEvent('esx_society:registerSociety', 'grocer', 'Epicier', 'society_grocer', 'society_grocer', 'society_grocer', {type = 'private'})

RegisterServerEvent('esx_grocerjob:getStockItem')
AddEventHandler('esx_grocerjob:getStockItem', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_grocer', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= count then
			inventory.removeItem(itemName, count)
			xPlayer.addInventoryItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, 'Quantité invalide')
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous avez retiré x' .. count .. ' ' .. item.label)

	end)

end)

RegisterServerEvent('esx_grocerjob:resell')
AddEventHandler('esx_grocerjob:resell', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)
	local item    = xPlayer.getInventoryItem(itemName)
	local price   = Config.ResellableItems[itemName] * count
	local file        = io.open('logs/Epicier.txt', "a")
    local time         = os.date("%d/%m/%y %X")
    local newFile     = "( Epicier ) Vente Moldu : " .. price .. "$ pour " .. count .." ".. item.label .. " par ".. xPlayer.name .. ".\n"
            
        file:write(newFile)
        file:flush()
        file:close()

	xPlayer.removeInventoryItem(itemName, count)
	
	TriggerEvent('esx_addonaccount:getSharedAccount', 'society_grocer', function(account)
		account.addMoney(price)
	end)

	TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous avez vendu ~y~x' .. count .. ' ' .. item.label .. '~s~ pour ~g~$' .. price)

end)

---------------------------- Account garage ----------------------------

RegisterServerEvent('esx_grocer:garage')
AddEventHandler('esx_grocer:garage', function()

	local _source 		  = source
 	local xPlayer         = ESX.GetPlayerFromId(source)

		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_grocer', function(account)
		account.removeMoney(600)
		end)
		TriggerEvent('esx_addonaccount:getSharedAccount', 'society_mecano', function(account)
  		account.addMoney(600)
 	end)

end)

-----------------------------------------------Sandiwich au thon-------------------------------------------------
local function Craft(source)

	SetTimeout(4000, function()

		if PlayersCrafting[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local fish = xPlayer.getInventoryItem('fish').count
			local bread = xPlayer.getInventoryItem('bread').count

			if fish <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de poisson')		
			end
			if bread <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de pain')		
			else
        xPlayer.removeInventoryItem('fish', 1)
				xPlayer.removeInventoryItem('bread', 1)
        xPlayer.addInventoryItem('fishburger', 1)
					
				Craft(source)
			end
		end
	end)
end

RegisterServerEvent('esx_grocerjob:startCraft')
AddEventHandler('esx_grocerjob:startCraft', function()
	local _source = source
	PlayersCrafting[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Fabrication de ~b~sandiwch au thon~s~...')
	Craft(_source)
end)

RegisterServerEvent('esx_grocerjob:stopCraft')
AddEventHandler('esx_grocerjob:stopCraft', function()
	local _source = source
	PlayersCrafting[_source] = false
end)

-----------------------------------------------Hamburger a la viande-------------------------------------------------

local function Craft2(source)

	SetTimeout(4000, function()

		if PlayersCrafting2[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local meat = xPlayer.getInventoryItem('meat').count
			local bread = xPlayer.getInventoryItem('bread').count

			if meat <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de viande')		
			end
			if bread <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de pain')		
			else
        xPlayer.removeInventoryItem('meat', 1)
				xPlayer.removeInventoryItem('bread', 1)
        xPlayer.addInventoryItem('ckiburger', 1)
					
				Craft2(source)
			end
		end
	end)
end

RegisterServerEvent('esx_grocerjob:startCraft2')
AddEventHandler('esx_grocerjob:startCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Fabrication de ~b~hamburger à la viande~s~...')
	Craft2(_source)
end)

RegisterServerEvent('esx_grocerjob:stopCraft2')
AddEventHandler('esx_grocerjob:stopCraft2', function()
	local _source = source
	PlayersCrafting2[_source] = false
end)

-----------------------------------------------Jus d'orange-------------------------------------------------

local function Craft3(source)

	SetTimeout(4000, function()

		if PlayersCrafting3[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local water = xPlayer.getInventoryItem('water').count
			local orange = xPlayer.getInventoryItem('orange').count

			if water <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ d\'eau')		
			end
			if orange <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ d\'orange')		
			else
        xPlayer.removeInventoryItem('water', 1)
				xPlayer.removeInventoryItem('orange', 1)
        xPlayer.addInventoryItem('orangejus', 1)
					
				Craft3(source)
			end
		end
	end)
end

RegisterServerEvent('esx_grocerjob:startCraft3')
AddEventHandler('esx_grocerjob:startCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Fabrication de ~b~de jus d\'orange~s~...')
	Craft3(_source)
end)

RegisterServerEvent('esx_grocerjob:stopCraft3')
AddEventHandler('esx_grocerjob:stopCraft3', function()
	local _source = source
	PlayersCrafting3[_source] = false
end)

-----------------------------------------------Jus de raisin-------------------------------------------------

local function Craft4(source)

	SetTimeout(4000, function()

		if PlayersCrafting4[source] == true then

			local xPlayer  = ESX.GetPlayerFromId(source)
			local water = xPlayer.getInventoryItem('water').count
			local grape = xPlayer.getInventoryItem('raisin').count

			if water <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ d\'eau')		
			end
			if grape <= 0 then
				TriggerClientEvent('esx:showNotification', source, 'Vous n\'avez ~r~pas assez~s~ de raisin')		
			else
        xPlayer.removeInventoryItem('water', 1)
				xPlayer.removeInventoryItem('raisin', 1)
        xPlayer.addInventoryItem('grapesjus', 1)
					
				Craft4(source)
			end
		end
	end)
end

RegisterServerEvent('esx_grocerjob:startCraft4')
AddEventHandler('esx_grocerjob:startCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = true
	TriggerClientEvent('esx:showNotification', _source, 'Fabrication de ~b~ jus de raisin~s~...')
	Craft4(_source)
end)

RegisterServerEvent('esx_grocerjob:stopCraft4')
AddEventHandler('esx_grocerjob:stopCraft4', function()
	local _source = source
	PlayersCrafting4[_source] = false
end)

RegisterServerEvent('esx_grocerjob:putStockItems')
AddEventHandler('esx_grocerjob:putStockItems', function(itemName, count)

	local xPlayer = ESX.GetPlayerFromId(source)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_grocer', function(inventory)

		local item = inventory.getItem(itemName)

		if item.count >= 0 then
			xPlayer.removeInventoryItem(itemName, count)
			inventory.addItem(itemName, count)
		else
			TriggerClientEvent('esx:showNotification', xPlayer.source, 'Quantité invalide')
		end

		TriggerClientEvent('esx:showNotification', xPlayer.source, 'Vous avez ajouté x' .. count .. ' ' .. item.label)

	end)

end)



ESX.RegisterServerCallback('esx_grocerjob:getStockItems', function(source, cb)

	TriggerEvent('esx_addoninventory:getSharedInventory', 'society_grocer', function(inventory)
		cb(inventory.items)
	end)

end)

ESX.RegisterServerCallback('esx_grocerjob:getPlayerInventory', function(source, cb)

	local xPlayer = ESX.GetPlayerFromId(source)
	local items   = xPlayer.inventory

	cb({
		items = items
	})

end)


RegisterServerEvent('esx_epicierjob:annonce')
AddEventHandler('esx_epicierjob:annonce', function(result)
	local _source  = source
	local xPlayer  = ESX.GetPlayerFromId(_source)
	local xPlayers = ESX.GetPlayers()
	local text     = result
	print(text)
	for i=1, #xPlayers, 1 do
 		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
 		TriggerClientEvent('esx_epicierjob:annonce', xPlayers[i],text)
	end

	Wait(8000)

	local xPlayers = ESX.GetPlayers()
	for i=1, #xPlayers, 1 do
 		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
 		TriggerClientEvent('esx_epicierjob:annoncestop', xPlayers[i])
	end

end)