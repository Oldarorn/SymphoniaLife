local Keys = {
  ["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
  ["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
  ["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
  ["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
  ["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
  ["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
  ["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
  ["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
  ["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

ESX                           = nil
local PlayerData              = {}
local HasAlreadyEnteredMarker = false
local LastZone                = nil
local CurrentAction           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local PedBlacklist            = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
end)

function Message()
	Citizen.CreateThread(function()
    while messagenotfinish do
    		Citizen.Wait(1)

			DisplayOnscreenKeyboard(1, "FMMC_MPM_NA", "", "", "", "", "", 30)
		    while (UpdateOnscreenKeyboard() == 0) do
		        DisableAllControlActions(0);
		       Citizen.Wait(1)
		    end
		    if (GetOnscreenKeyboardResult()) then
		        local result = GetOnscreenKeyboardResult()
		        messagenotfinish = false
		       TriggerServerEvent('esx_epicierjob:annonce',result)
		        
		    end


		end
	end)
	
end

function OpenGrocerActionsMenu()

	local elements = {
	    {label = 'Tenues', value = 'cloakroom'},
		{label = 'Retirer Stocks', value = 'stocks'},
		{label = 'Ajouter stocks', value = 'put_stock'},
		{label = 'Cuisiner sandwich au thon', value = 'sand_fish'},
		{label = 'Cuisiner hambuger a la viande', value = 'ham_viande'},
		{label = 'Tranformation jus d\'orange', value = 'orange'},
		{label = 'Tranformation jus de raisin', value = 'grape'}
	}

	if PlayerData.job.grade_name == 'boss' then
  	table.insert(elements, {label = 'Action Patron', value = 'boss_actions'})
	end

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'boss_actions',
    {
      title    = 'Epicier',
      elements = elements
    },
    function(data, menu)

    	if data.current.value == 'cloakroom' then
        OpenCloakroomMenu()
        end

    	if data.current.value == 'stocks' then
    		OpenStocksMenu()
    	end

    	if data.current.value == 'put_stock' then
    		 OpenPutStocksMenu()
    	end
			
			if data.current.value == 'sand_fish' then
				TriggerServerEvent('esx_grocerjob:startCraft')
    	end
			
			if data.current.value == 'ham_viande' then
    		TriggerServerEvent('esx_grocerjob:startCraft2')
    	end
			
			if data.current.value == 'orange' then
    		TriggerServerEvent('esx_grocerjob:startCraft3')
    	end
			
			if data.current.value == 'grape' then
    		TriggerServerEvent('esx_grocerjob:startCraft4')
    	end

			if data.current.value == 'boss_actions' then
				TriggerEvent('esx_society:openBossMenu', 'grocer', function(data, menu)
					menu.close()
				end)
			end

    end,
    function(data, menu)

      menu.close()

      CurrentAction     = 'grocer_actions_menu'
      CurrentActionMsg  = 'Appuez sur ~INPUT_CONTEXT~ pour accéder au menu'
      CurrentActionData = {}

    end
  )

end

function OpenCloakroomMenu()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'cloakroom',
    {
      title    = 'Tenues',
      align    = 'top-left',
      elements = {
        {label = 'Tenue Civil', value = 'citizen_wear'},
        {label = 'Tenue Epicier', value = 'agent_wear'}
      },
    },
    function(data, menu)

      menu.close()

      if data.current.value == 'citizen_wear' then

        TriggerServerEvent("player:serviceOff", "grocer")
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
          TriggerEvent('skinchanger:loadSkin', skin)
        end)

      end

      if data.current.value == 'agent_wear' then

        TriggerServerEvent("player:serviceOn", "grocer")
        ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)

          if skin.sex == 0 then
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_male)
          else
            TriggerEvent('skinchanger:loadClothes', skin, jobSkin.skin_female)
          end

        end)

      end
	  
    end,
    function(data, menu)
      menu.close()
    end
  )

end

function OpenBahamaActionsMenu()
	
	local elements = {
		{label = 'Ajouter stocks', value = 'put_stock'},
	}

  ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'boss_actions',
    {
      title    = 'Bahama Stock',
      elements = elements
    },
    function(data, menu)

    	if data.current.value == 'put_stock' then
    		 OpenPutStocksMenuBahama()
    	end

	
		end,
		function(data, menu)
	
		  menu.close()
	
		  CurrentAction     = 'Box_item_actions'
		  CurrentActionMsg  = 'Appuez sur ~INPUT_CONTEXT~ pour accéder au stock'
		  CurrentActionData = {}
	
		end
	  )
	
	end

function OpenMobileGrocerActionsMenu()

 	ESX.UI.Menu.CloseAll()

  ESX.UI.Menu.Open(
    'default', GetCurrentResourceName(), 'boss_actions',
    {
      title    = 'Epicier',
      elements = {
        {label = 'Passer une annonce', value = 'announce'},
      	{label = 'Facturation', value = 'billing'}
    	}
    },
    function(data, menu)

    	    if data.current.value == 'announce' then
			      messagenotfinish = true
				  Message()
			end

			if data.current.value == 'billing' then

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'billing',
					{
						title = 'Montant de la facture'
					},
					function(data, menu)
						local amount = tonumber(data.value)
						if amount == nil then
							ESX.ShowNotification('Montant invalide')
						else
							
							menu.close()
							
							local closestPlayer, closestDistance = ESX.Game.GetClosestPlayer()

							if closestPlayer == -1 or closestDistance > 3.0 then
								ESX.ShowNotification('Aucun joueur à proximité')
							else
								TriggerServerEvent('esx_billing:sendBill', GetPlayerServerId(closestPlayer), 'society_grocer', 'Epicier', amount)
							end

						end

					end,
					function(data, menu)
						menu.close()
					end
				)

			end

    end,
    function(data, menu)
    	menu.close()
    end
  )

end

function OpenStocksMenu()



	if Config.EnablePlayerManagement and PlayerData.job ~= nil and PlayerData.job.grade_name ~= 'interim' then
		ESX.TriggerServerCallback('esx_grocerjob:getStockItems', function(items)

			print(json.encode(items))

			local elements = {}

			for i=1, #items, 1 do
				table.insert(elements, {label = 'x' .. items[i].count .. ' ' .. items[i].label, value = items[i].name})
			end

			ESX.UI.Menu.Open(
				'default', GetCurrentResourceName(), 'stocks_menu',
				{
					title    = 'Epicier',
					elements = elements
				},
				function(data, menu)

					local itemName = data.current.value

					ESX.UI.Menu.Open(
						'dialog', GetCurrentResourceName(), 'stocks_menu_get_item_count',
						{
							title = 'Quantité'
						},
						function(data2, menu2)

							local count = tonumber(data2.value)

							if count == nil then
								ESX.ShowNotification('Quantité invalide')
							else
								menu2.close()
								menu.close()
								OpenStocksMenu()

								TriggerServerEvent('esx_grocerjob:getStockItem', itemName, count)
							end

						end,
						function(data2, menu2)
							menu2.close()
						end
					)

				end,
				function(data, menu)
					menu.close()
				end
			)

		end)
	else
		ESX.ShowNotification('Vous n\'avez pas l\'autorisation d\'ouvrir ce coffre')
	end
end

function OpenPutStocksMenuBahama()
	
	  ESX.TriggerServerCallback('esx_teamsterjob:getPlayerInventory', function(inventory)
	
		local elements = {}
	
		for i=1, #inventory.items, 1 do
	
		  local item = inventory.items[i]
	
		  if item.count > 0 then
			table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
		  end
	
		end
	
		ESX.UI.Menu.Open(
		  'default', GetCurrentResourceName(), 'stocks_menu',
		  {
			title    = 'Inventaire',
			elements = elements
		  },
		  function(data, menu)
	
			local itemName = data.current.value
	
			ESX.UI.Menu.Open(
			  'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
			  {
				title = 'Quantité'
			  },
			  function(data2, menu2)
	
				local count = tonumber(data2.value)
	
				if count == nil then
				  ESX.ShowNotification('Quantité invalide')
				else
				  menu2.close()
				  menu.close()
				  OpenPutStocksMenuBahama()
	
				  TriggerServerEvent('esx_bahamajob:putStockItems', itemName, count)
				end
	
			  end,
			  function(data2, menu2)
				menu2.close()
			  end
			)
	
		  end,
		  function(data, menu)
			menu.close()
		  end
		)
	
	  end)
	
	end

function OpenPutStocksMenu()

	ESX.TriggerServerCallback('esx_grocerjob:getPlayerInventory', function(inventory)

		local elements = {}

		for i=1, #inventory.items, 1 do

			local item = inventory.items[i]

			if item.count > 0 then
				table.insert(elements, {label = item.label .. ' x' .. item.count, type = 'item_standard', value = item.name})
			end

		end

	  ESX.UI.Menu.Open(
	    'default', GetCurrentResourceName(), 'stocks_menu',
	    {
	      title    = 'Inventaire',
	      elements = elements
	    },
	    function(data, menu)

	    	local itemName = data.current.value

				ESX.UI.Menu.Open(
					'dialog', GetCurrentResourceName(), 'stocks_menu_put_item_count',
					{
						title = 'Quantité'
					},
					function(data2, menu2)

						local count = tonumber(data2.value)

						if count == nil then
							ESX.ShowNotification('Quantité invalide')
						else
							menu2.close()
				    	menu.close()
				    	OpenPutStocksMenu()

							TriggerServerEvent('esx_grocerjob:putStockItems', itemName, count)
						end

					end,
					function(data2, menu2)
						menu2.close()
					end
				)

	    end,
	    function(data, menu)
	    	menu.close()
	    end
	  )

	end)

end

function OpenGrocerSpawn()
	
		local elements = {
			{label = 'Vehicule garage', value = 'spawn_vehicle'},
		}
	
		ESX.UI.Menu.CloseAll()
	
		ESX.UI.Menu.Open(
			'default', GetCurrentResourceName(), 'grocer_spawn_actions',
			{
				title    = 'Epicier',
				elements = elements
			},
			function(data, menu)
	
				if data.current.value == 'spawn_vehicle' then
	
					if Config.EnableSocietyOwnedVehicles then
	
						local elements = {}
	
						ESX.TriggerServerCallback('esx_society:getVehiclesInGarage', function(vehicles)
	
							for i=1, #vehicles, 1 do
								if IsModelValid(vehicles[i].model) and vehicles[i].plate ~= nil then
									table.insert(elements, {label = GetDisplayNameFromVehicleModel(vehicles[i].model) .. ' [' .. vehicles[i].plate .. ']', value = vehicles[i]})
								end
							end
							ESX.UI.Menu.Open(
								'default', GetCurrentResourceName(), 'vehicle_spawner',
								{
									title    = 'Sortir Vehicule',
									align    = 'top-left',
									elements = elements,
								},
								function(data, menu)
	
								menu.close()
	
									local vehicleProps = data.current.value
	
									ESX.Game.SpawnVehicle(vehicleProps.model, Config.Zones.VehicleSpawnPoint.Pos, 187.0, function(vehicle)
										ESX.Game.SetVehicleProperties(vehicle, vehicleProps)
										local playerPed = GetPlayerPed(-1)
										TaskWarpPedIntoVehicle(playerPed,  vehicle,  -1)
									end)
	
									TriggerServerEvent('esx_society:removeVehicleFromGarage', 'grocer', vehicleProps)
	
								end,
								function(data, menu)
								menu.close()
								end
							)
	
						end, 'grocer')
	
					else
	
						menu.close()
	
						if Config.MaxInService == -1 then
	
							local playerPed = GetPlayerPed(-1)
							local coords    = Config.Zones.VehicleSpawnPoint.Pos
	
							ESX.Game.SpawnVehicle('grocer', coords, 225.0, function(vehicle)
								TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
							end)
	
						else
	
							ESX.TriggerServerCallback('esx_service:enableService', function(canTakeService, maxInService, inServiceCount)
	
								if canTakeService then
	
									local playerPed = GetPlayerPed(-1)
									local coords    = Config.Zones.VehicleSpawnPoint.Pos
	
									ESX.Game.SpawnVehicle('grocer', coords, 225.0, function(vehicle)
										TaskWarpPedIntoVehicle(playerPed,  vehicle, -1)
									end)
	
								else
	
									ESX.ShowNotification(_U('full_service') .. inServiceCount .. '/' .. maxInService)
	
								end
	
							end, 'grocer')
	
						end
	
					end
	
				end

			end,
			function(data, menu)
	
				menu.close()
	
				CurrentAction     = ''
				CurrentActionMsg  = ''
				CurrentActionData = {}
	
			end
		)
	
	end

	

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  
  PlayerData = xPlayer

  if PlayerData.job.name == 'grocer' then
		Config.Zones.GrocerActions.Type = 1
		Config.Zones.VehicleDeleter.Type = 1
		Config.Zones.SpamActions.Type = 1
		Config.Zones.GrocerBoxItem.Type = 1
  else
		Config.Zones.GrocerActions.Type = -1
		Config.Zones.VehicleDeleter.Type = -1
		Config.Zones.SpamActions.Type = -1
		Config.Zones.GrocerBoxItem.Type = -1
  end

end)

RegisterNetEvent('esx:setJob')
AddEventHandler('esx:setJob', function(job)

  PlayerData.job = job

  if PlayerData.job.name == 'grocer' then
		Config.Zones.GrocerActions.Type = 1
		Config.Zones.VehicleDeleter.Type = 1
		Config.Zones.SpamActions.Type = 1
		Config.Zones.GrocerBoxItem.Type = 1
  else
		Config.Zones.GrocerActions.Type = -1
		Config.Zones.VehicleDeleter.Type = -1
		Config.Zones.SpamActions.Type = -1
		Config.Zones.GrocerBoxItem.Type = -1
  end

end)

RegisterNetEvent('esx_phone:loaded')
AddEventHandler('esx_phone:loaded', function(phoneNumber, contacts)

	local specialContact = {
		name       = 'Epicier',
		number     = 'grocer',
		base64Icon = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABcNJREFUeNrcm39I3HUYx19+71srSY1teo2wu80wuVM7y60kxv7wj2QSS4lRQ2pEJWNMSJHWJGoS9GMQhmiGlJujrRtMBvsRIiJRHkXJNs3Ydup+oBuHhtOa62zetz/uh3enzrv7fr7n93zg+ePz4fs8PO/n+3w+z+fzPN9vEtqTEXgGyAWeAp4A0oE04EHfM7PANDAO3ACuAAPAeeAWCUgFwIfAT8C/gBIjzwK9wEGgUO+g1wDvAA4VgJfjX4A9wMN6Ai4DtcCohsDD+RZwwOf0FaVdwEgcgYfzDeD1lQC+ATi1gsDD+SyQGS/wO4DbOgLv53+AV7QGX6dD4OF8UCvwXyUAeD9/Kxp8ewKB97NdFPiWBATv58NqwR9IYPB+ro8V/IurALyfdywFMmmJ+TTfxeSB5byUlpbmMplM6YDkn5MkiampKa5evaqX06oCZAATkQp0ReBVd319/XHlPtTU1KSnKPg5UvAvR6LQZrMNKRFQdna2npywKxystIgDvonES3fu3MlSFGXZ5ywWi54ubl8DhvCbXDBVA2sj0eR0OsnNzaWiooLU1FTm5ubweDyUlJSQk5MTeG7jxo16csAjwPvAx0ttiNNqw6ympkbP+4AC3A2qRIUsgTeAFLUudrlcIWOTyaS3ws1DwFuLpcF+IE+t9qKiIhwOR2A8OTlJe3s7SUlJQlEYDAZmZmaw2+309fVFK+4EsoMnckSFmNFoVDwejxJPslgssdhqC14Cr4p6My6Xi9HR0bjGdH5+fixirwU7oFSkQSMjI5qBnZ2dDRm73W46OztjUVXqT4NrgWdFGjk8PMy2bdsC40OHDtHa2orBYFANvrq6mr17984fWbu6mJycjEWdFdggA8/d504gJAI8Hg9Op1OTcO/o6FCj7nkJeFp0mA4NDYWMN2/eLERvVlYWW7duDZk7ffq0GpU2CW+7Ci0jYNOmTUL0lpWVhYx7e3uZmJhQozJbwturE+6Ae/fuBcaZmZmsX79etd7y8nKR4Y8fe78WR87h4eGQXL1lyxZV+tLT0xfkf7PZrNbOS5LvgiCcwoshRqNRlb7S0tBMPTg4yLVr19SamSYHXwxEp8Li4uLAuLi4mIGBASRJir6coyjs3r1bdPiDr8mqSVOztrZW0+OvzWYTYedtCfhPiwgIT4UiaXx8nAsXLohQdVcC/o7HHiCSGhsbRamalqOplEabCt1uN2vWzLfw7XY709PTMR2JJUlCURT6+/tpaGgQZeYEeLsnmlRfLl++HLJuMzIy9FYd+l7C+0ESWmWCYCooKNBbdeiKDFzUSnv4kXj//v0UFhYiy3JUemRZZmxsjJaWFtEmXgRYp1WIhRdI1dLJkydF2/i43xN9Wjhg+/btwvO/QPsGgytC57RYAt3d3XR1dQnRpSgK+/btE2neueCqsBX4Q6u9wGazkZycjMfjiVrWX02+efMm169fF2lWoS/yA/Qnq6cdvhwH0lPwzeQLrSLAZDKpapGZzWbMZrNIk75cbNKA91Mzod5ubm4ObGBHjx6NWr61tTUg39bWJsImN97u0KL0nkjwlZWVC3bxurq6iOWrqqoWyNfU1Ki166P77jnAlCgHnDlzZgEAh8MRsXxnZ+cC+Z6eHjU2zRD21Ut4dUIB3ha10BZrWESTFhd7VmVa3RPp9f9HUVFw5MiRwNvr6OhQZFmOSv7YsWMB+RMnTiiSJMVqy6/ReGotMCfKCRaLRcnLy4tZ3mq1KlarVa0dj0UbLi+tory/M9Y1c3AVgP9c7V52OIHBHxe1odsTEPwp0SfatgQC/51Wx/r6BAD/mdZ1tJ2+E5XegM8CFfEqJpqAH3QEvht4ciWqqm8CYysI3AVUrnRpORn4AO/n9fEC/pfvjJKCjigFqAJ+1xD4eeBd4FF0TkXAJ8BvAkD3AZ8CL2hhaFKcNswCIJ/53+fX+d5isu+ZGV8dYgJvu/4S87/Pa/rbyf8DAFrCzkYRijdSAAAAAElFTkSuQmCC'
	}

	TriggerEvent('esx_phone:addSpecialContact', specialContact.name, specialContact.number, specialContact.base64Icon)

end)

AddEventHandler('esx_grocerjob:hasEnteredMarker', function(zone)

  if zone == 'GrocerActions' and PlayerData.job ~= nil and PlayerData.job.name == 'grocer' then
    CurrentAction     = 'grocer_actions_menu'
    CurrentActionMsg  = 'Appuez sur ~INPUT_CONTEXT~ pour accéder au menu'
    CurrentActionData = {}
	end
	
	if zone == 'SpamActions' and PlayerData.job ~= nil and PlayerData.job.name == 'grocer' then
		CurrentAction     = 'grocer_spawn_actions'
		CurrentActionMsg  = 'Appuez sur ~INPUT_CONTEXT~ pour accéder au garage'
		CurrentActionData = {}
	end

	if zone == 'GrocerBoxItem' and PlayerData.job ~= nil and PlayerData.job.name == 'grocer' then
		CurrentAction     = 'Box_item_actions'
		CurrentActionMsg  = 'Appuez sur ~INPUT_CONTEXT~ pour accéder au stock Bahama Mas'
		CurrentActionData = {}
	end

	if zone == 'VehicleDeleter' and PlayerData.job ~= nil and PlayerData.job.name == 'grocer' then
		
		local playerPed = GetPlayerPed(-1)
		
		if IsPedInAnyVehicle(playerPed,  false) then

			local vehicle = GetVehiclePedIsIn(playerPed,  false)

			CurrentAction     = 'delete_vehicle'
			CurrentActionMsg  = 'Appuyez sur ~INPUT_CONTEXT~ pour ranger le véhicule.'
			CurrentActionData = {vehicle = vehicle}
		end
	end

end)

AddEventHandler('esx_grocerjob:hasExitedMarker', function(zone)
  CurrentAction = nil
  ESX.UI.Menu.CloseAll()
	TriggerServerEvent('esx_grocerjob:stopCraft')
	TriggerServerEvent('esx_grocerjob:stopCraft2')
	TriggerServerEvent('esx_grocerjob:stopCraft3')
	TriggerServerEvent('esx_grocerjob:stopCraft4')
end)

-- Create blips
Citizen.CreateThread(function()

 local blip = AddBlipForCoord(Config.Zones.Grocery.Pos.x, Config.Zones.Grocery.Pos.y, Config.Zones.Grocery.Pos.z)
  
  SetBlipSprite (blip, 52)
  SetBlipDisplay(blip, 4)
  SetBlipScale  (blip, 1.2)
	SetBlipColour (blip, 2)
  SetBlipAsShortRange(blip, true)
  
 	BeginTextCommandSetBlipName("STRING")
  AddTextComponentString("Epicerie Générale")
  EndTextCommandSetBlipName(blip)

end)

-- Display markers
Citizen.CreateThread(function()
  while true do
    
    Wait(0)
    
    local coords = GetEntityCoords(GetPlayerPed(-1))
    
    for k,v in pairs(Config.Zones) do
      if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
        DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
      end
    end

  end
end)

-- Enter / Exit marker events
Citizen.CreateThread(function()
  while true do
    
    Wait(0)
    
    local coords          = GetEntityCoords(GetPlayerPed(-1))
    local isInMarker      = false
    local currentZone     = nil

    for k,v in pairs(Config.Zones) do
      if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x) then
        isInMarker      = true
        currentZone     = k
      end
    end

    if (isInMarker and not HasAlreadyEnteredMarker) or (isInMarker and LastZone ~= currentZone) then
      HasAlreadyEnteredMarker = true
      LastZone                = currentZone
      TriggerEvent('esx_grocerjob:hasEnteredMarker', currentZone)
    end

    if not isInMarker and HasAlreadyEnteredMarker then
      HasAlreadyEnteredMarker = false
      TriggerEvent('esx_grocerjob:hasExitedMarker', LastZone)
    end

  end
end)

-- Key Controls
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(1)

    if CurrentAction ~= nil then

      SetTextComponentFormat('STRING')
      AddTextComponentString(CurrentActionMsg)
      DisplayHelpTextFromStringLabel(0, 0, 1, -1)

      if IsControlJustReleased(0,  Keys['E']) and PlayerData.job ~= nil and PlayerData.job.name == 'grocer' then
        
        if CurrentAction == 'grocer_actions_menu' then
          OpenGrocerActionsMenu()
				end
				
		if CurrentAction == 'grocer_spawn_actions' then
			OpenGrocerSpawn()
		end

		if CurrentAction == 'Box_item_actions' then
			OpenBahamaActionsMenu()
		end

				if CurrentAction == 'delete_vehicle' then
					
					if Config.EnableSocietyOwnedVehicles then
						if IsVehicleDamaged(GetVehiclePedIsIn(GetPlayerPed(-1))) then
							ESX.ShowNotification('Véhicule ~r~endomagé')
							ESX.ShowNotification('La réparation va couter a votre entreprise :~h~~r~600$')
							local elements = {
								{label = 'Réparer le vehicule', value = 'FixCar'}
							}
											
							ESX.UI.Menu.CloseAll()
											
							ESX.UI.Menu.Open(
								'default', GetCurrentResourceName(), '',
							{
								title    = 'Réparation',
								elements = elements
							},
						function(data, menu)
																							
							if data.current.value == 'FixCar' then
								menu.close()
								SetVehicleFixed(GetVehiclePedIsIn(GetPlayerPed(-1)))
								SetVehicleDeformationFixed(GetVehiclePedIsIn(GetPlayerPed(-1)))
								TriggerServerEvent('esx_grocer:garage')
							end
											
							end,
								function(data, menu)
								menu.close()
								CurrentAction     = ''
								CurrentActionMsg  = ''
								CurrentActionData = {}
							end
								)									
						else
							local vehicleProps = ESX.Game.GetVehicleProperties(CurrentActionData.vehicle)
							TriggerServerEvent('esx_society:putVehicleInGarage', 'grocer', vehicleProps)
						end		
						else
					
						if
							GetEntityModel(vehicle) == GetHashKey('')
						then
							TriggerServerEvent('esx_service:disableService', 'grocer')
						end
											
					end	

						if IsVehicleDamaged(GetVehiclePedIsIn(GetPlayerPed(-1))) then
						else
						ESX.Game.DeleteVehicle(CurrentActionData.vehicle)
						ESX.ShowNotification('Véhicule ~g~rangé')
						end
	
				end
        
        CurrentAction = nil
      end

    end

    if IsDisabledControlJustReleased(0, Keys['F6']) and PlayerData.job ~= nil and PlayerData.job.name == 'grocer' then
      OpenMobileGrocerActionsMenu()
    end

    if IsControlPressed(0,  Keys['LEFTSHIFT']) and IsControlJustReleased(0, Keys['G']) and PlayerData.job ~= nil and PlayerData.job.name == 'grocer' then
    
			local playerPed = GetPlayerPed(-1)
			local coords    = GetEntityCoords(playerPed)
			
			local closestPed, closestDistance = ESX.Game.GetClosestPed({
				x = coords.x,
				y = coords.y,
				z = coords.z
			}, {playerPed})

			-- Fallback code
			if closestDistance == -1 then
				
				print('Using fallback code to find ped')

				local success, ped = GetClosestPed(coords.x,  coords.y,  coords.z,  5.0, 1, 0, 0, 0,  26)

				if DoesEntityExist(ped) then
					local pedCoords = GetEntityCoords(ped)
					closestPed      = ped
					closestDistance = GetDistanceBetweenCoords(coords.x,  coords.y,  coords.z,  pedCoords.x,  pedCoords.y,  pedCoords.z,  true)
				end

			end

			if closestPed ~= -1 and closestDistance <= 5.0 then

				if IsPedInAnyVehicle(closestPed,  false) then
					ESX.ShowNotification('Action ~r~impossible~s~, cette personne est en voiture')
				else

					local playerData    = ESX.GetPlayerData()
					local isBlacklisted = false

					for i=1, #PedBlacklist, 1 do
						if PedBlacklist[i] == closestPed then
							isBlacklisted = true
						end
					end

					if isBlacklisted then
						ESX.ShowNotification('Vous avez déja traité avec ce client')
					else

						table.insert(PedBlacklist, closestPed)

						local resellable = {}

						for k,v in pairs(Config.ResellableItems) do
							for j=1, #playerData.inventory, 1 do
								if k == playerData.inventory[j].name and playerData.inventory[j].count > 0 then
									table.insert(resellable, {name = k, count = playerData.inventory[j].count})
								end
							end
						end

						if #resellable == 0 then
							ESX.ShowNotification('Vous n\'avez pas de marchandise à vendre')
						else

							local item     = resellable[GetRandomIntInRange(1,  #resellable)]
							local quantity = 5

							if quantity > item.count then
								quantity = item.count
							end

							TriggerServerEvent('esx_grocerjob:resell', item.name, quantity)

						end

					end

				end

			else
				ESX.ShowNotification('Aucun passant à proximité')
			end

    end

  end
end)

function DrawAdvancedTextCNN (x,y ,w,h,sc, text, r,g,b,a,font,jus)
    SetTextFont(font)
    SetTextProportional(0)
    SetTextScale(sc, sc)
    N_0x4e096588b13ffeca(jus)
    SetTextColour(r, g, b, a)
    SetTextDropShadow(0, 0, 0, 0,255)
    SetTextEdge(1, 0, 0, 0, 255)
    SetTextDropShadow()
    SetTextOutline()
    SetTextEntry("STRING")
    AddTextComponentString(text)
    DrawText(x - 0.1+w, y - 0.02+h)
end


 Citizen.CreateThread(function()
		    while true do
		        Citizen.Wait(1)    
		                       
		            if (affichenews == true) then
		           
				            DrawRect(0.494, 0.227, 5.185, 0.118, 0, 0, 0, 150)
				            DrawAdvancedTextCNN(0.588, 0.14, 0.005, 0.0028, 0.8, "~r~ EPICIER ~d~", 255, 255, 255, 255, 1, 0)
				            DrawAdvancedTextCNN(0.586, 0.199, 0.005, 0.0028, 0.6, texteafiche, 255, 255, 255, 255, 7, 0)
				            DrawAdvancedTextCNN(0.588, 0.246, 0.005, 0.0028, 0.4, "", 255, 255, 255, 255, 0, 0)

				    end                
		   end
		end)



RegisterNetEvent('esx_epicierjob:annonce')
AddEventHandler('esx_epicierjob:annonce', function(text)
 		texteafiche = text
 		affichenews = true
		
  end) 


RegisterNetEvent('esx_epicierjob:annoncestop')
AddEventHandler('esx_epicierjob:annoncestop', function()
 		affichenews = false
 		
  end)