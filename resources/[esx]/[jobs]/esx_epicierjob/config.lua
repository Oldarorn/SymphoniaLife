Config              = {}
Config.DrawDistance = 100.0
Config.EnableSocietyOwnedVehicles   = true
Config.EnablePlayerManagement	= true

Config.ResellableItems = {
	bread      	= 15,
	water      	= 15,
	beer       	= 25,
	cigaret    	= 20,
	fishburger 	= 20,
	ckiburger  	= 20,
	orangejus  	= 20,
	grapesjus  	= 20,
	grape      	= 20,
	orange     	= 20,
	cocacola    = 20,
	redbull 	= 40,
}

Config.Zones = {
	
	Grocery = {
		Pos   = {x = -3041.009, y = 588.693, z = 7.905},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = -1
	},

	GrocerActions = {
		Pos   = {x = -3046.645, y = 584.055, z = 6.908},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 204, g = 204, b = 0},
		Type  = -1
	},
	
	VehicleSpawnPoint = {
		Pos   = {x = -3028.57, y = 573.361, z = 6.79448},
		Size  = {x = 1.5, y = 1.5, z = 1.0},
		Type  = -1
	},

	VehicleDeleter = {
		Pos   = {x = -3028.57, y = 573.361, z = 6.79448},
		Size  = {x = 3.0, y = 3.0, z = 1.0},
		Color = {r = 255, g = 0, b = 0},
		Type  = 1
	},

	SpamActions = {
		Pos   = {x = -3029.18, y = 568.836, z = 6.79448},
		Size  = {x = 1.5, y = 1.5, z = 0.8},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	},

	GrocerBoxItem ={ 
		Pos   = {x = -1403.58, y = -629.524, z = 27.6734},
		Size  = {x = 1.5, y = 1.5, z = 0.8},
		Color = {r = 204, g = 204, b = 0},
		Type  = 1
	}

}