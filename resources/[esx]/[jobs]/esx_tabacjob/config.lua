Config                            = {}
Config.DrawDistance               = 100.0
Config.MaxInService               = -1
Config.EnablePlayerManagement     = true
Config.EnableSocietyOwnedVehicles = false
Config.EnableLicenses             = false
Config.EnableESXIdentity          = false
Config.Locale                     = 'fr'

Config.Cig = {
  'malbora',
  'gitanes'
}

Config.CigResellChances = {
  malbora = 45,
  gitanes = 55,
}

Config.CigResellQuantity= {
  malbora = {min = 5, max = 10},
  gitanes = {min = 5, max = 10},
}

Config.CigPrices = {
  malbora = {min = 35, max = 65},
  gitanes = {min = 25,   max = 55},
}

Config.CigPricesHigh = {
  malbora = {min = 65, max = 150},
  gitanes = {min = 55,   max = 100},
}

Config.Time = {
	malbora = 5 * 60,
	gitanes = 5 * 60,
}

Config.Zones = {
  
  TabacActions = {
    Pos   = { x = 2340.6191, y = 3124.6501, z = 48.0087 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 23,
  },

  Garage = {
    Pos   = { x = 501.33618, y = 6478.11962, z = 30.5222 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 23,
  },

  Craft = {
    Pos   = { x = 2346.3117, y = 3146.1376, z = 48.0087 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 27,
  },

  Craft2 = {
    Pos   = { x = 2356.1296, y = 3114.8027, z = 48.0087 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 27,
  },

  VehicleSpawnPoint = {
    Pos   = { x = 2371.8051, y = 3127.9262, z = 48.1384 },
    Size  = { x = 1.5, y = 1.5, z = 1.0 },
    Type  = -1,
  },

  VehicleDeleter = {
    Pos   = { x = 2363.0761, y = 3113.7097, z = 48.2520 },
    Size  = { x = 3.0, y = 3.0, z = 1.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = 1,
  },

  VehicleDelivery = {
    Pos   = { x = -382.925, y = -133.748, z = 37.685 },
    Size  = { x = 20.0, y = 20.0, z = 3.0 },
    Color = { r = 204, g = 204, b = 0 },
    Type  = -1,
  },
  
}
