Config                            = {}
Config.Locale                     = 'en'


Config.Volume = 0.5 				-- 0.1 , 1.0
Config.EnablePrice = true -- false = bikes for free
Config.EnableEffects = true
Config.EnableSoundEffects = false
Config.EnableBlips = true

	
Config.PriceTriBike = 89
Config.PriceScorcher = 99
Config.PriceCruiser = 129
Config.PriceBmx = 109

Config.EnableBuyOutfits = false -- WIP !!!!
	
Config.MarkerZones = { 

    {x = -246.980,y = -339.820,z = 29.000},

}


-- Edit blip titles
Config.BlipZones = { 

   {title="Ve-Loc", colour=46, id=226, x = -248.938, y = -339.955, z = 29.969},

}
