ESX = nil
TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
local Vehicles = nil

RegisterServerEvent('esx_lscustombike:buyMod')
AddEventHandler('esx_lscustombike:buyMod', function(price)
    local _source = source
    local xPlayer = ESX.GetPlayerFromId(_source)
    price = tonumber(price)

    if  Config.IsMecanoJobOnly == true then
        local societyAccount = nil
        TriggerEvent('esx_addonaccount:getSharedAccount', 'society_biker', function(account)
            societyAccount = account
        end)
        if price < societyAccount.money then
            TriggerClientEvent('esx_lscustombike:installMod', _source)
            TriggerClientEvent('esx:showNotification', _source, _U('purchased'))
        else
            TriggerClientEvent('esx_lscustombike:cancelInstallMod', _source)
            TriggerClientEvent('esx:showNotification', _source, _U('not_enough_money'))
        end

    else
        if price < xPlayer.getMoney() then
            TriggerClientEvent('esx_lscustombike:installMod', _source)
            TriggerClientEvent('esx:showNotification', _source, _U('purchased'))
        else
            TriggerClientEvent('esx_lscustombike:cancelInstallMod', _source)
            TriggerClientEvent('esx:showNotification', _source, _U('not_enough_money'))
        end
    end
end)

RegisterServerEvent('esx_lscustombike:refreshOwnedVehicle')
AddEventHandler('esx_lscustombike:refreshOwnedVehicle', function(myCar)

	MySQL.Async.execute(
		'UPDATE `owned_vehicles` SET `vehicle` = @vehicle WHERE `vehicle` LIKE "%' .. myCar['plate'] .. '%"',
		{
			['@vehicle'] = json.encode(myCar)
		}
	)
end)

ESX.RegisterServerCallback('esx_lscustombike:getVehiclesPrices', function(source, cb)

	if Bikes == nil then
		MySQL.Async.fetchAll(
			'SELECT * FROM bikes',
			{},
			function(result)
				local bikes = {}
				for i=1, #result, 1 do
					table.insert(bikes,{
						model = result[i].model,
						price = result[i].price
					})
				end
				Bikes = bikes
				cb(Bikes)
			end
		)		
	else
		cb(Bikes)
	end
end)