--Truck
Config	=	{}

 -- Limit, unit can be whatever you want. Originally grams (as average people can hold 25kg)
Config.Limit = 25000

-- Default weight for an item:
	-- weight == 0 : The item do not affect character inventory weight
	-- weight > 0 : The item cost place on inventory
	-- weight < 0 : The item add place on inventory. Smart people will love it.
Config.DefaultWeight = 0



-- If true, ignore rest of file
Config.WeightSqlBased = false

-- I Prefer to edit weight on the config.lua and I have switched Config.WeightSqlBased to false:

Config.localWeight = {
    stone = 10000,
    washed_stone = 8000,
    copper = 200,
    iron = 400,
    gold = 600,
    diamond = 800,
    wood = 15000,
    cutted_wood = 7000,
    packaged_plank = 5000,
    whool = 100,
    fabric = 50,
    clothe = 25,
	bread = 125,
	water = 330,
    fish = 100,
    alive_chicken = 300,
    slaughtered_chicken = 250,
    packaged_chicken = 150,
    petrol = 8000,
    petrol_raffin = 7000,
    essence = 5000,
    gazbottle = 2000,
    fixtool = 50,
    carotool = 50,
    blowpipe = 50,
    fixkit = 150,
    carokit = 150, 
    weed = 40,
    weed_pooch = 25,
	WEAPON_COMBATPISTOL = 1000, -- poids pour une munition
	black_money = 1, -- poids pour un argent
}

Config.VehicleLimit = {
    [0] = 30000, --Compact
    [1] = 40000, --Sedan
    [2] = 70000, --SUV
    [3] = 25000, --Coupes
    [4] = 30000, --Muscle
    [5] = 10000, --Sports Classics
    [6] = 5000, --Sports
    [7] = 5000, --Super
    [8] = 5000, --Motorcycles
    [9] = 180000, --Off-road
    [10] = 300000, --Industrial
    [11] = 70000, --Utility
    [12] = 100000, --Vans
    [13] = 0, --Cycles
    [14] = 5000, --Boats
    [15] = 20000, --Helicopters
    [16] = 0, --Planes
    [17] = 40000, --Service
    [18] = 40000, --Emergency
    [19] = 0, --Military
    [20] = 300000, --Commercial
    [21] = 0, --Trains
}