Config                      = {}
Config.DrawDistance         = 100.0
Config.MarkerType           = 27
Config.MarkerSize           = {x = 1.5, y = 1.5, z = 1.0}
Config.MarkerColor          = {r = 204, g = 204, b = 0}
Config.ParkingMarkerSize    = {x = 2.0, y = 2.0, z = 1.5}
Config.ParkingMarkerColor   = {r = 102, g = 102, b = 204}
Config.ZDiff                = 0.5
Config.EnableOwnedVehicles  = false
Config.MinimumHealthPercent = 0

Config.Locale = 'fr'

Config.Zones = {}

Config.Garages = {

	MiltonDrive = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x= -796.542, y = 318.137, z = 84.673},
			Type = 27
		},

		ExteriorSpawnPoint = {
			Pos     = {x = -796.501, y = 302.271, z = 85.000},
			Heading = 180.0,
			Type = 27
		},

		InteriorSpawnPoint = {
			Pos     = {x = 228.930, y = -1000.698, z = -100.000},
			Heading = 0.0,
			Type = 27
		},

		InteriorExitPoint = {
			Pos = {x = 224.613, y = -1004.769, z = -100.000},
			Type = 27
		},

		Parkings = {
			{
				Pos     = {x = 224.500, y = -998.695, z = -100.000},
				Heading = 225.0,
				Type = 23
			},
		  {
				Pos     = {x = 224.500, y = -994.630, z = -100.000},
				Heading = 225.0,
				Type = 23
			},
		  {
				Pos     = {x = 224.500, y = -990.255, z = -100.000},
				Heading = 225.0,
				Type = 23
			},
		  {
				Pos     = {x = 224.500, y = -986.628, z = -100.000},
				Heading = 225.0,
				Type = 23
			},
		  {
				Pos     = {x = 224.500, y = -982.496, z = -100.000},
				Heading = 225.0,
				Type = 23
			},
		  {
				Pos     = {x = 232.500, y = -982.496, z = -100.000},
				Heading = 135.0,
				Type = 23
			},
		  {
				Pos     = {x = 232.500, y = -986.628, z = -100.000},
				Heading = 135.0,
				Type = 23
			},
		  {
				Pos     = {x = 232.500, y = -990.255, z = -100.000},
				Heading = 135.0,
				Type = 23
			},
		  {
				Pos     = {x = 232.500, y = -994.630, z = -100.000},
				Heading = 135.0,
				Type = 23
			},
		  {
				Pos     = {x = 232.500, y = -998.695, z = -100.000},
				Heading = 135.0,
				Type = 23
			},
		}
	},	

--[[
	BunkerPark = {

		IsClosed = true,

		ExteriorEntryPoint = {
			Pos = {x= -480.414, y = 6259.865, z = 12.020},
			Type = 27
		},

		ExteriorSpawnPoint = {
			Pos     = {x = -488.854, y = 6262.873, z = 12.408, a = 67.368},
			Heading = 180.0,
			Type = 27
		},

		InteriorSpawnPoint = {
			Pos     = {x = 884.8750, y = -3245.2236, z = -98.070},
			Heading = 82.0,
			Type = -1
		},

		InteriorExitPoint = {
			Pos = {x = 890.125, y = -3245.2019, z = -99.070},
			Type = 27
		},

		Parkings = {
			{
				Pos     = {x = 830.830, y = -3234.545, z = -99.399},
				Heading = 207.0,
				Type = 23
			},
		  {
				Pos     = {x = 837.769, y = -3233.545, z = -99.399},
				Heading = 207.0,
				Type = 23
			},
		  {
				Pos     = {x = 842.117, y = -3233.545, z = -99.399},
				Heading = 207.0,
				Type = 23
			},
		  {
				Pos     = {x = 845.821, y = -3234.208, z = -99.399},
				Heading = 207.0,
				Type = 23
			},
		}
	}	
--]]	
}
