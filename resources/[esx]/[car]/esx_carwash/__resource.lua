description 'esx_carwash.lua'

server_scripts {

    '@es_extended/locale.lua',
    'locales/fr.lua',
    'server/esx_carwash.lua',
    'config.lua'
}

client_scripts {

    '@es_extended/locale.lua',
    'locales/fr.lua',
    'config.lua',
    'client/esx_carwash.lua',
}