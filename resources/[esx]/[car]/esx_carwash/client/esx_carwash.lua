local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57, 
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177, 
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70, 
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}

local PID           			= 0
local GUI           			= {}
ESX 			    			= nil
GUI.Time            			= 0
local PlayerData 				= {}
local GUI 						= {}
local HasAlreadyEnteredMarker   = false
local LastZone                  = nil
local CurrentAction             = nil
local CurrentActionMsg          = ''
local CurrentActionData         = {}

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
end)

AddEventHandler('esx_carwash:hasEnteredMarker', function(zone)
    
   ESX.UI.Menu.CloseAll()

    for i=1, #Config.CarWash, 1 do       
        if zone == 'CarWash' .. i and (IsPedSittingInAnyVehicle(GetPlayerPed(-1)) ) then
          CurrentAction     = 'CarWash_Open'
          CurrentActionMsg  = _U('press_wash')
          CurrentActionData = {}
        end
    end
    
    for i=1, #Config.BoosAction, 1 do       
        if zone == 'BoosAction' .. i then
          CurrentAction     = 'CarWash_Open_Boss'
          CurrentActionMsg  = _U('press_boss')
          CurrentActionData = {}
        end
    end

end)

AddEventHandler('esx_carwash:hasExitedMarker', function(zone)

	CurrentAction = nil
	ESX.UI.Menu.CloseAll()

end)

----------------------------------------------------------------------
-------------------------------- Para --------------------------------
----------------------------------------------------------------------
function OpenCarwashMenu()
    
          local elements = {

            {label = 'Lavage Auto / Moto 250$', value = 'wash'},
          }
      
          ESX.UI.Menu.CloseAll()
      
          ESX.UI.Menu.Open(
            'default', GetCurrentResourceName(), 'CarWash_Open',
            {
              title    = 'Lavage Auto / Moto',
              elements = elements
            },
            function(data, menu)
              if data.current.value == 'wash' then
                if IsVehicleDamaged(GetVehiclePedIsIn(GetPlayerPed(-1))) then
                  ESX.ShowNotification('Véhicule ~r~endomagé')
                  ESX.UI.Menu.CloseAll()
                else  
                  SetVehicleDirtLevel(GetVehiclePedIsIn(GetPlayerPed(-1),  false), 0)
                  SetVehicleFixed(GetVehiclePedIsIn(GetPlayerPed(-1), false))
                  TriggerServerEvent('esx_carwash:wash')
                  ESX.ShowNotification('Véhicule ~g~nettoyé')
                  ESX.UI.Menu.CloseAll()
                end
    
              end
              
            end,
      
    function(data, menu)
        menu.close()
        CurrentAction     = 'CarWash_Open'
        CurrentActionData = {}
    end)
end

function OpenCarwashMenuBoss()

    TriggerEvent('esx_society:openBossMenu', 'wash', function(data, menu)
      menu.close()
    end, {
      withdraw  = true,
      deposit   = true,
      wash      = false,
      employees = false,
      grades    = false,
      stocks    = false,
    })
end


----------------------------
------ RENDER MARKERS ------
----------------------------
Citizen.CreateThread(function()
    while true do

        Wait(0)

        local coords = GetEntityCoords(GetPlayerPed(-1))

        for k,v in pairs(Config.Zones) do
            if(v.Type ~= -1 and GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < Config.DrawDistance) then
                DrawMarker(v.Type, v.Pos.x, v.Pos.y, v.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, v.Size.x, v.Size.y, v.Size.z, v.Color.r, v.Color.g, v.Color.b, 100, false, true, 2, false, false, false, false)
            end
        end
    end
end)

-- Activate menu when player is inside marker
Citizen.CreateThread(function()
    while true do
        
        Wait(0)
        
        local coords      = GetEntityCoords(GetPlayerPed(-1))
        local isInMarker  = false
        local currentZone = nil

        for k,v in pairs(Config.Zones) do
            if(GetDistanceBetweenCoords(coords, v.Pos.x, v.Pos.y, v.Pos.z, true) < v.Size.x / 2) then
                isInMarker  = true
                currentZone = k
            end
        end

        if isInMarker and not HasAlreadyEnteredMarker then
            HasAlreadyEnteredMarker = true
            LastZone                = currentZone
            TriggerEvent('esx_carwash:hasEnteredMarker', currentZone)
        end

        if not isInMarker and HasAlreadyEnteredMarker then
            HasAlreadyEnteredMarker = false
            TriggerEvent('esx_carwash:hasExitedMarker', LastZone)
        end
    end
end)


-- Key Controls
Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		if CurrentAction ~= nil then
			SetTextComponentFormat('STRING')
			AddTextComponentString(CurrentActionMsg)
			DisplayHelpTextFromStringLabel(0, 0, 1, -1)
    if IsControlJustReleased(0, 38) then
        
        if CurrentAction == 'CarWash_Open' then
        OpenCarwashMenu()
        end

        if CurrentAction == 'CarWash_Open_Boss' then
            OpenCarwashMenuBoss()
        end
				CurrentAction = nil				
			end
		end
	end
end)

--------------------------------------------------------------------
------------------------------ BLIPS--------------------------------
--------------------------------------------------------------------

local blips = {
    {title="Car Wash", id=100, x= 26.5906,  y= -1392.0261, z= 28.3634},
    {title="Car Wash", id=100, x= 167.1034, y=  -1719.4704, z= 28.2916},
    {title="Car Wash", id=100, x= -74.5693,  y= 6427.8715,  z= 30.4400},
    {title="Car Wash", id=100, x= -699.6325, y=  -932.7043, z= 18.0139}
}

Citizen.CreateThread(function()

    for _, info in pairs(blips) do
      info.blip = AddBlipForCoord(info.x, info.y, info.z)
      SetBlipSprite(info.blip, info.id)
      SetBlipDisplay(info.blip, 4)
      SetBlipScale(info.blip, 0.7)
      SetBlipColour(info.blip, info.colour)
      SetBlipAsShortRange(info.blip, true)
	  BeginTextCommandSetBlipName("STRING")
      AddTextComponentString(info.title)
      EndTextCommandSetBlipName(info.blip)
    end
end)

