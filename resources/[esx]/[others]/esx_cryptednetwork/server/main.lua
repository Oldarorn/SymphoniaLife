ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function OnCryptedPhoneItemChange(source)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local found      = false
	local cryptedphone = xPlayer.getInventoryItem('cryptedphone')

	if cryptedphone.count > 0 then
		found = true
	end

	if found then
		TriggerEvent('esx_cryptedphone:addSource', 'crypted', source)
	else
		TriggerEvent('esx_cryptedphone:removeSource', 'crypted', source)
	end

	TriggerClientEvent('esx_cryptedphone:onHasCryptedphone', source, found)

end

AddEventHandler('esx:playerLoaded', function(source)

	local xPlayer    = ESX.GetPlayerFromId(source)
	local cryptedphone = xPlayer.getInventoryItem('cryptedphone')

	if cryptedphone.count > 0 then
		TriggerEvent('esx_cryptedphone:addSource', 'crypted', source)
	end

end)

AddEventHandler('esx:playerDropped', function(source)
	TriggerEvent('esx_cryptedphone:removeSource', 'crypted', source)
end)

AddEventHandler('esx:onAddInventoryItem', function(source, item, count)
	if item.name == 'cryptedphone' then
		OnCryptedPhoneItemChange(source)
	end
end)

AddEventHandler('esx:onRemoveInventoryItem', function(source, item, count)
	if item.name == 'cryptedphone' then
		OnCryptedPhoneItemChange(source)
	end
end)

TriggerEvent('esx_cryptedphone:registerNumber', 'crypted', 'Réseau Crypté', true, false, true, true)
