----------------------------------------------------------------------
----------------------- Developped by AlphaKush ----------------------
----------------------------------------------------------------------

Config              = {}
Config.DrawDistance = 100.0
Config.MarkerColor  = {r = 120, g = 120, b = 240}
Config.Locale = 'fr'

Config.Zones = {
	---------------- Prison ------------------

    EnterPrisoncop = { -- marker entré dans le parloir
        Pos = { x = 462.77740478516, y = -1009.7374267578, z = 24.562419891357},
        Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 0, g = 204, b = 3},
		Type  = 1
    },

    ExitPrison = { -- marker quitter le parloir prisonnier
        Pos = { x = 1694.9205, y = 2518.7785, z = -122.1498},
        Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 0, g = 204, b = 3},
		Type  = 1
    },

    SpawnPrisonComico = { -- spawn Prison to Comico PoliceC
        Pos = { x = 462.77740478516, y = -1009.7374267578, z = 24.562419891357},
        Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 0, g = 204, b = 3},
		Type  = -1
    },

    SpawnInterieurPrison = { -- spawn interieur de la prison
        Pos = { x = 1678.1314, y = 2518.6730, z = -120.9499},
        Size  = {x = 1.5, y = 1.5, z = 1.0},
		Color = {r = 0, g = 204, b = 3},
		Type  = -1
    },
}
