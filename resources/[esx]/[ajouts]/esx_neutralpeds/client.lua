local entityEnumerator = {
  __gc = function(enum)
    if enum.destructor and enum.handle then
      enum.destructor(enum.handle)
    end
    enum.destructor = nil
    enum.handle = nil
  end
}

local function EnumerateEntities(initFunc, moveFunc, disposeFunc)
  return coroutine.wrap(function()
    local iter, id = initFunc()
    if not id or id == 0 then
      disposeFunc(iter)
      return
    end
    
    local enum = {handle = iter, destructor = disposeFunc}
    setmetatable(enum, entityEnumerator)
    
    local next = true
    repeat
      coroutine.yield(id)
      next, id = moveFunc(iter)
    until not next
    
    enum.destructor, enum.handle = nil, nil
    disposeFunc(iter)
  end)
end

function EnumerateObjects()
  return EnumerateEntities(FindFirstObject, FindNextObject, EndFindObject)
end

function EnumeratePeds()
  return EnumerateEntities(FindFirstPed, FindNextPed, EndFindPed)
end

function EnumerateVehicles()
  return EnumerateEntities(FindFirstVehicle, FindNextVehicle, EndFindVehicle)
end

function EnumeratePickups()
  return EnumerateEntities(FindFirstPickup, FindNextPickup, EndFindPickup)
end

Citizen.CreateThread(function()

	while true do
		
    Citizen.Wait(0)
		
    for ped in EnumeratePeds() do

      if not IsPedAPlayer(ped) then

  			local pos       = GetEntityCoords(ped)
  			local ply       = GetPlayerPed(-1)
  			local plyCoords = GetEntityCoords(ply, 0)
  			local distance  = GetDistanceBetweenCoords(pos.x, pos.y, pos.z,  plyCoords["x"], plyCoords["y"], plyCoords["z"], true)

  			if(distance <= 250) then
  				
          SetPedAlertness(ped, 0)
  				
          if IsPedInCombat(ped, ply) then
            TaskCower(ped, 1)
  					SetPedCombatMovement(ped, 0)
          elseif IsPedShooting(ped) or IsPedTracked(ped)  then
  					TaskCower(ped, 1)
  				end

  			end

      end

		end
	end

end)