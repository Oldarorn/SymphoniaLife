Locales['fr'] = {

	['bought']           = 'vous avez acheté ~g~1x~b~ ',
	['not_enough']       = '~r~vous n\'avez pas assez d\'argent.',
	['press_menu']       = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au magasin.',
	['press_menu_mafia'] = 'appuyez sur ~INPUT_CONTEXT~ pour accéder au magasin.\n~INPUT_REPLAY_SCREENSHOT~ pour racketter',
	['racket_canceled']  = 'racket annulé',
	['racketting']       = 'racket en cours...',
	['shop']             = 'magasin',
	['shops']            = 'magasins',

}
