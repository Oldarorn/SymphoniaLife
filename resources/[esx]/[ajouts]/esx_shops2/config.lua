Config = {}

Config.DrawDistance = 100
Config.Size         = {x = 1.5, y = 1.5, z = 1.5}
Config.Color        = {r = 0, g = 128, b = 255}
Config.Type         = 1
Config.Locale       = 'fr'


Config.Zones = {
  TwentyFourSeven = {
    Items = {},
    Pos = {
     --{x = 373.875,   y = 325.896,  z = 102.566}
    }
  },

  RobsLiquor = {
    Items = {},
    Pos = {
      --{x = -559.906,  y = 287.093,   z = 81.176}  --Bahamamas ????
    }
  },

  LTDgasoline = {
    Items = {},
    Pos = {
      --{x = 1698.388,  y = 4924.404,  z = 41.063}
    }
  },

  ShopAero = {
    Items = {},
    Pos = {
      {x = -1038.669,   y = -2731.113, z = 19.069}
    }
  },

  UpgradeArmes = {
    Items = {},
    Pos = {
      {x = 1691.6618652,   y = 3757.926269,  z = 34.3053},
      {x = 19.38835144,  y = -1106.208618,  z = 29.197023},
      {x = -664.448,   y = -935.001,  z = 21.529},
      {x = 812.526,   y = -2157.765,  z = 29.319}
    }
  },

  ShopLicense = {
    Items = {},
    Pos = {
      --{x = 120.986,   y = -2468.724,  z = 5.094}
    }
  },

  UpgradeArmesPolice = {
    Items = {},
    Pos = {
      {x = 461.57238,   y = -979.61309,  z = 30.3895},
    }
  }
}
