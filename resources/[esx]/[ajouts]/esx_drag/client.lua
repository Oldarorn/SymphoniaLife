local isDragging = false
local obj = -1
local draggedPed = -1
local lastDraggedPed = -1
local boneToDrag = -1

Citizen.CreateThread(function()
  ClearPedTasksImmediately(GetPlayerPed(-1))
  while ESX == nil do
    TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
    Citizen.Wait(1)
  end
  while true do
    Citizen.Wait(1)

    if(isDragging) then
      ProcessEntityAttachments(obj)

      Info("Appuyez sur ~g~G~w~ pour ~r~arrêter~w~ de trainer le corps.")

      if(IsControlJustPressed(1, 47)) then
      	--ClearPedSecondaryTask(draggedPed)
        ClearPedSecondaryTask(GetPlayerPed(-1))
        --ApplyDamageToPed(draggedPed, 10000)
        spawnAttachedProp()
      end
    else



      if(SearchForDraggingPed() and not IsPedInAnyVehicle(GetPlayerPed(-1))) then
        Info("Appuyez sur ~g~G~w~ pour ~g~trainer~w~ le corps.")

        if(IsControlJustPressed(1, 47)) then
          spawnAttachedProp()
        end
      end
    end

  end

end)

Citizen.CreateThread(function()

	while true do
		Citizen.Wait(1)
		if(lastDraggedPed ~= -1) then
			Citizen.Wait(20000)
			--SetPedToRagdoll(lastDraggedPed, -1, -1, 1,1,1,1)
      		--ApplyForceToEntity(lastDraggedPed, 1, GetEntityCoords(lastDraggedPed), 0,0,0, 1 , false, true, false, true, true)
      		print("unfreeze")
		end
	end

end)

function SearchForDraggingPed()
  draggedPed = -1
  boneToDrag = -1

  

  local testPed = GetPedNearbyPeds(GetPlayerPed(-1))

  for k in EnumeratePeds()do
    local curPed = k
    if(GetDistanceBetweenCoords(GetEntityCoords(GetPlayerPed(-1)), GetEntityCoords(curPed), true) < 1.5 and curPed ~= GetPlayerPed(-1) and not IsPedDeadOrDying(GetPlayerPed(-1))) then

      if(IsEntityStatic(curPed)) then
      	--setPedUnStatic(curPed)

      end

      if(IsEntityDead(curPed) and IsPedHuman(curPed)--[[ and not IsEntityStatic(curPed)]]) then
        draggedPed = curPed
        SetEntityDynamic(draggedPed, true)
      end

    end
  end

  

  if(not IsPedInAnyVehicle(draggedPed)) then
    local boneDistanceArray = {}

    boneDistanceArray[14201] = GetDistanceBetweenCoords(GetPedBoneCoords(GetPlayerPed(-1), 18905, 0.0, 0.0, 0.0), GetPedBoneCoords(draggedPed, 14201, 0.0, 0.0, 0.0), true)
    boneDistanceArray[52301] = GetDistanceBetweenCoords(GetPedBoneCoords(GetPlayerPed(-1), 57005, 0.0, 0.0, 0.0), GetPedBoneCoords(draggedPed, 52301, 0.0, 0.0, 0.0), true)

    if(not IsPedCuffed(draggedPed)) then
      boneDistanceArray[61163] = GetDistanceBetweenCoords(GetPedBoneCoords(GetPlayerPed(-1), 18905, 0.0, 0.0, 0.0), GetPedBoneCoords(draggedPed, 61163, 0.0, 0.0, 0.0), true)
      boneDistanceArray[28252] = GetDistanceBetweenCoords(GetPedBoneCoords(GetPlayerPed(-1), 57005, 0.0, 0.0, 0.0), GetPedBoneCoords(draggedPed, 28252, 0.0, 0.0, 0.0), true)
    end


    local shorterBone = 14201
    local shorterDistance = 10
    for i,k in pairs(boneDistanceArray) do
      if(shorterDistance > k) then
        shorterBone = i
        shorterDistance = k
      end
    end

    boneToDrag = shorterBone


    if(shorterDistance > 3) then
      --print("short = "..shorterDistance)
      draggedPed = nil
    end

    if(draggedPed ~= nil and DoesEntityExist(draggedPed)) then
      return true
    else
      return false
    end
  end
end

function spawnAttachedProp()
  local nearbyProp = GetClosestObjectOfType(GetEntityCoords(GetPlayerPed(-1)), 2.0, GetHashKey("prop_candy_pqs"))
  local attached = false


  if(nearbyProp ~= nil) then
    if(IsEntityAttached(nearbyProp)) then
      --DeleteObject(nearbyProp)
      DetachEntity(draggedPed, true, true)
      DeleteObject(nearbyProp)
      --ClearPedTasksImmediately(draggedPed)
      
      --SetEntityCollision(draggedPed, true, true)
      --ActivatePhysics(draggedPed)
      --SetEntityDynamic(draggedPed, true)
      --SetEntityCoords(draggedPed, GetOffsetFromEntityInWorldCoords(draggedPed, 0.0, 0.0, 1.0))
      --SetEntityHealth(draggedPed, 0)
      Citizen.Wait(8000)
      ClearPedTasks(draggedPed)
      ApplyForceToEntity(draggedPed, 1, GetEntityCoords(draggedPed), 0,0,0, 1 , false, true, false, true, true)
      print("clear : "..draggedPed)
  	  --SetEntityHealth(ped, 0)
      lastDraggedPed = draggedPed
      isDragging = false
      attached = true
    end
  end


  if(not attached) then
    local playerPed = GetPlayerPed(-1)

    obj = CreateObject(GetHashKey("prop_candy_pqs"),GetEntityCoords(playerPed),  true,  true, true)
    AttachEntityToEntity(obj, playerPed, GetPedBoneIndex(playerPed, 60309), 0.12, 0.028, 0.001, 10.0, 175.0, 0.0, true, true, false, true, 1, true)
    --AttachEntityToEntity(entity1, entity2, boneIndex, xPos, yPos, zPos, xRot, yRot, zRot, p9, useSoftPinning, collision, isPed, vertexIndex, fixedRot)
    SetEntityAlpha(obj, 100)
    DragBody(draggedPed, boneToDrag, false)
  end
end

function DragBody(body, boneToDrag, fixRot)

  local player = GetPlayerPed(-1)

  GiveWeaponToPed(player, -1569615261, 12, true, true)
  --SetEntityNoCollisionEntity(body, player, true)

  loadAnimDict("combat@drag_ped@")

  




  setPedUnStatic(body)
  
  Citizen.Wait(10)
  if(boneToDrag == 28252 or boneToDrag == 61163) then  
  	--SetEntityCoords(player, GetOffsetFromEntityInWorldCoords(obj, 0.5, 0.0, 0.0))
  	--xRot, yRot, zRot = table.unpack(GetEntityRotation(obj))
    --SetEntityHeading(player, zRot)
    AttachEntityToEntityPhysically(body, obj, GetPedBoneIndex(body, 28252), 0, 0.0, -0.1, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000000.0, fixRot, 1, 0, 0, 1)
    AttachEntityToEntityPhysically(body, obj, GetPedBoneIndex(body, 61163), 0, -0.1, -0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000000.0, fixRot, 1, 0, 0, 1)
  elseif(boneToDrag==14201 or boneToDrag == 52301) then
  	--SetEntityCoords(player, GetOffsetFromEntityInWorldCoords(body, 0.5, 0.0, 0.0))
    --xRot, yRot, zRot = table.unpack(GetEntityRotation(body))
    --SetEntityHeading(player, zRot+180)
    AttachEntityToEntityPhysically(body, obj, GetPedBoneIndex(body, 57717), 0, -0.1, -0.4, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000000.0, fixRot, 1, 0, 0, 1)
    AttachEntityToEntityPhysically(body, obj, GetPedBoneIndex(body, 24806), 0, 0.0, -0.02, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1000000.0, fixRot, 1, 0, 0, 1)
  end

  print("===========")
  print("obj : "..obj)
  print("body : "..body)
  print("dragged : "..draggedPed)
  print("static ? "..IsEntityStatic(obj))
  print("bone : "..boneToDrag)

  TaskPlayAnim(player, "combat@drag_ped@", "injured_drag_plyr", 8.0, 2.0, -1, 33)

  isDragging = true
end



function Info(text, loop)
  SetTextComponentFormat("STRING")
  AddTextComponentString(text)
  DisplayHelpTextFromStringLabel(0, loop, 1, 0)
end


function loadAnimDict( dict )
    while ( not HasAnimDictLoaded( dict ) ) do
        RequestAnimDict( dict )
        Citizen.Wait(10)
    end
end


function setPedUnStatic(ped)
  local loc = GetEntityCoords(ped)
  local rot = GetEntityRotation(ped)
  ResurrectPed(ped)
  ClearPedTasks(ped)
  SetEntityHealth(ped, 0)
  ApplyForceToEntity(ped, 1, GetEntityCoords(ped), 0,0,0, 1 , false, true, false, true, true)
  --ApplyForceToEntity(entity, forceType, x, y, z, xRot, yRot, zRot, p8, isRel, p10, highForce, p12, p13)
  SetEntityDynamic(ped, true)
  SetEntityCoords(ped, loc, rot)
  --ApplyPedDamagePack(ped, "BigHitByVehicle",  10.0, 0.0)
  --print("1")
end



local entityEnumerator = {
  __gc = function(enum)
    if enum.destructor and enum.handle then
      enum.destructor(enum.handle)
    end
    enum.destructor = nil
    enum.handle = nil
  end
}

local function EnumerateEntities(initFunc, moveFunc, disposeFunc)
  return coroutine.wrap(function()
    local iter, id = initFunc()
    if not id or id == 0 then
      disposeFunc(iter)
      return
    end
    
    local enum = {handle = iter, destructor = disposeFunc}
    setmetatable(enum, entityEnumerator)
    
    local next = true
    repeat
      coroutine.yield(id)
      next, id = moveFunc(iter)
    until not next
    
    enum.destructor, enum.handle = nil, nil
    disposeFunc(iter)
  end)
end

function EnumerateObjects()
  return EnumerateEntities(FindFirstObject, FindNextObject, EndFindObject)
end

function EnumeratePeds()
  return EnumerateEntities(FindFirstPed, FindNextPed, EndFindPed)
end

function EnumerateVehicles()
  return EnumerateEntities(FindFirstVehicle, FindNextVehicle, EndFindVehicle)
end

function EnumeratePickups()
  return EnumerateEntities(FindFirstPickup, FindNextPickup, EndFindPickup)
end
