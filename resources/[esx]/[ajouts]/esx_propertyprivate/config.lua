Config                        = {}
Config.DrawDistance           = 100
Config.MarkerSize             = {x = 1.0, y = 1.0, z = 1.0}
Config.MarkerColor            = {r = 250, g = 100, b = 100}
Config.RoomMenuMarkerColor    = {r = 102, g = 204, b = 102}
Config.MarkerType             = 1
Config.Zones                  = {}
Config.Hangars                = {}

--[[
Config.Hangars={

  {
      id= 1,
      nom ="weed_farm",
      label="Entrepôt ventilé",
      proprio="aucun",
      entree={x=551.427,y=-3052.23,z=13.28},
      sortie={x=1065.88,y=-3183.5741,z=-39.235},
      action={x=1059.1313,y=-3205.041,z=-39.041},
      ouvert = true
  },
  {
      id= 2,
      nom ="coke_farm",
      label="Entrepôt ventilé",
      proprio="aucun",
      entree={x=-287.638,y=2535.62,z=75.69},
      sortie={x=1088.61,y=-3187.5741,z=-38.93},
      action={x=1099.6456,y=-3194.16,z=-38.99},
      ouvert = true
  },
  {
      id= 3,
      nom ="meth_farm",
      label="Laboratoire clandestin",
      proprio="aucun",
      entree={x=2251.3822,y=5155.49,z=57.89},
      sortie={x=996.85,y=-3200.80,z=-36.3},
      action={x=1009.61,y=-3196.57,z=-38.93},
      ouvert = true
  },
}
--]]
