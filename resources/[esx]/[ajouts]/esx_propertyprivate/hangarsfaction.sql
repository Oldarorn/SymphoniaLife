CREATE TABLE `hangars` (
  `id` int(11) NOT NULL,
  `nom` varchar(25) NOT NULL,
  `label` varchar(50) NOT NULL,
  `proprio` varchar(25) DEFAULT 'aucun',
  `entree` varchar(255) NOT NULL,
  `sortie` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `ouvert` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;



INSERT INTO `faction_hangar` (`id`, `nom`, `label`, `proprio`, `entree`, `sortie`, `action`, `ouvert`) VALUES
(1, 'weed_farm_1', 'test1', 'aucun', '{"y":-1895.1313,"z":25.8141,"x":288.235}', '{"y":-3200.76,"z":-37.39682,"x":997.1385}', '{"y":-3197.02,"z":-39.99682,"x":1010.58}', 1),
(2, 'weed_farm_2', 'test2', 'aucun', '{"y":-1879.1313,"z":25.8141,"x":291.269}', '{"y":-3200.76,"z":-37.39682,"x":997.1385}', '{"y":-3197.02,"z":-39.99682,"x":1010.58}', 1);





ALTER TABLE `faction_action`
  ADD PRIMARY KEY (`id`);



ALTER TABLE `faction_hangar`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `faction_hangar_inventory`
  ADD PRIMARY KEY (`id`);



ALTER TABLE `faction_action`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `faction_hangar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

ALTER TABLE `faction_hangar_inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;