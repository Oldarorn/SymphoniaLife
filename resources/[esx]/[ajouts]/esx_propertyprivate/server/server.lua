ESX = nil

TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)

function GetHangar(name)
  for i=1, #Config.Hangars, 1 do
    if Config.Hangars[i].nom == name then
      return Config.Hangars[i]
    end
  end
end


ESX.RegisterServerCallback('bl_hangarfaction:getHangars', function(source, cb)
	cb(Config.Hangars)
end)

AddEventHandler('onMySQLReady', function ()
	MySQL.Async.fetchAll('SELECT * FROM hangars', {}, function(hangars)
    for i=1, #hangars, 1 do

      local _id        = nil
      local _nom       = nil
      local _label     = nil
      local _proprio   = nil
      local _entree    = nil
      local _sortie    = nil
	    local _action    = nil
      local _ouvert    = nil
      local _price     = nil

      if hangars[i].id ~= nil then
        _id =  hangars[i].id
      end

      if hangars[i].nom ~= nil then
        _nom = hangars[i].nom
      end
      
      if hangars[i].label ~= nil then
        _label = hangars[i].label
      end
      
      if hangars[i].proprio ~= nil then
        _proprio = hangars[i].proprio
      end
      
      if hangars[i].entree ~= nil then
        _entree =  json.decode(hangars[i].entree)
      end
      
      if hangars[i].sortie ~= nil then
        _sortie =  json.decode(hangars[i].sortie)
      end
	  
	    if hangars[i].action ~= nil then
		    _action =  json.decode(hangars[i].action)
	    end
      
      if hangars[i].ouvert ~= nil then
        _ouvert =  hangars[i].ouvert
      end
      
      if hangars[i].price ~= nil then
        _price =  hangars[i].price       
      end

      table.insert(Config.Hangars, {
        id       = _id,
        nom      = _nom,
        label    = _label,
        proprio  = _proprio,
        entree   = _entree,
        sortie   = _sortie,
		    action   = _action,
        ouvert   = _ouvert,
        price    = _price

      })

    end
    
  end)

end)


AddEventHandler('bl_hangarfaction:sellHangar', function(name, faction)
  SellHangar(name, faction)
end)

function SellHangar(name, faction)

    local _source  = source
    local xPlayer =  ESX.GetPlayerFromId(_source)
    local faction  = xPlayer.faction.name

    MySQL.Async.execute(
      'UPDATE hangars SET proprio = @nom_faction WHERE nom =@name ', 
      {
        ['@nom_faction'] = faction,
        ['@name'] = name
      },
      function()

      TriggerClientEvent('esx:showNotification', _source, "La faction ~r~: "..faction.." ~w~est maintenant ~b~propriétaire ~w~de ce lieu !")

        local xPlayers = ESX.GetPlayers()

        for i=1, #xPlayers, 1 do
        
          local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
            TriggerClientEvent('bl_hangarfaction:hangarowned',xPlayer.source,name,faction)
        end        

      end
    )
end


RegisterServerEvent('bl_hangarfaction:enoughtmoney')
AddEventHandler('bl_hangarfaction:enoughtmoney', function(name, price, faction)

  local xPlayer    = ESX.GetPlayerFromId(source)
  local hangars    = GetHangar(name)
  local faction    = xPlayer.faction.name
  local blackMoney = xPlayer.getAccount('black_money').money

  if hangars.price <= xPlayer.getAccount('black_money').money then

    xPlayer.removeAccountMoney('black_money', hangars.price)
    SellHangar(name, faction)
  else
    TriggerClientEvent('esx:showNotification', source, 'Pas assez d\'argent pour acheter le hangar mec !')
  end

end)

RegisterServerEvent('bl_hangarfaction:closefaction_s')
AddEventHandler('bl_hangarfaction:closefaction_s',function(name)
	local xPlayers = ESX.GetPlayers()

	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
			TriggerClientEvent('bl_hangarfaction:closefaction_c',xPlayer.source,name)
	end

	MySQL.Async.execute('UPDATE hangars SET ouvert = 0 WHERE nom =@name ', {
		['@name'] = name

	}, function()

	end)
end)


RegisterServerEvent('bl_hangarfaction:openfaction_s')
AddEventHandler('bl_hangarfaction:openfaction_s',function(name)
	local xPlayers = ESX.GetPlayers()
	for i=1, #xPlayers, 1 do
		local xPlayer = ESX.GetPlayerFromId(xPlayers[i])
			TriggerClientEvent('bl_hangarfaction:openfaction_c',xPlayer.source,name)
	end

	MySQL.Async.execute('UPDATE hangars SET ouvert = 1 WHERE nom =@name ', {
		['@name'] = name

	}, function()

	end)
end)