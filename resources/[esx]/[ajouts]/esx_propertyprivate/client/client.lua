local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ESX              = nil

local HasAlreadyEnteredMarker = false
local LastHangar              = nil
local LastPart                = nil
local CurrentAction           = nil
local CurrentHangar           = nil
local CurrentActionMsg        = ''
local CurrentActionData       = {}
local GUI                     = {}
GUI.Time                      = 0
local isInMarker              = false
local PlayerData              = {}
PlayerData.faction = {
  name = nil,
  faction_grade = nil,
  faction_label = nil,
  faction_grade_label = nil,
  faction_grade_name = nil,
  money = 0,
  locker = nil,
  garage = nil
}



Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
end)

RegisterNetEvent('esx:playerLoaded')
AddEventHandler('esx:playerLoaded', function(xPlayer)
  PlayerData = xPlayer
	ESX.TriggerServerCallback('bl_hangarfaction:getHangars', function(hangars)
		Config.Hangars = hangars
	end)  
end)

function GetHangar(name)
	for i=1, #Config.Hangars, 1 do
		if Config.Hangars[i].nom == name then
			return Config.Hangars[i]
		end
	end
end


function DrawSub(text, time)
  ClearPrints()
	SetTextEntry_2('STRING')
	AddTextComponentString(text)
	DrawSubtitleTimed(time, 1)
end

function SetHangarOwned(name,faction)
		local hangar   = GetHangar(name)
		hangar.proprio = faction
end

function SetHangarClosed(name,faction)
		local hangar  = GetHangar(name)
		hangar.ouvert = false
end

function SetHangarOpen(name,faction)
		local hangar  = GetHangar(name)
		hangar.ouvert = true
end

function EnterHangar(name)

			local hangar  = GetHangar(name)
			local playerPed = GetPlayerPed(-1)
			CurrentHangar = hangar

			for i=1, #Config.Hangars, 1 do
				if Config.Hangars[i].nom ~= name then
					Config.Hangars[i].disabled = true
				end
			end
			Citizen.CreateThread(function()

				DoScreenFadeOut(800)

			while not IsScreenFadedOut() do
			Citizen.Wait(1)
		    end

		  SetEntityCoords(playerPed, hangar.sortie.x,  hangar.sortie.y,  hangar.sortie.z)

		DoScreenFadeIn(800)

		DrawSub(hangar.label, 5000)
		end)
end

function ExitHangar(name)

			local hangar    = GetHangar(name)
			local playerPed = GetPlayerPed(-1)
			CurrentHangar   = hangar

			for i=1, #Config.Hangars, 1 do
				if Config.Hangars[i].nom ~= name then
					Config.Hangars[i].disabled = true
				end
			end
			Citizen.CreateThread(function()

				DoScreenFadeOut(800)

			while not IsScreenFadedOut() do
			Citizen.Wait(1)
		end

		  SetEntityCoords(playerPed, hangar.entree.x,  hangar.entree.y,  hangar.entree.z)

		DoScreenFadeIn(800)

		DrawSub(hangar.label.." sortie", 5000)
		end)
end



function OpenHangarMenu(hangar)

	local elements   = {}

	if(hangar.proprio == "aucun" and PlayerData.faction.name ~= "resid" ) then

		table.insert(elements, {label = "Acheter cette propriété", value = 'vente'})

	else

	if(hangar.proprio == PlayerData.faction.name ) then

	    table.insert(elements, {label = "Entrer dans le hangar", value = 'entree'})
        table.insert(elements, {label = "Sortir du hangar",      value = 'sortie'})		
		table.insert(elements, {label = "Fermer cette propriété", value = 'close'})
		table.insert(elements, {label = "Ouvrir cette propriété", value = 'open' })

	end

	end


	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'hangar',
		{
			title    = hangar.label,
			align    = 'top-left',
			elements = elements,
		},
		function(data2, menu)

	menu.close()

			if data2.current.value == 'close' then
				TriggerServerEvent('bl_hangarfaction:closefaction_s',hangar.nom)
				ESX.ShowNotification("Vous avez ~r~fermé ~b~ "..hangar.label.." ~w~!")
			end

			if data2.current.value == 'open' then
				TriggerServerEvent('bl_hangarfaction:openfaction_s',hangar.nom)
				ESX.ShowNotification("Vous avez ~g~ouvert ~b~ "..hangar.label.." ~w~!")
			end

			if data2.current.value == 'entree' then
				if(hangar.ouvert == true) then
						EnterHangar(hangar.nom)
				else
						ESX.ShowNotification("Le Hangar est ~r~fermé~w~.")
				end
			end

			if data2.current.value == 'sortie' then
				if(hangar.ouvert == true) then
						ExitHangar(hangar.nom)
				else
						ESX.ShowNotification("Le hangar est ~r~fermé~w~.")
				end
			end
			
			if data2.current.value == 'vente' then
				TriggerServerEvent('bl_hangarfaction:enoughtmoney', hangar.nom)
			end
		
		end,
		function(data, menu)

			menu.close()

				CurrentAction     = 'hangar_menu'
				CurrentActionMsg  = "Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu"
				CurrentActionData = {hangar = hangar}
		end
	)

end

function OpenHangarActionMenu(hangar)
	
	local elements = {}

	if(PlayerData.faction.name~="resid")then

			if(hangar.nom == "hangar_weed" and PlayerData.faction.grade_name ~= 'recrue')then
				table.insert(elements, {label = "Fabriquer du Haschich", value = 'weed2_farm'})
			end

			if(hangar.nom == "hangar_coke" and PlayerData.faction.grade_name ~= 'recrue')then
				table.insert(elements, {label = "Fabriquer de la Coke Pure", value = 'coke_farm'})
			end

			if(hangar.nom == "hangar_meth" and PlayerData.faction.grade_name ~= 'recrue')then
				table.insert(elements, {label = "Fabriquer des Cristaux de Meth Purs", value = 'meth_farm'})
				table.insert(elements, {label = "Fabriquer de la Dolphin", value = 'dolph_farm'})
			end

			if(hangar.nom == "hangar_money" and PlayerData.faction.grade_name ~= 'recrue')then
				table.insert(elements, {label = "Blanchir des faux billets", value = 'wash_money'})
				table.insert(elements, {label = "Fabriquer des faux billets", value = 'dirt_money'})
				table.insert(elements, {label = "Arrêter la fabrication", value = 'stop_money'})
			end

	end

	ESX.UI.Menu.Open(
		'default', GetCurrentResourceName(), 'hangar_action',
		{
			title    = hangar.label .." - travail",
			align    = 'top-left',
			elements = elements,
		},
		function(data2, menu)

			menu.close()

			if data2.current.value == 'weed_farm' then

				TriggerServerEvent('bl_Advanceddrugs:startTransformWeed2')
				ESX.ShowNotification('Vous traitez la ~g~weed~w~ pour extraire le pollen.')
			end

			if data2.current.value == 'coke_farm' then

				TriggerServerEvent('bl_Advanceddrugs:startTransformCoke')
				ESX.ShowNotification('Vous fabriquez votre ~g~cocaïne~w~ pure.')
			 ESX.ShowNotification('~b~Attendez ~w~que les produits agissent...')
			end

			if data2.current.value == 'meth_farm' then

				TriggerServerEvent('bl_Advanceddrugs:startTransformMeth')
				ESX.ShowNotification('Vous fabriquez des cristaux de ~g~méthamphétamine~w~ purs.')
			 ESX.ShowNotification('~b~Attendez ~w~que les produits agissent...')
			end

			--if data2.current.value == 'dolph_farm' then

			--	TriggerServerEvent('bl_hangarfaction:startTransformDolph')
			--	ESX.ShowNotification('Vous fabriquez des cristaux de ~g~Dolphin~w~.')
			-- ESX.ShowNotification('~b~Attendez ~w~que les produits agissent...')
			--end

			if data2.current.value == 'wash_money' then

				TriggerServerEvent('bl_Advanceddrugs:startWash')
				ESX.ShowNotification('Vous blanchissez des faux ~g~billets~w~')
			 ESX.ShowNotification('~b~ Attendez ~w~...')
			end

			if data2.current.value == 'dirt_money' then

				TriggerServerEvent('bl_Advanceddrugs:startDirt')
				ESX.ShowNotification('Vous fabriquez des faux ~g~billets~w~.')
			 ESX.ShowNotification('~b~ Attendez ~w~...')
			end	

			if data2.current.value == 'stop_money' then

				TriggerServerEvent('bl_Advanceddrugs:stopDirt')
				TriggerServerEvent('bl_Advanceddrugs:stopWash')
				ESX.ShowNotification('~g~Imprimante~w~ en arrêt...')
			 ESX.ShowNotification('~b~ Attendez ~w~...')
			end	
		end,
		function(data, menu)

			menu.close()

				CurrentAction     = 'hangar_menu'
				CurrentActionMsg  = "Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu"
				CurrentActionData = {hangar = hangar}
		end
	)

end

RegisterNetEvent('bl_hangarfaction:closefaction_c')
AddEventHandler('bl_hangarfaction:closefaction_c',function(name)
	SetHangarClosed(name)
end)

RegisterNetEvent('bl_hangarfaction:openfaction_c')
AddEventHandler('bl_hangarfaction:openfaction_c',function(name)
	SetHangarOpen(name)
end)

RegisterNetEvent('bl_hangarfaction:hangarowned')
AddEventHandler('bl_hangarfaction:hangarowned',function(name,faction)
	SetHangarOwned(name,faction)
end)

RegisterNetEvent('bl_hangarfaction:sellhangar')
AddEventHandler('bl_hangarfaction:sellhangar',function(name)
	SellHangar(name)
end)

AddEventHandler('bl_hangarfaction:hasExitedMarker', function(name, part)	
	CurrentAction = nil
	--ESX.UI.Menu.CloseAll()
end)

AddEventHandler('bl_hangarfaction:hasEnteredMarker', function(name, part)

	local hangar = GetHangar(name)

	if part == 'entree' then
		CurrentAction     = 'entree'
		CurrentActionMsg  = "Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu du hangar"
		CurrentActionData = {hangar = hangar}
	end

	if part == 'action' then
	    CurrentAction     = 'action'
	    CurrentActionMsg  = "Appuyez sur ~INPUT_CONTEXT~ pour ouvrir le menu fabrication"
		CurrentActionData = {hangar = hangar}
	end

	if part == 'sortie' then
		CurrentAction     = 'sortie'
		CurrentActionMsg  = "Appuyez sur ~INPUT_CONTEXT~ pour sortir du hangar"
		CurrentActionData = {hangar = hangar}
	end
	
end)



--Draw markers
Citizen.CreateThread(function()
  while true do

    Citizen.Wait(1)
		local coords = GetEntityCoords(GetPlayerPed(-1))
			for i=1, #Config.Hangars, 1 do
				local hangar = Config.Hangars[i]


				if(hangar.entree ~= nil and GetDistanceBetweenCoords(coords, hangar.entree.x, hangar.entree.y, hangar.entree.z, true) < Config.DrawDistance) then
					--DrawMarker(Config.MarkerType, hangar.entree.x, hangar.entree.y, hangar.entree.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
				end

				if(hangar.sortie ~= nil  and GetDistanceBetweenCoords(coords, hangar.sortie.x, hangar.sortie.y, hangar.sortie.z, true) < Config.DrawDistance) then
					DrawMarker(Config.MarkerType, hangar.sortie.x, hangar.sortie.y, hangar.sortie.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
				end

				if(hangar.action ~= nil  and GetDistanceBetweenCoords(coords, hangar.action.x, hangar.action.y, hangar.action.z, true) < Config.DrawDistance) then
				    DrawMarker(Config.MarkerType, hangar.action.x, hangar.action.y, hangar.action.z-1, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.MarkerSize.x, Config.MarkerSize.y, Config.MarkerSize.z, Config.MarkerColor.r, Config.MarkerColor.g, Config.MarkerColor.b, 100, false, true, 2, false, false, false, false)
				end
			end
  
  end

end)


-- Enter / Exit marker events
Citizen.CreateThread(function()
	while true do

		Citizen.Wait(1)
		local coords          = GetEntityCoords(GetPlayerPed(-1))
		local currentHangar   = nil
		local currentPart     = nil
		isInMarker	 = false
		local coords = GetEntityCoords(GetPlayerPed(-1))
			for i=1, #Config.Hangars, 1 do
				local hangar = Config.Hangars[i]

				if(hangar.entree ~= nil and GetDistanceBetweenCoords(coords, hangar.entree.x, hangar.entree.y, hangar.entree.z, true) < 3) then
					isInMarker      = true
					currentHangar = hangar.nom
					currentPart     = 'entree'

				end
				
				if(hangar.sortie ~= nil and GetDistanceBetweenCoords(coords, hangar.sortie.x, hangar.sortie.y, hangar.sortie.z, true) < 3) then
					isInMarker      = true
					currentHangar = hangar.nom
					currentPart     = 'sortie'
				end

				if(hangar.action ~= nil and GetDistanceBetweenCoords(coords, hangar.action.x, hangar.action.y, hangar.action.z, true) < 3) then
					isInMarker      = true
					currentHangar = hangar.nom
					currentPart     = 'action'
				end



				if isInMarker and not HasAlreadyEnteredMarker or (isInMarker and (LastHangar ~= currentHangar or LastPart ~= currentPart) ) then


					HasAlreadyEnteredMarker = true
					LastHangar              = currentHangar
					LastPart                = currentPart

					TriggerEvent('bl_hangarfaction:hasEnteredMarker', currentHangar, currentPart)
				end

				if not isInMarker and HasAlreadyEnteredMarker then

					HasAlreadyEnteredMarker = false

					TriggerEvent('bl_hangarfaction:hasExitedMarker', LastHangar, LastPart)
				end



			end
		end

	end)

	-- Key controls
	Citizen.CreateThread(function()
		while true do

			Citizen.Wait(1)

			if CurrentAction ~= nil then

				SetTextComponentFormat('STRING')
				AddTextComponentString(CurrentActionMsg)
				DisplayHelpTextFromStringLabel(0, 0, 1, -1)

				if IsControlPressed(0,  Keys['E']) and (GetGameTimer() - GUI.Time) > 300 then


					if CurrentAction == 'entree' then
						OpenHangarMenu(CurrentActionData.hangar)
					end

					if CurrentAction == 'sortie' then
						OpenHangarMenu(CurrentActionData.hangar)
					end

					if CurrentAction == 'action' then
						OpenHangarActionMenu(CurrentActionData.hangar)
					end

					CurrentAction = nil
					GUI.Time      = GetGameTimer()

				end

			end

		end
	end)
