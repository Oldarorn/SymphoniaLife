local Keys = {
	["ESC"] = 322, ["F1"] = 288, ["F2"] = 289, ["F3"] = 170, ["F5"] = 166, ["F6"] = 167, ["F7"] = 168, ["F8"] = 169, ["F9"] = 56, ["F10"] = 57,
	["~"] = 243, ["1"] = 157, ["2"] = 158, ["3"] = 160, ["4"] = 164, ["5"] = 165, ["6"] = 159, ["7"] = 161, ["8"] = 162, ["9"] = 163, ["-"] = 84, ["="] = 83, ["BACKSPACE"] = 177,
	["TAB"] = 37, ["Q"] = 44, ["W"] = 32, ["E"] = 38, ["R"] = 45, ["T"] = 245, ["Y"] = 246, ["U"] = 303, ["P"] = 199, ["["] = 39, ["]"] = 40, ["ENTER"] = 18,
	["CAPS"] = 137, ["A"] = 34, ["S"] = 8, ["D"] = 9, ["F"] = 23, ["G"] = 47, ["H"] = 74, ["K"] = 311, ["L"] = 182,
	["LEFTSHIFT"] = 21, ["Z"] = 20, ["X"] = 73, ["C"] = 26, ["V"] = 0, ["B"] = 29, ["N"] = 249, ["M"] = 244, [","] = 82, ["."] = 81,
	["LEFTCTRL"] = 36, ["LEFTALT"] = 19, ["SPACE"] = 22, ["RIGHTCTRL"] = 70,
	["HOME"] = 213, ["PAGEUP"] = 10, ["PAGEDOWN"] = 11, ["DELETE"] = 178,
	["LEFT"] = 174, ["RIGHT"] = 175, ["TOP"] = 27, ["DOWN"] = 173,
	["NENTER"] = 201, ["N4"] = 108, ["N5"] = 60, ["N6"] = 107, ["N+"] = 96, ["N-"] = 97, ["N7"] = 117, ["N8"] = 61, ["N9"] = 118
}


ESX = nil
local GUI           = {}
GUI.Time            = 0
local onDivingSuit  = false
local lastCapture   = nil

Citizen.CreateThread(function()
	while ESX == nil do
		TriggerEvent('esx:getSharedObject', function(obj) ESX = obj end)
		Citizen.Wait(1)
	end
end)
function Info(str)
	SetTextComponentFormat("STRING")
	AddTextComponentString(str)
	DisplayHelpTextFromStringLabel(0, 0, 1, -1)
end

function GetRecompense()
	local random = math.random(0,100)


	if(random>-1 and random<66)then -- 0 a 66 67%
		TriggerServerEvent("esx_plongee:giveitem","fish")
	end
	if(random>65 and random < 73)then --50 à 72 22%
		TriggerServerEvent("esx_plongee:giveitem","stone")
	end
	if(random>83 and random<95)then -- 84 à 94 10 %
		TriggerServerEvent("esx_plongee:giveitem","wood")
	end
	if(random>94 and random<99)then	-- 95 à 100 3%
TriggerServerEvent("esx_plongee:giveitem","diamond")
	end
	if(random>80 and random<84)then	-- 81 à 84 3%
		TriggerServerEvent("esx_plongee:giveitem","gold")
	end
	--if(random==73)then	--1%
	--	TriggerServerEvent("esx_plongee:giveitem","coke_pooch")
	--end
	--if(random ==74)then	--1%
	--	TriggerServerEvent("esx_plongee:giveitem","meth_pooch")
	--end
	if(random ==75)then	--1%
		TriggerServerEvent("esx_plongee:giveitem","tresor")
	end
	if(random>75 and random<81)then -- 76 à 80 4%
		TriggerServerEvent("esx_plongee","tel")
	end


end


Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)
		local coords = GetEntityCoords(GetPlayerPed(-1))


				if(GetDistanceBetweenCoords(coords, Config.Zones.ChangeRoom.Pos.x, Config.Zones.ChangeRoom.Pos.y, Config.Zones.ChangeRoom.Pos.z, true) <5) then
				 DrawMarker(Config.Type, Config.Zones.ChangeRoom.Pos.x, Config.Zones.ChangeRoom.Pos.y, Config.Zones.ChangeRoom.Pos.z, 0.0, 0.0, 0.0, 0, 0.0, 0.0, Config.Size.x, Config.Size.y, Config.Size.z, Config.Color.r, Config.Color.g, Config.Color.b, 100, false, true, 2, false, false, false, false)
				 Info("Appuyez sur ~INPUT_CONTEXT~ pour vous changer")
				 if(IsControlPressed( 0,Keys["E"])and (GetGameTimer() - GUI.Time) > 300)then
					 onDivingSuit = false
					 GUI.Time      = GetGameTimer()
					ESX.TriggerServerCallback('esx_skin:getPlayerSkin', function(skin, jobSkin)
						local model = nil

						if skin.sex == 0 then
							model = GetHashKey("mp_m_freemode_01")
						else
							model = GetHashKey("mp_f_freemode_01")
						end

						RequestModel(model)
						while not HasModelLoaded(model) do
							RequestModel(model)
							Citizen.Wait(1)
						end

						SetPlayerModel(PlayerId(), model)
						SetModelAsNoLongerNeeded(model)

						TriggerEvent('skinchanger:loadSkin', skin)
						TriggerEvent('esx:restoreLoadout')
					end)

				 end
				end



	end

end)


Citizen.CreateThread(function()
	while true do
		Citizen.Wait(1)

		local player = GetPlayerPed(-1)
		if(IsPedSwimmingUnderWater(player))then
			local coords = GetEntityCoords(player)
			if(coords.z < -20.00)then
						if(lastCapture ~= nil)then
							if(GetDistanceBetweenCoords(lastCapture, coords, true)>75)then
								Info("Appuyez sur ~INPUT_CONTEXT~ pour fouiller le sable")
								 if(IsControlPressed( 0,Keys["E"])and (GetGameTimer() - GUI.Time) > 300)then
											lastCapture= coords
											 local random = math.random(0,100)

												if(random>-1)then
														ESX.ShowNotification("Vous n'avez ~g~trouvé quelque chose~w~...")
														GetRecompense()
												else
														ESX.ShowNotification("Vous n'avez ~r~rien trouvé~w~...")

												end
												 GUI.Time      = GetGameTimer()
									end
							end

							else
								Info("Appuyez sur ~INPUT_CONTEXT~ pour fouiller le sable")
								 if(IsControlPressed( 0,Keys["E"])and (GetGameTimer() - GUI.Time) > 300)then
									 		lastCapture= coords
											 local random = math.random(0,100)

												if(random>65)then
													ESX.ShowNotification("Vous n'avez ~g~trouvé quelque chose~w~...")
													GetRecompense()
												else
														ESX.ShowNotification("Vous n'avez ~r~rien trouvé~w~...")

												end
											 	 GUI.Time      = GetGameTimer()
									end
							end
				end

		end
	end
end)
